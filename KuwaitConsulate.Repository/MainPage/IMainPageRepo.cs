﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.MainPage;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Repository.MainPage;

public interface IMainPageRepo
{
    Task AddCircularsAsync(string name, List<Guid> fileIds);
    Task UpdateCircularsAsync(Guid id, string name, List<Guid> fileIds);
    Task RemoveCircularsAsync(Guid id);
    Task<CommonResponseDto<List<CircularsDto>>> GetAllCircularsAsync(int pageSize, int pageNum, string search, CircularsSort circularsSort, bool isAsc);
    Task<CommonResponseDto<CircularsDto>> GetCircularsAsync(Guid id);
    Task AddInstructionAsync(string name, List<Guid> fileIds);
    Task AddStudentGuideAsync(string title, string link, string icon);
    Task AddUsefulLinkAsync(string title, string link, string icon);
    Task<CommonResponseDto<List<InstructionDto>>> GetAllInstructionAsync(int pageSize, int pageNum, string search, InstructionSort instructionSort, bool isAsc);
    Task<CommonResponseDto<List<StudentGuideDto>>> GetAllStudentGuideAsync(string search, int pageSize, int pageNum, StudentGuideSort studentGuideSort, bool isAsc);
    Task<CommonResponseDto<List<UsefulLinkDto>>> GetAllUsefulLinkAsync(string search, int pageSize, int pageNum, UsefulLinkSort usefulLinkSort, bool isAsc);
    Task<CommonResponseDto<InstructionDto>> GetInstructionAsync(Guid id);
    Task<CommonResponseDto<StudentGuideDto>> GetStudentGuideAsync(Guid id);
    Task<CommonResponseDto<UsefulLinkDto>> GetUsefulLinkAsync(Guid id);
    Task RemoveInstructionAsync(Guid id);
    Task RemoveStudentGuideAsync(Guid id);
    Task RemoveUsefulLinkAsync(Guid id);
    Task UpdateInstructionAsync(Guid id, string name, List<Guid> fileIds);
    Task UpdateStudentGuideAsync(Guid id, string title, string link, string icon);
    Task UpdateUsefulLinkAsync(Guid id, string title, string link, string icon);
    Task AddShortNewsAsync(string body);
    Task<CommonResponseDto<List<ShortNewsDto>>> GetAllShortNewsAsync(string search, int pageSize, int pageNum, ShortNewsSort shortNewsSort, bool isAsc,
        DateTime? from, DateTime? to);
    Task UpdateShortNewsAsync(Guid id, string body);
    Task<CommonResponseDto<ShortNewsDto>> GetShortNewsAsync(Guid id);
    Task RemoveShortNewsAsync(Guid id);
    Task<CommonResponseDto<List<FileDto>>> GetAllCarouselItemAsync();
}