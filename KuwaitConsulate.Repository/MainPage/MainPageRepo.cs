﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models.MainPage;
using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.MainPage;
using KuwaitConsulate.Shared.Enum;
using KuwaitConsulate.Shared.Exception;
using KuwaitConsulate.Shared.Helper;
using KuwaitConsulate.Shared.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System.Linq.Expressions;

namespace KuwaitConsulate.Repository.MainPage;

public class MainPageRepo : BaseRepo, IMainPageRepo
{
    private readonly IStringLocalizer<MainPageResource> mainPageLocalizer;
    public MainPageRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment, IStringLocalizer<MainPageResource> mainPageLocalizer)
        : base(context, configuration, httpContextAccessor, hostingEnvironment)
    {
        this.mainPageLocalizer = mainPageLocalizer;
    }
    public async Task AddCircularsAsync(string name, List<Guid> fileIds)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<CircularsModel>(m => m.IncrementNumber);
        var x = await context.Circulars.AddAsync(new CircularsModel
        {
            Name = name,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.CR, NextIncrementNumber)
        });
        await context.SaveChangesAsync();

        await context.File.Where(f => fileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.CircularsId, x.Entity.Id));

        await context.SaveChangesAsync();
    }
    public async Task UpdateCircularsAsync(Guid id, string name, List<Guid> fileIds)
    {
        var circularsModel = await context.Circulars.Where(n => n.IsValid && n.Id == id).Include(c => c.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(mainPageLocalizer[ErrorMessages.CircularsNotFound]);
        circularsModel.Name = name;

        context.File.RemoveRange(circularsModel.Files.Where(f => !fileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && fileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.CircularsId, id));

        await context.SaveChangesAsync();
    }
    public async Task RemoveCircularsAsync(Guid id)
    {
        await context.Circulars.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<CircularsDto>>> GetAllCircularsAsync(int pageSize, int pageNum, string search, CircularsSort circularsSort, bool isAsc)
    {
        Expression<Func<CircularsModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Name.Contains(search));

        var circularsQuery = context.Circulars.Where(expression);
        circularsQuery = CustomOrderBy(circularsQuery, circularsSort, isAsc);

        var circularsDto = await context.Circulars.Where(expression)
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new CircularsDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).ToListAsync();
        return new(circularsDto);
    }
    public async Task<CommonResponseDto<CircularsDto>> GetCircularsAsync(Guid id)
    {
        var circularDto = (await context.Circulars.Where(n => n.IsValid && n.Id == id)
            .Select(n => new CircularsDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(mainPageLocalizer[ErrorMessages.CircularsNotFound]);

        return new(circularDto);
    }




    public async Task AddInstructionAsync(string name, List<Guid> fileIds)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<InstructionModel>(m => m.IncrementNumber);
        var x = await context.Instruction.AddAsync(new InstructionModel
        {
            Name = name,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.IN, NextIncrementNumber)
        });
        await context.SaveChangesAsync();

        await context.File.Where(f => fileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.InstructionId, x.Entity.Id));

        await context.SaveChangesAsync();
    }
    public async Task UpdateInstructionAsync(Guid id, string name, List<Guid> fileIds)
    {
        var instructionModel = await context.Instruction.Where(n => n.IsValid && n.Id == id).Include(c => c.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(mainPageLocalizer[ErrorMessages.InstructionNotFound]);

        instructionModel.Name = name;

        context.File.RemoveRange(instructionModel.Files.Where(f => !fileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && fileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.InstructionId, id));

        await context.SaveChangesAsync();
    }
    public async Task RemoveInstructionAsync(Guid id)
    {
        await context.File.Where(n => n.InstructionId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.Instruction.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<InstructionDto>>> GetAllInstructionAsync(int pageSize, int pageNum, string search, InstructionSort instructionSort, bool isAsc)
    {
        Expression<Func<InstructionModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Name.Contains(search));

        var instructionQuery = context.Instruction.Where(expression);
        instructionQuery = CustomOrderBy(instructionQuery, instructionSort, isAsc);

        var circularsDto = await context.Instruction.Where(expression)
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new InstructionDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).ToListAsync();
        return new(circularsDto);
    }
    public async Task<CommonResponseDto<InstructionDto>> GetInstructionAsync(Guid id)
    {
        var circularDto = (await context.Instruction.Where(n => n.IsValid && n.Id == id)
            .Select(n => new InstructionDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(mainPageLocalizer[ErrorMessages.InstructionNotFound]);

        return new(circularDto);
    }



    public async Task AddStudentGuideAsync(string title, string link, string icon)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<StudentGuideModel>(m => m.IncrementNumber);
        var x = await context.StudentGuide.AddAsync(new StudentGuideModel
        {
            Icon = icon,
            Link = link,
            Title = title,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.SG, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
    }
    public async Task UpdateStudentGuideAsync(Guid id, string title, string link, string icon)
    {
        var studentGuideModel = await context.StudentGuide.Where(n => n.IsValid && n.Id == id).FirstOrDefaultAsync() ??
            throw new NotFoundException(mainPageLocalizer[ErrorMessages.StudentGuideNotFound]);

        studentGuideModel.Icon = icon;
        studentGuideModel.Link = link;
        studentGuideModel.Title = title;

        await context.SaveChangesAsync();
    }
    public async Task RemoveStudentGuideAsync(Guid id)
    {
        await context.StudentGuide.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<StudentGuideDto>>> GetAllStudentGuideAsync(string search, int pageSize, int pageNum, StudentGuideSort studentGuideSort, bool isAsc)
    {
        Expression<Func<StudentGuideModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Title.Contains(search) || n.Link.Contains(search));

        var studentGuideQuery = context.StudentGuide.Where(expression);
        studentGuideQuery = CustomOrderBy(studentGuideQuery, studentGuideSort, isAsc);

        var studentGuidesDto = await studentGuideQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new StudentGuideDto
            {
                Id = n.Id,
                Icon = n.Icon,
                Title = n.Title,
                Link = n.Link,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).ToListAsync();

        var totalRecords = await studentGuideQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(studentGuidesDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<StudentGuideDto>> GetStudentGuideAsync(Guid id)
    {
        var studentGuideDto = (await context.StudentGuide.Where(n => n.IsValid && n.Id == id)
            .Select(n => new StudentGuideDto
            {
                Id = n.Id,
                Icon = n.Icon,
                Title = n.Title,
                Link = n.Link,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(mainPageLocalizer[ErrorMessages.StudentGuideNotFound]);

        return new(studentGuideDto);
    }




    public async Task AddUsefulLinkAsync(string title, string link, string icon)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<UsefulLinkModel>(m => m.IncrementNumber);
        var x = await context.UsefulLink.AddAsync(new UsefulLinkModel
        {
            Icon = icon,
            Link = link,
            Title = title,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.UL, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
    }
    public async Task UpdateUsefulLinkAsync(Guid id, string title, string link, string icon)
    {
        var usefulLinkModel = await context.UsefulLink.Where(n => n.IsValid && n.Id == id).FirstOrDefaultAsync() ??
            throw new NotFoundException(mainPageLocalizer[ErrorMessages.UsefulLinkNotFound]);

        usefulLinkModel.Icon = icon;
        usefulLinkModel.Link = link;
        usefulLinkModel.Title = title;

        await context.SaveChangesAsync();
    }
    public async Task RemoveUsefulLinkAsync(Guid id)
    {
        await context.UsefulLink.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<UsefulLinkDto>>> GetAllUsefulLinkAsync(string search, int pageSize, int pageNum, UsefulLinkSort usefulLinkSort, bool isAsc)
    {
        Expression<Func<UsefulLinkModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Title.Contains(search) || n.Link.Contains(search));

        var usefulLinkQuery = context.UsefulLink.Where(expression);
        usefulLinkQuery = CustomOrderBy(usefulLinkQuery, usefulLinkSort, isAsc);

        var usefulLinkDto = await usefulLinkQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new UsefulLinkDto
            {
                Id = n.Id,
                Icon = n.Icon,
                Title = n.Title,
                Link = n.Link,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).ToListAsync();

        var totalRecords = await usefulLinkQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(usefulLinkDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<UsefulLinkDto>> GetUsefulLinkAsync(Guid id)
    {
        var studentGuideDto = (await context.UsefulLink.Where(n => n.IsValid && n.Id == id)
            .Select(n => new UsefulLinkDto
            {
                Id = n.Id,
                Icon = n.Icon,
                Title = n.Title,
                Link = n.Link,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(mainPageLocalizer[ErrorMessages.UsefulLinkNotFound]);

        return new(studentGuideDto);
    }




    public async Task AddShortNewsAsync(string body)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<ShortNewsModel>(m => m.IncrementNumber);
        var x = await context.ShortNews.AddAsync(new ShortNewsModel
        {
            Body = body,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.SN, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
    }
    public async Task UpdateShortNewsAsync(Guid id, string body)
    {
        var shortNewsModel = await context.ShortNews.Where(n => n.IsValid && n.Id == id).FirstOrDefaultAsync() ??
            throw new NotFoundException(mainPageLocalizer[ErrorMessages.ShortNewsNotFound]);

        shortNewsModel.Body = body;

        await context.SaveChangesAsync();
    }
    public async Task RemoveShortNewsAsync(Guid id)
    {
        await context.ShortNews.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<ShortNewsDto>>> GetAllShortNewsAsync(string search, int pageSize, int pageNum, ShortNewsSort shortNewsSort, bool isAsc
        , DateTime? from, DateTime? to)
    {
        Expression<Func<ShortNewsModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Body.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var shortNewsQuery = context.ShortNews.Where(expression);
        shortNewsQuery = CustomOrderBy(shortNewsQuery, shortNewsSort, isAsc);

        var shortNewsDto = await shortNewsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new ShortNewsDto
            {
                Id = n.Id,
                Body = n.Body,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).ToListAsync();

        var totalRecords = await shortNewsQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(shortNewsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<ShortNewsDto>> GetShortNewsAsync(Guid id)
    {
        var studentGuideDto = (await context.ShortNews.Where(n => n.IsValid && n.Id == id)
            .Select(n => new ShortNewsDto
            {
                Id = n.Id,
                Body = n.Body,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(mainPageLocalizer[ErrorMessages.ShortNewsNotFound]);

        return new(studentGuideDto);
    }




    public async Task<CommonResponseDto<List<FileDto>>> GetAllCarouselItemAsync()
    {
        var filesDto = await context.File.Where(f => f.IsValid && f.FileKind == FileKind.CarouselItem)
            .Select(f => new FileDto
            {
                Id = f.Id,
                Url = f.Url,
                Name = f.Name,
                FileKind = f.FileKind,
                FileType = f.FileType
            }).ToListAsync();
        return new(filesDto);
    }
}