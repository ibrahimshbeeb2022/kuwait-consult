﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models;
using KuwaitConsulate.Dto.Token;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;

namespace KuwaitConsulate.Repository;

public class BaseRepo
{
    protected readonly IConfiguration configuration;
    protected readonly KuwaitConsulateDbContext context;
    protected readonly IHttpContextAccessor httpContextAccessor;
    protected readonly IHostingEnvironment hostingEnvironment;
    public BaseRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment)
    {
        this.context = context;
        this.configuration = configuration;
        this.hostingEnvironment = hostingEnvironment;
        this.httpContextAccessor = httpContextAccessor;
    }
    public Guid CurrentUserId
    {
        get
        {
            return GetUserId();
        }
    }
    public UserType CurrentUserType
    {
        get
        {
            return GetUserType();
        }
    }
    public double GetTimeZone()
    {
        var timeZoneOffsetInMinutes = httpContextAccessor.HttpContext.Request.Headers["timezone"].FirstOrDefault();
        if (timeZoneOffsetInMinutes.StartsWith('-'))
            return -double.Parse(timeZoneOffsetInMinutes[1..]);
        return -double.Parse(timeZoneOffsetInMinutes);
    }
    public Guid GetUserId()
    {
        var bearerToken = httpContextAccessor.HttpContext.Request.Headers["Authorization"];

        var handler = new JwtSecurityTokenHandler();
        var jwtSecurityToken = handler.ReadJwtToken(bearerToken.ToString().Split("Bearer ").ElementAt(1));
        var userId = jwtSecurityToken.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault().Value;

        return new Guid(userId);
    }
    public UserType GetUserType()
    {
        var bearerToken = httpContextAccessor.HttpContext.Request.Headers["Authorization"];

        var handler = new JwtSecurityTokenHandler();
        var jwtSecurityToken = handler.ReadJwtToken(bearerToken.ToString().Split("Bearer ").ElementAt(1));
        var userType = jwtSecurityToken.Claims.Where(c => c.Type == "userType").FirstOrDefault().Value;
        UserType res = UserType.NormalUser;
        switch (userType)
        {
            case nameof(UserType.Admin):
                res = UserType.Admin;
                break;
            case nameof(UserType.Employee):
                res = UserType.Employee;
                break;
            case nameof(UserType.NormalUser):
                res = UserType.NormalUser;
                break;
        }
        return res;
    }
    public async Task<bool> CheckEntityExist<T>(Expression<Func<T, bool>> whereExpr) where T : class
    {
        var Exist = await context.Set<T>().AnyAsync(whereExpr);
        return Exist;
    }
    public async Task<int> CountEntityExist<T>(Expression<Func<T, bool>> whereExpr) where T : class
    {
        var count = await context.Set<T>().CountAsync(whereExpr);
        return count;
    }
    public async Task<int> GetNextIncrementNumber<T>(Expression<Func<T, int>> whereExpr) where T : BaseModel
    {
        if (await context.Set<T>().Where(x => x.IsValid).AnyAsync())
            return await context.Set<T>().Where(x => x.IsValid).MaxAsync(whereExpr) + 1;
        return 1;
    }
    public TokenDto GenerateJwtTokenAsync(Guid userId, UserType userType)
    {
        var secretKey = configuration["JwtConfig:secret"];
        var validIssuer = configuration.GetSection("JwtConfig:validIssuer").Value;
        var validAudience = configuration.GetSection("JwtConfig:validAudience").Value;

        var claims = new List<Claim>
        {
            new ("userType", userType.ToString()),
            new (ClaimTypes.NameIdentifier, userId.ToString()),
            new (JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
        };

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        var expires = DateTime.UtcNow.AddDays(3);

        var token = new JwtSecurityToken(
            validIssuer,
            validAudience,
            claims,
            expires: expires,
            signingCredentials: creds);

        var t = new JwtSecurityTokenHandler().WriteToken(token);
        return new TokenDto("Bearer " + t, token.ValidTo);
    }
    public static PasswordVerificationResult CheckPassword(string password, string hash, out string newHash)
    {
        var passwordHasher = new PasswordHasher<object>();
        var result = passwordHasher.VerifyHashedPassword(null, hash, password);
        if (result == PasswordVerificationResult.SuccessRehashNeeded)
        {
            newHash = HashPassword(password);

            return result;
        }
        else if (result == PasswordVerificationResult.Success)
        {
            newHash = hash;
            return result;
        }
        newHash = null;
        return result;
    }
    public static string HashPassword(string password)
    {
        var passwordHasher = new PasswordHasher<object>();
        return passwordHasher.HashPassword(null, password);
    }
    public static PasswordVerificationResult VerifyHashedPassword(string hash, string password)
    {
        var passwordHasher = new PasswordHasher<object>();
        return passwordHasher.VerifyHashedPassword(null, hash, password);
    }
    public static IQueryable<TEntity> CustomOrderBy<TEntity, TEnum>(IQueryable<TEntity> source, TEnum property, bool ascending = false)
    {
        if (property == null) return source;

        var keySelector = GetKeySelectorExpression<TEntity, TEnum>(property);

        if (keySelector == null) return source;

        if (ascending)
            return source.OrderBy(keySelector);

        else
            return source.OrderByDescending(keySelector);

    }
    private static Expression<Func<TEntity, object>> GetKeySelectorExpression<TEntity, TEnum>(TEnum sortBy)
    {
        var parameter = Expression.Parameter(typeof(TEntity), "entity");

        var s = typeof(TEntity).GetProperties();
        if (!s.Any(m => m.Name == sortBy.ToString()))
            return null;

        var property = Expression.Property(parameter, sortBy.ToString());

        var conversion = Expression.Convert(property, typeof(object));

        var keySelectorExpression = Expression.Lambda<Func<TEntity, object>>(conversion, parameter);

        return keySelectorExpression;
    }
    public async Task<bool> CheckIfHasNextPageAsync<T>(Expression<Func<T, bool>> expression, int pageSize, int pageNum) where T : class
    {
        var totalRecords = await context.Set<T>().Where(expression).CountAsync();
        ++pageNum;
        return totalRecords > (pageSize * pageNum);
    }
}