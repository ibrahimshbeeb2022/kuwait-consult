﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models.BasicPage;
using KuwaitConsulate.Database.Models.MainPage;
using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.BasicPage;
using KuwaitConsulate.Shared.Enum;
using KuwaitConsulate.Shared.Exception;
using KuwaitConsulate.Shared.Helper;
using KuwaitConsulate.Shared.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System.Linq.Expressions;

namespace KuwaitConsulate.Repository.BasicPage;

public class BasicPageRepo : BaseRepo, IBasicPageRepo
{
    private readonly IStringLocalizer<BasicPageResource> basicPageLocalizer;
    public BasicPageRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment, IStringLocalizer<BasicPageResource> basicPageLocalizer)
        : base(context, configuration, httpContextAccessor, hostingEnvironment)
    {
        this.basicPageLocalizer = basicPageLocalizer;
    }
    public async Task AddNewsAsync(NewsFormDto newsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<NewsModel>(m => m.IncrementNumber);
        var x = await context.News.AddAsync(new NewsModel
        {
            Body = newsFormDto.Body,
            Title = newsFormDto.Title,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.N, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.NewsId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateNewsAsync(NewsFormDto newsFormDto, Guid id)
    {
        var newsModel = await context.News.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.NewsNotFound]);

        await context.Database.BeginTransactionAsync();
        newsModel.Body = newsFormDto.Body;
        newsModel.Title = newsFormDto.Title;

        context.File.RemoveRange(newsModel.Files.Where(f => !newsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.NewsId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveNewsAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.News.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.NewsId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<NewsDto>>> GetAllNewsAsync(int pageSize, int pageNum, NewsSort newsSort, bool isAsc, string search, DateTime? from, DateTime? to)
    {
        Expression<Func<NewsModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Title.Contains(search) || n.Body.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var newsQuery = context.News.Where(expression);
        newsQuery = CustomOrderBy(newsQuery, newsSort, isAsc);

        var newsDto = await newsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).ToListAsync();
        var totalRecords = await newsQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(newsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<NewsDto>> GetNewsAsync(Guid id)
    {
        var newsDto = (await context.News.Where(n => n.IsValid && n.Id == id)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.NewsNotFound]);

        return new(newsDto);
    }

    public async Task AddRegulationsAsync(RegulationsFormDto regulationsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<RegulationsModel>(m => m.IncrementNumber);
        var x = await context.Regulations.AddAsync(new RegulationsModel
        {
            Name = regulationsFormDto.Name,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.CF, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && regulationsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.RegulationsId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateRegulationsAsync(RegulationsFormDto regulationsFormDto, Guid id)
    {
        var regulationsModel = await context.Regulations.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.RegulationsNotFound]);

        await context.Database.BeginTransactionAsync();
        regulationsModel.Name = regulationsFormDto.Name;

        context.File.RemoveRange(regulationsModel.Files.Where(f => !regulationsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && regulationsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.RegulationsId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveRegulationsAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.Regulations.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.RegulationsId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<RegulationsDto>>> GetAllRegulationsAsync(int pageSize, int pageNum, RegulationsSort regulationsSort,
        bool isAsc, string search, DateTime? from, DateTime? to)
    {
        Expression<Func<RegulationsModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Name.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var regulationsQuery = context.Regulations.Where(expression);
        regulationsQuery = CustomOrderBy(regulationsQuery, regulationsSort, isAsc);

        var regulationsDto = await regulationsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new RegulationsDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).ToListAsync();
        var totalRecords = await regulationsQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(regulationsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<RegulationsDto>> GetRegulationsAsync(Guid id)
    {
        var regulationsDto = (await context.Regulations.Where(n => n.IsValid && n.Id == id)
            .Select(n => new RegulationsDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.RegulationsNotFound]);

        return new(regulationsDto);
    }


    public async Task AddOrderFormAsync(RegulationsFormDto regulationsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<OrderFormModel>(m => m.IncrementNumber);
        var x = await context.OrderForm.AddAsync(new OrderFormModel
        {
            Name = regulationsFormDto.Name,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.OF, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && regulationsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.OrderFormId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateOrderFormAsync(RegulationsFormDto regulationsFormDto, Guid id)
    {
        var regulationsModel = await context.OrderForm.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.OrderFormNotFound]);

        await context.Database.BeginTransactionAsync();
        regulationsModel.Name = regulationsFormDto.Name;

        context.File.RemoveRange(regulationsModel.Files.Where(f => !regulationsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && regulationsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.OrderFormId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveOrderFormAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.OrderForm.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.OrderFormId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<RegulationsDto>>> GetAllOrderFormAsync(int pageSize, int pageNum, RegulationsSort regulationsSort,
        bool isAsc, string search, DateTime? from, DateTime? to)
    {
        Expression<Func<OrderFormModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Name.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var regulationsQuery = context.OrderForm.Where(expression);
        regulationsQuery = CustomOrderBy(regulationsQuery, regulationsSort, isAsc);

        var regulationsDto = await regulationsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new RegulationsDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).ToListAsync();
        var totalRecords = await regulationsQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(regulationsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<RegulationsDto>> GetOrderFormAsync(Guid id)
    {
        var regulationsDto = (await context.OrderForm.Where(n => n.IsValid && n.Id == id)
            .Select(n => new RegulationsDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.OrderFormNotFound]);

        return new(regulationsDto);
    }


    public async Task AddMissionAsync(RegulationsFormDto regulationsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<MissionModel>(m => m.IncrementNumber);
        var x = await context.Mission.AddAsync(new MissionModel
        {
            Name = regulationsFormDto.Name,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.M, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && regulationsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.MissionId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateMissionAsync(RegulationsFormDto regulationsFormDto, Guid id)
    {
        var regulationsModel = await context.Mission.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.MissionNotFound]);

        await context.Database.BeginTransactionAsync();
        regulationsModel.Name = regulationsFormDto.Name;

        context.File.RemoveRange(regulationsModel.Files.Where(f => !regulationsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && regulationsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.MissionId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveMissionAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.Mission.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.MissionId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<RegulationsDto>>> GetAllMissionAsync(int pageSize, int pageNum, RegulationsSort regulationsSort,
        bool isAsc, string search, DateTime? from, DateTime? to)
    {
        Expression<Func<MissionModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Name.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var regulationsQuery = context.Mission.Where(expression);
        regulationsQuery = CustomOrderBy(regulationsQuery, regulationsSort, isAsc);

        var regulationsDto = await regulationsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new RegulationsDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).ToListAsync();
        var totalRecords = await regulationsQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(regulationsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<RegulationsDto>> GetMissionAsync(Guid id)
    {
        var regulationsDto = (await context.Mission.Where(n => n.IsValid && n.Id == id)
            .Select(n => new RegulationsDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.MissionNotFound]);

        return new(regulationsDto);
    }


    public async Task AddFaqAsync(FaqFormDto faqFormDto)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<FaqModel>(m => m.IncrementNumber);
        var x = await context.Faq.AddAsync(new FaqModel
        {
            Answer = faqFormDto.Answer,
            Question = faqFormDto.Question,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.FQ, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
    }
    public async Task UpdateFaqAsync(FaqFormDto faqFormDto, Guid id)
    {
        var faqModel = await context.Faq.Where(n => n.IsValid && n.Id == id).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.FaqNotFound]);

        faqModel.Answer = faqFormDto.Answer;
        faqModel.Question = faqFormDto.Question;

        await context.SaveChangesAsync();
    }
    public async Task RemoveFaqAsync(Guid id) => await context.Faq.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    public async Task<CommonResponseDto<List<FaqDto>>> GetAllFaqAsync(int pageSize, int pageNum, FaqSort faqSort, bool isAsc, string search, DateTime? from, DateTime? to)
    {
        Expression<Func<FaqModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Question.Contains(search) || n.Answer.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var faqQuery = context.Faq.Where(expression);
        faqQuery = CustomOrderBy(faqQuery, faqSort, isAsc);

        var faqsDto = await faqQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new FaqDto
            {
                Id = n.Id,
                Answer = n.Answer,
                DocCode = n.DocCode,
                Question = n.Question,
                CreateDate = n.CreateDate,
            }).ToListAsync();
        var totalRecords = await faqQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(faqsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<FaqDto>> GetFaqAsync(Guid id)
    {
        var faqDto = (await context.Faq.Where(n => n.IsValid && n.Id == id)
            .Select(n => new FaqDto
            {
                Id = n.Id,
                Answer = n.Answer,
                DocCode = n.DocCode,
                Question = n.Question,
                CreateDate = n.CreateDate
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.FaqNotFound]);

        return new(faqDto);
    }

    public async Task AddHealthInsuranceAsync(NewsFormDto newsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<HealthInsuranceModel>(m => m.IncrementNumber);
        var x = await context.HealthInsurance.AddAsync(new HealthInsuranceModel
        {
            Body = newsFormDto.Body,
            Title = newsFormDto.Title,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.HI, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.HealthInsuranceId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateHealthInsuranceAsync(NewsFormDto newsFormDto, Guid id)
    {
        var newsModel = await context.HealthInsurance.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.HealthInsuranceNotFound]);

        await context.Database.BeginTransactionAsync();
        newsModel.Body = newsFormDto.Body;
        newsModel.Title = newsFormDto.Title;

        context.File.RemoveRange(newsModel.Files.Where(f => !newsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.HealthInsuranceId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveHealthInsuranceAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.HealthInsurance.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.HealthInsuranceId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<NewsDto>>> GetAllHealthInsuranceAsync(int pageSize, int pageNum, NewsSort newsSort, bool isAsc, string search, DateTime? from, DateTime? to)
    {
        Expression<Func<HealthInsuranceModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Title.Contains(search) || n.Body.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var newsQuery = context.HealthInsurance.Where(expression);
        newsQuery = CustomOrderBy(newsQuery, newsSort, isAsc);

        var newsDto = await newsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).ToListAsync();
        var totalRecords = await newsQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(newsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<NewsDto>> GetHealthInsuranceAsync()
    {
        var newsDto = (await context.HealthInsurance.Where(n => n.IsValid)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) /*?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.HealthInsuranceNotFound]);*/;

        return new(newsDto);
    }


    public async Task AddFinancialDuesAsync(NewsFormDto newsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<FinancialDuesModel>(m => m.IncrementNumber);
        var x = await context.FinancialDues.AddAsync(new FinancialDuesModel
        {
            Body = newsFormDto.Body,
            Title = newsFormDto.Title,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.FD, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.FinancialDuesId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateFinancialDuesAsync(NewsFormDto newsFormDto, Guid id)
    {
        var newsModel = await context.FinancialDues.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.FinancialDuesNotFound]);

        await context.Database.BeginTransactionAsync();
        newsModel.Body = newsFormDto.Body;
        newsModel.Title = newsFormDto.Title;

        context.File.RemoveRange(newsModel.Files.Where(f => !newsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.FinancialDuesId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveFinancialDuesAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.FinancialDues.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.FinancialDuesId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<NewsDto>>> GetAllFinancialDuesAsync(int pageSize, int pageNum, NewsSort newsSort, bool isAsc, string search, DateTime? from, DateTime? to)
    {
        Expression<Func<FinancialDuesModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Title.Contains(search) || n.Body.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var newsQuery = context.FinancialDues.Where(expression);
        newsQuery = CustomOrderBy(newsQuery, newsSort, isAsc);

        var newsDto = await newsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).ToListAsync();
        var totalRecords = await newsQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(newsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<NewsDto>> GetFinancialDuesAsync()
    {
        var newsDto = (await context.FinancialDues.Where(n => n.IsValid)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) /*?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.FinancialDuesNotFound]);*/;

        return new(newsDto);
    }


    public async Task AddTechSupportAsync(NewsFormDto newsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<TechSupportModel>(m => m.IncrementNumber);
        var x = await context.TechSupport.AddAsync(new TechSupportModel
        {
            Body = newsFormDto.Body,
            Title = newsFormDto.Title,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.TS, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.TechSupportId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateTechSupportAsync(NewsFormDto newsFormDto, Guid id)
    {
        var newsModel = await context.TechSupport.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.TechSupportNotFound]);

        await context.Database.BeginTransactionAsync();
        newsModel.Body = newsFormDto.Body;
        newsModel.Title = newsFormDto.Title;

        context.File.RemoveRange(newsModel.Files.Where(f => !newsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.TechSupportId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveTechSupportAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.TechSupport.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.TechSupportId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }

    public async Task<CommonResponseDto<NewsDto>> GetTechSupportAsync()
    {
        var newsDto = (await context.TechSupport.Where(n => n.IsValid)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) /*?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.FinancialDuesNotFound]);*/;

        return new(newsDto);
    }



    public async Task AddCalendarAsync(NewsFormDto newsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<CalendarModel>(m => m.IncrementNumber);
        var x = await context.Calendar.AddAsync(new CalendarModel
        {
            Body = newsFormDto.Body,
            Title = newsFormDto.Title,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.CA, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.CalendarId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateCalendarAsync(NewsFormDto newsFormDto, Guid id)
    {
        var newsModel = await context.Calendar.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.CalendarNotFound]);

        await context.Database.BeginTransactionAsync();
        newsModel.Body = newsFormDto.Body;
        newsModel.Title = newsFormDto.Title;

        context.File.RemoveRange(newsModel.Files.Where(f => !newsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.CalendarId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveCalendarAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.TechSupport.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.CalendarId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }

    public async Task<CommonResponseDto<NewsDto>> GetCalendarAsync()
    {
        var newsDto = (await context.Calendar.Where(n => n.IsValid)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) /*?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.FinancialDuesNotFound]);*/;

        return new(newsDto);
    }

    public async Task AddAdvantagesHigherEducationAsync(NewsFormDto newsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<AdvantagesHigherEducationModel>(m => m.IncrementNumber);
        var x = await context.AdvantagesHigherEducation.AddAsync(new AdvantagesHigherEducationModel
        {
            Body = newsFormDto.Body,
            Title = newsFormDto.Title,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.AHE, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.AdvantagesHigherEducationId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateAdvantagesHigherEducationAsync(NewsFormDto newsFormDto, Guid id)
    {
        var newsModel = await context.AdvantagesHigherEducation.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.AdvantagesHigherEducationNotFound]);

        await context.Database.BeginTransactionAsync();
        newsModel.Body = newsFormDto.Body;
        newsModel.Title = newsFormDto.Title;

        context.File.RemoveRange(newsModel.Files.Where(f => !newsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.AdvantagesHigherEducationId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveAdvantagesHigherEducationAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.AdvantagesHigherEducation.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.AdvantagesHigherEducationId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<NewsDto>>> GetAllAdvantagesHigherEducationAsync(int pageSize, int pageNum, NewsSort newsSort, bool isAsc, string search, DateTime? from, DateTime? to)
    {
        Expression<Func<AdvantagesHigherEducationModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Title.Contains(search) || n.Body.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var newsQuery = context.AdvantagesHigherEducation.Where(expression);
        newsQuery = CustomOrderBy(newsQuery, newsSort, isAsc);

        var newsDto = await newsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).ToListAsync();
        var totalRecords = await newsQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(newsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<NewsDto>> GetAdvantagesHigherEducationAsync()
    {
        var newsDto = (await context.AdvantagesHigherEducation.Where(n => n.IsValid)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) /*?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.AdvantagesHigherEducationNotFound]);*/;

        return new(newsDto);
    }


    public async Task AddGovernmentCommunicationAsync(NewsFormDto newsFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<GovernmentCommunicationModel>(m => m.IncrementNumber);
        var x = await context.GovernmentCommunication.AddAsync(new GovernmentCommunicationModel
        {
            Body = newsFormDto.Body,
            Title = newsFormDto.Title,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.GC, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.GovernmentCommunicationId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateGovernmentCommunicationAsync(NewsFormDto newsFormDto, Guid id)
    {
        var newsModel = await context.GovernmentCommunication.Where(n => n.IsValid && n.Id == id).Include(n => n.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(basicPageLocalizer[ErrorMessages.GovernmentCommunicationNotFound]);

        await context.Database.BeginTransactionAsync();
        newsModel.Body = newsFormDto.Body;
        newsModel.Title = newsFormDto.Title;

        context.File.RemoveRange(newsModel.Files.Where(f => !newsFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && newsFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.GovernmentCommunicationId, id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveGovernmentCommunicationAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.GovernmentCommunication.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.File.Where(n => n.GovernmentCommunicationId == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<NewsDto>>> GetAllGovernmentCommunicationAsync(int pageSize, int pageNum, NewsSort newsSort, bool isAsc, string search, DateTime? from, DateTime? to)
    {
        Expression<Func<GovernmentCommunicationModel, bool>> expression = n => n.IsValid && (string.IsNullOrEmpty(search) || n.Title.Contains(search) || n.Body.Contains(search))
        && (!from.HasValue || n.CreateDate >= from)
        && (!to.HasValue || n.CreateDate <= to);

        var newsQuery = context.GovernmentCommunication.Where(expression);
        newsQuery = CustomOrderBy(newsQuery, newsSort, isAsc);

        var newsDto = await newsQuery
            .Skip(pageSize * pageNum)
            .Take(pageSize)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).ToListAsync();
        var totalRecords = await newsQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(newsDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<NewsDto>> GetGovernmentCommunicationAsync()
    {
        var newsDto = (await context.GovernmentCommunication.Where(n => n.IsValid)
            .Select(n => new NewsDto
            {
                Id = n.Id,
                Body = n.Body,
                Title = n.Title,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                Files = n.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType,
                }).ToList()
            }).FirstOrDefaultAsync()) /*?? throw new NotFoundException(basicPageLocalizer[ErrorMessages.GovernmentCommunicationNotFound]);*/;

        return new(newsDto);
    }
}