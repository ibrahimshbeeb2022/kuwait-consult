﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models.Office;
using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Office;
using KuwaitConsulate.Shared.Enum;
using KuwaitConsulate.Shared.Exception;
using KuwaitConsulate.Shared.Helper;
using KuwaitConsulate.Shared.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System.Linq.Expressions;

namespace KuwaitConsulate.Repository.Office;

public class OfficeRepo : BaseRepo, IOfficeRepo
{
    private readonly IStringLocalizer<EmployeeResource> employeeLocalizer;
    public OfficeRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment, IStringLocalizer<EmployeeResource> employeeLocalizer)
        : base(context, configuration, httpContextAccessor, hostingEnvironment)
    {
        this.employeeLocalizer = employeeLocalizer;
    }
    public async Task UpdateAboutOfficeAsync(string text)
    {
        await context.Database.BeginTransactionAsync();
        await context.AboutOffice.ExecuteDeleteAsync();
        await context.AboutOffice.AddAsync(new AboutOfficeModel { Text = text });
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<string>> GetAboutOfficeAsync()
    {
        var text = await context.AboutOffice.Select(a => a.Text).FirstOrDefaultAsync();
        return new(text);
    }
    public async Task AddOfficeEmployeeAsync(OfficeEmployeeFormDto officeEmployeeFormDto)
    {
        await context.Database.BeginTransactionAsync();
        var NextIncrementNumber = await GetNextIncrementNumber<OfficeEmployeeModel>(m => m.IncrementNumber);
        var x = await context.OfficeEmployee.AddAsync(new OfficeEmployeeModel
        {
            Name = officeEmployeeFormDto.Name,
            Email = officeEmployeeFormDto.Email,
            IncrementNumber = NextIncrementNumber,
            JobTitle = officeEmployeeFormDto.JobTitle,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.OE, NextIncrementNumber),
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => f.IsValid && officeEmployeeFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.OfficeEmployeeId, x.Entity.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task UpdateOfficeEmployeeAsync(OfficeEmployeeFormDto officeEmployeeFormDto, Guid id)
    {
        await context.Database.BeginTransactionAsync();

        var officeEmployeeModel = await context.OfficeEmployee.Where(of => of.IsValid && of.Id == id).Include(of => of.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(employeeLocalizer[ErrorMessages.OfficeEmployeeNotFound]);

        officeEmployeeModel.Name = officeEmployeeFormDto.Name;
        officeEmployeeModel.Email = officeEmployeeFormDto.Email;
        officeEmployeeModel.JobTitle = officeEmployeeFormDto.JobTitle;


        context.File.RemoveRange(officeEmployeeModel.Files.Where(f => !officeEmployeeFormDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => f.IsValid && officeEmployeeFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.OfficeEmployeeId, officeEmployeeModel.Id));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveOfficeEmployeeAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        await context.OfficeEmployee.Where(of => of.Id == id).ExecuteUpdateAsync(of => of.SetProperty(of => of.IsValid, false));
        await context.File.Where(of => of.OfficeEmployeeId == id).ExecuteUpdateAsync(of => of.SetProperty(of => of.IsValid, false));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<OfficeEmployeeDto>>> GetAllOfficeEmployeeAsync(int pageSize, int pageNum, string search,
        OfficeEmployeeSort officeEmployeeSort, bool isAsc, DateTime? from, DateTime? to)
    {
        Expression<Func<OfficeEmployeeModel, bool>> expression = of => of.IsValid && (string.IsNullOrEmpty(search) || of.Name.Contains(search) || of.Email.Contains(search)
        || of.DocCode.Contains(search) || of.JobTitle.Contains(search))
        && (!from.HasValue || of.CreateDate >= from)
        && (!to.HasValue || of.CreateDate <= to);

        var officeEmployeesQuery = context.OfficeEmployee.Where(expression);
        officeEmployeesQuery = CustomOrderBy(officeEmployeesQuery, officeEmployeeSort, isAsc);

        var officeEmployeesDto = await officeEmployeesQuery
            .Skip(pageNum * pageSize)
            .Take(pageSize)
            .Select(of => new OfficeEmployeeDto
            {
                Id = of.Id,
                Name = of.Name,
                Email = of.Email,
                DocCode = of.DocCode,
                JobTitle = of.JobTitle,
                CreateDate = of.CreateDate,
                Files = of.Files.Where(f => f.IsValid).Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).ToListAsync();

        var totalRecords = await officeEmployeesQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(officeEmployeesDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<OfficeEmployeeDto>> GetOfficeEmployeeAsync(Guid id)
    {
        var officeEmployeesDto = (await context.OfficeEmployee.Where(of => of.Id == id && of.IsValid)
            .Select(of => new OfficeEmployeeDto
            {
                Id = of.Id,
                Name = of.Name,
                Email = of.Email,
                DocCode = of.DocCode,
                JobTitle = of.JobTitle,
                CreateDate = of.CreateDate,
                Files = of.Files.Where(f => f.IsValid).Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileType = f.FileType,
                    FileKind = f.FileKind
                }).ToList()
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(employeeLocalizer[ErrorMessages.OfficeEmployeeNotFound]);
        return new(officeEmployeesDto);
    }
}