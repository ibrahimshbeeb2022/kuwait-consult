﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Office;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Repository.Office;

public interface IOfficeRepo
{
    Task AddOfficeEmployeeAsync(OfficeEmployeeFormDto officeEmployeeFormDto);
    Task<CommonResponseDto<string>> GetAboutOfficeAsync();
    Task<CommonResponseDto<List<OfficeEmployeeDto>>> GetAllOfficeEmployeeAsync(int pageSize, int pageNum, string search, OfficeEmployeeSort officeEmployeeSort, bool isAsc
        , DateTime? from, DateTime? to);
    Task<CommonResponseDto<OfficeEmployeeDto>> GetOfficeEmployeeAsync(Guid id);
    Task RemoveOfficeEmployeeAsync(Guid id);
    Task UpdateAboutOfficeAsync(string text);
    Task UpdateOfficeEmployeeAsync(OfficeEmployeeFormDto officeEmployeeFormDto, Guid id);
}