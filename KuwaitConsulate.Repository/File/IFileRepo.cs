﻿
using KuwaitConsulate.Dto;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Http;

namespace KuwaitConsulate.Repository.File;

public interface IFileRepo
{
    Task DeleteFile(Guid id);
    Task<FileDto> SaveFile(IFormFile file, FileKind fileKind);
    Task UpdateCarouselItemAsync(Guid fileId, string url);
}