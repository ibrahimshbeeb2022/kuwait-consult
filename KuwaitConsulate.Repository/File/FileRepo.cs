﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models;
using KuwaitConsulate.Dto;
using KuwaitConsulate.Shared.Enum;
using KuwaitConsulate.Shared.Exception;
using KuwaitConsulate.Shared.Helper;
using KuwaitConsulate.Shared.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace KuwaitConsulate.Repository.File;

public class FileRepo : BaseRepo, IFileRepo
{
    private readonly IStringLocalizer<SharedResource> sharedLocalizer;
    public FileRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment, IStringLocalizer<SharedResource> sharedLocalizer)
        : base(context, configuration, httpContextAccessor, hostingEnvironment)
    {
        this.sharedLocalizer = sharedLocalizer;
    }
    public async Task DeleteFile(Guid id)
    {
        var fileModel = await context.File.Where(m => m.Id == id).FirstOrDefaultAsync() ?? throw new NotFoundException(sharedLocalizer[ErrorMessages.FileNotFound]);
        var urls = new List<string>() { fileModel.Url };
        FileManagement.DeleteFiles(urls.AsEnumerable());

        context.Remove(fileModel);
        await context.SaveChangesAsync();
    }
    public async Task UpdateCarouselItemAsync(Guid fileId, string url)
        => await context.File.Where(f => f.IsValid && f.Id == fileId).ExecuteUpdateAsync(f => f.SetProperty(f => f.Url, url).SetProperty(f => f.FileKind, FileKind.CarouselItem));

    public async Task<FileDto> SaveFile(IFormFile file, FileKind fileKind)
    {
        var uploadResult = await FileManagement.SaveFiles([file], hostingEnvironment.WebRootPath);

        var fileModel = new FileModel
        {
            Id = Guid.NewGuid(),
            FileKind = fileKind,
            Name = file.FileName,
            Url = uploadResult.Select(f => f.url.FileUrlSimplify()).FirstOrDefault(),
            FileType = uploadResult.Select(f => f.fileType).FirstOrDefault(),
        };
        await context.File.AddAsync(fileModel);
        await context.SaveChangesAsync();
        return new FileDto
        {
            Id = fileModel.Id,
            Url = fileModel.Url,
            Name = fileModel.Name,
            FileKind = fileModel.FileKind,
            FileType = fileModel.FileType
        };
    }
}