﻿using KuwaitConsulate.Dto.Mail;

namespace KuwaitConsulate.Repository.Mail;

public interface IMailService
{
    public Task<string> SendEmailAsync(MailRequest mailRequest);
}