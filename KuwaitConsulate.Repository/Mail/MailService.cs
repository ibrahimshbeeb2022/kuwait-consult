﻿using KuwaitConsulate.Dto.Helper;
using KuwaitConsulate.Dto.Mail;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;

namespace KuwaitConsulate.Repository.Mail;

public class MailService : IMailService
{
    private readonly MailSettings _mailSettings;
    public MailService(IOptions<MailSettings> mailSettings)
    {
        _mailSettings = mailSettings.Value;
    }
    public async Task<string> SendEmailAsync(MailRequest mailRequest)
    {
        string response = string.Empty;
        try
        {
            var email = new MimeMessage
            {
                //Sender = MailboxAddress.Parse(_mailSettings.Username),
                Subject = mailRequest.Subject,
            };
            email.From.Add(new MailboxAddress(_mailSettings.DisplayName, _mailSettings.Username));

            foreach (string toEmail in mailRequest.ToEmails)
                email.To.Add(MailboxAddress.Parse(toEmail));

            var builder = new BodyBuilder();
            if (mailRequest.Attachments.Count != 0)
            {
                //byte[] fileBytes;
                foreach (var file in mailRequest.Attachments)
                {
                    var ms = new MemoryStream();

                    await file.CopyToAsync(ms);
                    ms.Position = 0;
                    //fileBytes = ms.ToArray();
                    await builder.Attachments.AddAsync(file.FileName, ms, ContentType.Parse(file.ContentType));
                    await ms.DisposeAsync();
                }
            }
            builder.HtmlBody = mailRequest.Body;
            email.Body = builder.ToMessageBody();
            var smtp = new SmtpClient();
            await smtp.ConnectAsync(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.SslOnConnect);
            await smtp.AuthenticateAsync(_mailSettings.Username, _mailSettings.Password);
            response = await smtp.SendAsync(email);
            await smtp.DisconnectAsync(true);
            return response;
        }
        catch (Exception)
        {
            return response;
        }
    }
}