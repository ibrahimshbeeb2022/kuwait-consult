﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Token;
using KuwaitConsulate.Dto.User;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Repository.User;

public interface IUserRepo
{
    Task ActiveUserAsync(Guid userId);
    Task ChangePasswordAsync(ChangePasswordDto changePasswordDto);
    Task<bool> CheckIfUserActiveAsync(Guid userId);
    Task ConfirmAccountAsync(string token, string email);
    Task DeactivateUserAsync(Guid userId);
    Task<CommonResponseDto<List<UserDto>>> GetAllAsync(List<AccountStatus> accountStatuses, string search, int pageSize, int pageNum, UserSort userSort, bool isAsc
        , DateTime? from, DateTime? to);
    Task<CommonResponseDto<UserDto>> GetAsync(Guid userId);
    Task<CommonResponseDto<UserInfoDto>> GetUserInfoAsync();
    Task<CommonResponseDto<UserStatusDto>> GetUserStatusAsync();
    TokenDto RefreshToken();
    Task RejectUserAsync(Guid userId, string reason);
    Task RequestConfirmEmailAsync(string email);
    Task RequestResetPasswordAsync(string email);
    Task ReSendConfirmTokenAsync(string email);
    Task ResetPasswordAsync(string email, string token, string newPassword);
    Task ReSubmitUserAsync(SignUpDto signUpDto, Guid userId);
    Task<CommonResponseDto<UserSignInDto>> SignInAsync(string civilNo, string password);
    Task SignUpAsync(SignUpDto signUpDto);
    Task UpdateInfoAsync(UserInfoFormDto userInfoDto);
}