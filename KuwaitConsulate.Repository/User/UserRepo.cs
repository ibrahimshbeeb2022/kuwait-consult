﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models.User;
using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Mail;
using KuwaitConsulate.Dto.Token;
using KuwaitConsulate.Dto.User;
using KuwaitConsulate.Repository.Mail;
using KuwaitConsulate.Shared.Enum;
using KuwaitConsulate.Shared.Exception;
using KuwaitConsulate.Shared.Helper;
using KuwaitConsulate.Shared.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System.Linq.Expressions;

namespace KuwaitConsulate.Repository.User;

public class UserRepo : BaseRepo, IUserRepo
{
    private readonly IMailService mailService;
    private readonly IStringLocalizer<UserResource> userLocalizer;
    private readonly IStringLocalizer<SharedResource> sharedLocalizer;
    public UserRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor,
        IStringLocalizer<UserResource> userLocalizer, IStringLocalizer<SharedResource> sharedLocalizer, IHostingEnvironment hostingEnvironment, IMailService mailService)
        : base(context, configuration, httpContextAccessor, hostingEnvironment)
    {
        this.userLocalizer = userLocalizer;
        this.sharedLocalizer = sharedLocalizer;
        this.mailService = mailService;
    }
    public async Task SignUpAsync(SignUpDto signUpDto)
    {
        signUpDto.Email = signUpDto.Email.Trim().ToLower();
        var NextIncrementNumber = await GetNextIncrementNumber<UserModel>(m => m.IncrementNumber);

        if (await CheckEntityExist<UserModel>(u => u.Username == signUpDto.Username && u.IsValid))
            throw new ValidException(userLocalizer[ErrorMessages.UsernameExist]);

        if (await CheckEntityExist<UserModel>(u => u.Email == signUpDto.Email && u.IsValid))
            throw new ValidException(userLocalizer[ErrorMessages.EmailExist]);

        if (await CheckEntityExist<UserModel>(u => u.CivilNo == signUpDto.CivilNo && u.IsValid))
            throw new ValidException(userLocalizer[ErrorMessages.CivilNoExist]);

        var hashPassword = HashPassword(signUpDto.Password);
        var confirmOTP = StringHelper.GenerateRandomNumber(6);
        UserModel userModel = new()
        {
            Email = signUpDto.Email,
            ConfirmOTP = confirmOTP,
            CivilNo = signUpDto.CivilNo,
            HashPassword = hashPassword,
            Username = signUpDto.Username,
            PhoneNumber = signUpDto.PhoneNumber,
            IncrementNumber = NextIncrementNumber,
            AccountStatus = AccountStatus.Inactive,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.U, NextIncrementNumber),
        };
        await context.User.AddAsync(userModel);
        await context.SaveChangesAsync();

        try
        {
            var stream = new StreamReader(hostingEnvironment.WebRootPath + Constants.ConfirmAccountTemplatePath);
            var body = await stream.ReadToEndAsync();
            stream.Close();

            body = string.Format(body, userModel.Username, confirmOTP);
            mailService.SendEmailAsync(new MailRequest
            {
                Body = body,
                Subject = "Confirm account",
                ToEmails = [signUpDto.Email]
            });
        }
        catch (Exception)
        {
        }
    }
    public async Task<CommonResponseDto<UserSignInDto>> SignInAsync(string civilNo, string password)
    {
        var userModel = await context.User.Where(u => u.IsValid && u.CivilNo == civilNo).FirstOrDefaultAsync() ??
            throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);

        var result = VerifyHashedPassword(userModel.HashPassword, password);
        if (result != PasswordVerificationResult.Success)
            throw new ValidException(userLocalizer[ErrorMessages.WrongCivilOrPassword]);

        if (!userModel.EmailConfirmed)
            throw new NotConfirmAccountException(userLocalizer[ErrorMessages.EmailNotConfirmed]);

        //if (userModel.AccountStatus == AccountStatus.Inactive)
        //    throw new ValidException(userLocalizer[ErrorMessages.UserInActive]);

        //if (userModel.AccountStatus == AccountStatus.Pending)
        //    throw new ValidException(userLocalizer[ErrorMessages.UserIsPending]);

        var signInDto = new UserSignInDto
        {
            Id = userModel.Id,
            CivilNo = civilNo,
            Email = userModel.Email,
            DocCode = userModel.DocCode,
            Username = userModel.Username,
            PhoneNumber = userModel.PhoneNumber,
            RejectReason = userModel.RejectReason,
            AccountStatus = userModel.AccountStatus,
            Token = userModel.AccountStatus != AccountStatus.Rejected ? GenerateJwtTokenAsync(userModel.Id, UserType.NormalUser) : null
        };

        return new(signInDto);
    }
    public async Task ActiveUserAsync(Guid userId)
    {
        var userModel = await context.User.Where(u => u.IsValid && u.Id == userId).FirstOrDefaultAsync() ??
            throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);

        userModel.AccountStatus = AccountStatus.Active;
        await context.SaveChangesAsync();


        try
        {
            var stream = new StreamReader(hostingEnvironment.WebRootPath + Constants.ActiveAccountTemplatePath);
            var body = await stream.ReadToEndAsync();
            stream.Close();
            body = string.Format(body, userModel.FirstNameAr + " " + userModel.SecondNameAr);
            mailService.SendEmailAsync(new MailRequest
            {
                Body = body,
                Subject = "Active account",
                ToEmails = [userModel.Email]
            });
        }
        catch (Exception)
        {
        }
    }
    public async Task DeactivateUserAsync(Guid userId)
    {
        var userModel = await context.User.Where(u => u.IsValid && u.Id == userId).FirstOrDefaultAsync() ??
            throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);

        userModel.AccountStatus = AccountStatus.Inactive;
        await context.SaveChangesAsync();
    }
    public async Task RejectUserAsync(Guid userId, string reason)
    {
        var userModel = await context.User.Where(u => u.IsValid && u.Id == userId).FirstOrDefaultAsync() ??
            throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);

        userModel.AccountStatus = AccountStatus.Rejected;
        userModel.RejectReason = reason;
        await context.SaveChangesAsync();

        try
        {
            var stream = new StreamReader(hostingEnvironment.WebRootPath + Constants.RejectAccountTemplatePath);
            var body = await stream.ReadToEndAsync();
            stream.Close();

            body = string.Format(body, userModel.Username, reason);
            mailService.SendEmailAsync(new MailRequest
            {
                Body = body,
                Subject = "Reject account",
                ToEmails = [userModel.Email]
            });
        }
        catch (Exception)
        {
        }
    }
    public async Task ReSubmitUserAsync(SignUpDto signUpDto, Guid userId)
    {
        var userModel = await context.User.Where(u => u.IsValid && u.Id == userId).FirstOrDefaultAsync() ??
            throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);

        signUpDto.Email = signUpDto.Email.Trim().ToLower();
        if (userModel.AccountStatus != AccountStatus.Rejected)
            throw new AccessViolationException(sharedLocalizer[ErrorMessages.NoAccess]);

        if (await CheckEntityExist<UserModel>(u => u.Username == signUpDto.Username && u.IsValid && u.Id != userId))
            throw new ValidException(userLocalizer[ErrorMessages.UsernameExist]);

        if (await CheckEntityExist<UserModel>(u => u.CivilNo == signUpDto.CivilNo && u.IsValid && u.Id != userId))
            throw new ValidException(userLocalizer[ErrorMessages.CivilNoExist]);

        if (await CheckEntityExist<UserModel>(u => u.Email == signUpDto.Email && u.IsValid && u.Id != userId))
            throw new ValidException(userLocalizer[ErrorMessages.EmailExist]);

        userModel.Email = signUpDto.Email;
        userModel.CivilNo = signUpDto.CivilNo;
        userModel.Username = signUpDto.Username;
        userModel.PhoneNumber = signUpDto.PhoneNumber;
        userModel.AccountStatus = AccountStatus.Pending;

        await context.SaveChangesAsync();
    }
    public async Task<bool> CheckIfUserActiveAsync(Guid userId) => await CheckEntityExist<UserModel>(u => u.Id == userId && u.IsValid && u.AccountStatus == AccountStatus.Inactive);
    public async Task<CommonResponseDto<List<UserDto>>> GetAllAsync(List<AccountStatus> accountStatuses, string search, int pageSize, int pageNum, UserSort userSort, bool isAsc
        , DateTime? from, DateTime? to)
    {
        Expression<Func<UserModel, bool>> expression = u => u.IsValid && (string.IsNullOrEmpty(search) || u.Email.Contains(search) || u.Username.Contains(search)
        || u.CivilNo.Contains(search) || u.DocCode.Contains(search) || u.PhoneNumber.Contains(search))
        && (accountStatuses.Count == 0 || accountStatuses.Contains(u.AccountStatus))
        && (!from.HasValue || u.CreateDate >= from)
        && (!to.HasValue || u.CreateDate <= to);

        var userQuery = context.User.Where(expression);
        userQuery = CustomOrderBy(userQuery, userSort, isAsc);

        var usersDto = await userQuery
            .Skip(pageNum * pageSize)
            .Take(pageSize)
            .Select(u => new UserDto
            {
                Id = u.Id,
                Email = u.Email,
                DocCode = u.DocCode,
                CivilNo = u.CivilNo,
                Username = u.Username,
                CreateDate = u.CreateDate,
                PhoneNumber = u.PhoneNumber,
                RejectReason = u.RejectReason,
                AccountStatus = u.AccountStatus,
                EmailConfirmed = u.EmailConfirmed
            }).ToListAsync();

        var totalRecords = await userQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);

        return new(usersDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<UserDto>> GetAsync(Guid userId)
    {
        var userDto = (await context.User.Where(u => u.IsValid && u.Id == userId)
            .Select(u => new UserDto
            {
                Id = u.Id,
                Email = u.Email,
                DocCode = u.DocCode,
                CivilNo = u.CivilNo,
                Username = u.Username,
                CreateDate = u.CreateDate,
                PhoneNumber = u.PhoneNumber,
                RejectReason = u.RejectReason,
                AccountStatus = u.AccountStatus,
                AddressKuwait = u.AddressKuwait,
                AddressUAE = u.AddressUAE,
                BirthDate = u.BirthDate,
                FirstNameAr = u.FirstNameAr,
                FirstNameEn = u.FirstNameEn,
                FourthNameAr = u.FourthNameAr,
                FourthNameEn = u.FourthNameEn,
                EmergencyPhoneNumber = u.EmergencyPhoneNumber,
                ThirdNameEn = u.ThirdNameEn,
                ThirdNameAr = u.FourthNameAr,
                SocialStatus = u.SocialStatus,
                SecondNameEn = u.SecondNameEn,
                SecondNameAr = u.SecondNameAr,
                PhoneNumberUAE = u.PhoneNumberUAE,
                PhoneNumberKuwait = u.PhoneNumberKuwait,
                PassportNo = u.PassportNo,
                EmailConfirmed = u.EmailConfirmed,
                Gender = u.Gender,
                PassportExpireDate = u.PassportExpireDate,
                Files = u.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileType = f.FileType,
                    FileKind = f.FileKind
                }).ToList()
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);

        return new(userDto);
    }
    public async Task RequestConfirmEmailAsync(string email)
    {
        try
        {
            email = email.Trim().ToLower();
            var token = StringHelper.GenerateRandomNumber(6);
            var userModel = await context.User.Where(u => (u.Email == email || u.CivilNo == email)).FirstOrDefaultAsync() ??
                throw new ValidException("Email not exit");

            userModel.ConfirmOTP = token;
            await context.SaveChangesAsync();
            try
            {
                var stream = new StreamReader(hostingEnvironment.WebRootPath + Constants.ConfirmAccountTemplatePath);
                var body = await stream.ReadToEndAsync();
                stream.Close();

                body = string.Format(body, userModel.Username, token);
                mailService.SendEmailAsync(new MailRequest
                {
                    Body = body,
                    Subject = "Confirm account",
                    ToEmails = [userModel.Email]
                });
            }
            catch (Exception)
            {
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
    public async Task RequestResetPasswordAsync(string email)
    {
        try
        {
            var token = StringHelper.GenerateRandomString(6);
            var userModel = await context.User.Where(u => u.Email == email.Trim().ToLower()).FirstOrDefaultAsync() ??
                throw new ValidException(userLocalizer[ErrorMessages.UserNotFound]);

            userModel.ResetPasswordToken = token;
            await context.SaveChangesAsync();
            try
            {
                var stream = new StreamReader(hostingEnvironment.WebRootPath + Constants.ResetPasswordTemplatePath);
                var body = await stream.ReadToEndAsync();
                stream.Close();

                body = string.Format(body, configuration.GetSection("BaseWebsiteDomain").Value + $"reset-password?email={email}&token={token}");
                mailService.SendEmailAsync(new MailRequest
                {
                    Body = body,
                    Subject = "Reset password",
                    ToEmails = [email]
                });
            }
            catch (Exception)
            {
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
    public async Task ResetPasswordAsync(string email, string token, string newPassword)
    {
        try
        {
            var userModel = await context.User.Where(u => u.Email == email.Trim().ToLower()).FirstOrDefaultAsync();
            if (userModel.ResetPasswordToken != token)
                throw new ValidException(userLocalizer[ErrorMessages.InvalidToken]);

            var passwordHasher = new PasswordHasher<object>();
            userModel.HashPassword = passwordHasher.HashPassword(null, newPassword);
            await context.SaveChangesAsync();
        }
        catch (Exception)
        {
            throw;
        }
    }
    public async Task UpdateInfoAsync(UserInfoFormDto userInfoDto)
    {
        var userModel = await context.User.Where(u => u.Id == CurrentUserId).Include(f => f.Files).FirstOrDefaultAsync() ??
            throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);


        if (userModel.AccountStatus == AccountStatus.Active)
            throw new ValidException(userLocalizer[ErrorMessages.UpdateProfileLocked]);

        await context.Database.BeginTransactionAsync();

        userModel.Email = userInfoDto.Email;

        userModel.FirstNameAr = userInfoDto.FirstNameAr;
        userModel.FirstNameEn = userInfoDto.FirstNameEn;

        userModel.SecondNameAr = userInfoDto.SecondNameAr;
        userModel.SecondNameEn = userInfoDto.SecondNameEn;

        userModel.ThirdNameAr = userInfoDto.ThirdNameAr;
        userModel.ThirdNameEn = userInfoDto.ThirdNameEn;

        userModel.FourthNameAr = userInfoDto.FourthNameAr;
        userModel.FourthNameEn = userInfoDto.FourthNameEn;

        userModel.PassportNo = userInfoDto.PassportNo;

        userModel.PassportExpireDate = userInfoDto.PassportExpireDate;

        userModel.PhoneNumberUAE = userInfoDto.PhoneNumberUAE;

        userModel.PhoneNumberKuwait = userInfoDto.PhoneNumberKuwait;

        userModel.EmergencyPhoneNumber = userInfoDto.EmergencyPhoneNumber;

        userModel.AddressUAE = userInfoDto.AddressUAE;

        userModel.AddressKuwait = userInfoDto.AddressKuwait;

        userModel.Gender = userInfoDto.Gender;

        userModel.BirthDate = userInfoDto.BirthDate;

        userModel.SocialStatus = userInfoDto.SocialStatus;

        userModel.AccountStatus = AccountStatus.Pending;

        context.File.RemoveRange(userModel.Files.Where(f => !userInfoDto.FileIds.Contains(f.Id)));
        await context.File.Where(f => userInfoDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.UserId, CurrentUserId));

        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task ChangePasswordAsync(ChangePasswordDto changePasswordDto)
    {
        var userModel = await context.User.Where(u => u.IsValid && u.Id == CurrentUserId).FirstOrDefaultAsync() ??
            throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);

        var hashPassword = HashPassword(changePasswordDto.OldPassword);
        if (hashPassword != userModel.HashPassword)
            throw new ValidException(userLocalizer[ErrorMessages.OldPasswordWrong]);

        var newHashPassword = HashPassword(changePasswordDto.NewPassword);
        userModel.HashPassword = newHashPassword;
        await context.SaveChangesAsync();
    }
    public async Task ConfirmAccountAsync(string token, string email)
    {
        email = email.Trim().ToLower();
        var userModel = await context.User.Where(u => u.IsValid && (u.Email == email || u.CivilNo == email)).FirstOrDefaultAsync() ??
            throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);

        if (token != userModel.ConfirmOTP)
            throw new ValidException(userLocalizer[ErrorMessages.ExpireOTP]);

        userModel.EmailConfirmed = true;
        await context.SaveChangesAsync();
    }
    public async Task ReSendConfirmTokenAsync(string email)
    {
        email = email.Trim().ToLower();
        var userModel = await context.User.Where(u => u.IsValid && (u.Email == email || u.CivilNo == email)).FirstOrDefaultAsync() ??
            throw new NotFoundException(userLocalizer[ErrorMessages.UserNotFound]);

        var newOtp = StringHelper.GenerateRandomString(6);
        userModel.ConfirmOTP = newOtp;
        await context.SaveChangesAsync();

        try
        {
            var stream = new StreamReader(hostingEnvironment.WebRootPath + Constants.ConfirmAccountTemplatePath);
            var body = await stream.ReadToEndAsync();
            stream.Close();

            body = string.Format(body, newOtp);
            mailService.SendEmailAsync(new MailRequest
            {
                Body = body,
                Subject = "Confirm account",
                ToEmails = [userModel.Email]
            });
        }
        catch (Exception)
        {
        }
    }

    public TokenDto RefreshToken() => GenerateJwtTokenAsync(CurrentUserId, CurrentUserType);


    public async Task<CommonResponseDto<UserInfoDto>> GetUserInfoAsync()
    {
        var userModel = await context.User.Where(u => u.Id == CurrentUserId).Select(u => new UserInfoDto
        {
            AddressKuwait = u.AddressKuwait,
            AddressUAE = u.AddressUAE,
            BirthDate = u.BirthDate,
            Email = u.Email,
            EmergencyPhoneNumber = u.EmergencyPhoneNumber,
            FirstNameAr = u.FirstNameAr,
            Files = u.Files.Select(f => new FileDto
            {
                Id = f.Id,
                Url = f.Url,
                Name = f.Name,
                FileKind = f.FileKind,
                FileType = f.FileType
            }).ToList(),
            FirstNameEn = u.FirstNameEn,
            FourthNameAr = u.FirstNameAr,
            FourthNameEn = u.FirstNameEn,
            Gender = u.Gender,
            PassportExpireDate = u.PassportExpireDate,
            PassportNo = u.PassportNo,
            PhoneNumberKuwait = u.PhoneNumberKuwait,
            PhoneNumberUAE = u.PhoneNumberUAE,
            SecondNameAr = u.SecondNameAr,
            SecondNameEn = u.SecondNameEn,
            SocialStatus = u.SocialStatus,
            ThirdNameAr = u.ThirdNameAr,
            ThirdNameEn = u.ThirdNameEn
        }).FirstOrDefaultAsync();
        return new(userModel);
    }
    public async Task<CommonResponseDto<UserStatusDto>> GetUserStatusAsync()
        => new(await context.User.Where(u => u.Id == CurrentUserId).Select(u => new UserStatusDto
        {
            Status = u.AccountStatus,
            RejectReason = u.RejectReason
        }).FirstOrDefaultAsync());
}