﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models.AccreditedUniversity;
using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.AccreditedUniversity;
using KuwaitConsulate.Shared.Enum;
using KuwaitConsulate.Shared.Exception;
using KuwaitConsulate.Shared.Helper;
using KuwaitConsulate.Shared.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System.Linq.Expressions;

namespace KuwaitConsulate.Repository.AccreditedUniversity;

public class AccreditedUniversityRepo : BaseRepo, IAccreditedUniversityRepo
{
    private readonly IStringLocalizer<AccreditedUniversityResource> accreditedUniversityLocalizer;
    public AccreditedUniversityRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment, IStringLocalizer<AccreditedUniversityResource> accreditedUniversityLocalizer)
        : base(context, configuration, httpContextAccessor, hostingEnvironment)
    {
        this.accreditedUniversityLocalizer = accreditedUniversityLocalizer;
    }
    public async Task<CountryDto> AddCountryAsync(string name)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<CountryModel>(m => m.IncrementNumber);
        var x = await context.Country.AddAsync(new CountryModel
        {
            Name = name,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.C, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        return (await GetCountryAsync(x.Entity.Id)).Data;
    }
    public async Task UpdateCountryAsync(Guid id, string name)
    {
        var countryModel = await context.Country.Where(n => n.IsValid && n.Id == id).FirstOrDefaultAsync() ??
            throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.CountryNotFound]);

        countryModel.Name = name;

        await context.SaveChangesAsync();
    }
    public async Task RemoveCountryAsync(Guid id)
    {
        if (await context.University.AnyAsync(u => u.CountryId == id && u.IsValid))
            throw new ValidException(accreditedUniversityLocalizer[ErrorMessages.CountryHasUniversity]);

        await context.Country.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<CountryDto>>> GetAllCountryAsync()
    {
        Expression<Func<CountryModel, bool>> expression = n => n.IsValid;

        //var countryQuery = context.Country.Where(expression);
        //countryQuery = CustomOrderBy(countryQuery, countrySort, isAsc);

        var countriesDto = await context.Country.Where(expression)
            .Select(n => new CountryDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).ToListAsync();
        return new(countriesDto);
    }
    public async Task<CommonResponseDto<CountryDto>> GetCountryAsync(Guid id)
    {
        var countryDto = (await context.Country.Where(n => n.IsValid && n.Id == id)
            .Select(n => new CountryDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.CountryNotFound]);

        return new(countryDto);
    }

    public async Task<MainSpecialtyDto> AddMainSpecialtyAsync(string name)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<MainSpecialtyModel>(m => m.IncrementNumber);
        var x = await context.MainSpecialty.AddAsync(new MainSpecialtyModel
        {
            Name = name,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.MS, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        return (await GetMainSpecialtyAsync(x.Entity.Id)).Data;
    }
    public async Task UpdateMainSpecialtyAsync(Guid id, string name)
    {
        var mainSpecialtyModel = await context.MainSpecialty.Where(n => n.IsValid && n.Id == id).FirstOrDefaultAsync() ??
            throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.MainSpecialtyNotFound]);

        mainSpecialtyModel.Name = name;

        await context.SaveChangesAsync();
    }
    public async Task RemoveMainSpecialtyAsync(Guid id)
    {
        if (await context.SubSpecialty.AnyAsync(u => u.MainSpecialtyId == id && u.IsValid))
            throw new ValidException(accreditedUniversityLocalizer[ErrorMessages.MainSpecialtyHasSub]);

        await context.MainSpecialty.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<MainSpecialtyDto>>> GetAllMainSpecialtyAsync()
    {
        Expression<Func<MainSpecialtyModel, bool>> expression = n => n.IsValid;

        //var countryQuery = context.Country.Where(expression);
        //countryQuery = CustomOrderBy(countryQuery, countrySort, isAsc);

        var countriesDto = await context.MainSpecialty.Where(expression)
            .Select(n => new MainSpecialtyDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).ToListAsync();
        return new(countriesDto);
    }
    public async Task<CommonResponseDto<MainSpecialtyDto>> GetMainSpecialtyAsync(Guid id)
    {
        var countryDto = (await context.MainSpecialty.Where(n => n.IsValid && n.Id == id)
            .Select(n => new MainSpecialtyDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.MainSpecialtyNotFound]);

        return new(countryDto);
    }

    public async Task<SubSpecialtyDto> AddSubSpecialtyAsync(string name, Guid mainSpecialtyId)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<SubSpecialtyModel>(m => m.IncrementNumber);
        var x = await context.SubSpecialty.AddAsync(new SubSpecialtyModel
        {
            Name = name,
            MainSpecialtyId = mainSpecialtyId,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.SS, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        return (await GetSubSpecialtyAsync(x.Entity.Id)).Data;
    }
    public async Task UpdateSubSpecialtyAsync(Guid id, string name, Guid mainSpecialtyId)
    {
        var subSpecialtyModel = await context.SubSpecialty.Where(n => n.IsValid && n.Id == id).FirstOrDefaultAsync() ??
            throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.SubSpecialtyNotFound]);

        subSpecialtyModel.Name = name;
        subSpecialtyModel.MainSpecialtyId = mainSpecialtyId;

        await context.SaveChangesAsync();
    }
    public async Task RemoveSubSpecialtyAsync(Guid id)
    {
        if (await context.University.AnyAsync(u => u.MainSpecialtyId == id && u.IsValid))
            throw new ValidException(accreditedUniversityLocalizer[ErrorMessages.SubSpecialtyHasUniversity]);

        await context.SubSpecialty.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<SubSpecialtyDto>>> GetAllSubSpecialtyAsync(Guid? mainSpecialtyId)
    {
        Expression<Func<SubSpecialtyModel, bool>> expression = n => n.IsValid && (!mainSpecialtyId.HasValue || n.MainSpecialtyId == mainSpecialtyId);

        //var countryQuery = context.Country.Where(expression);
        //countryQuery = CustomOrderBy(countryQuery, countrySort, isAsc);

        var countriesDto = await context.SubSpecialty.Where(expression)
            .Select(n => new SubSpecialtyDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                MainSpecialty = new MainSpecialtyDto
                {
                    Id = n.MainSpecialty.Id,
                    Name = n.MainSpecialty.Name,
                    DocCode = n.MainSpecialty.DocCode,
                    CreateDate = n.MainSpecialty.CreateDate
                }
            }).ToListAsync();
        return new(countriesDto);
    }
    public async Task<CommonResponseDto<SubSpecialtyDto>> GetSubSpecialtyAsync(Guid id)
    {
        var countryDto = (await context.SubSpecialty.Where(n => n.IsValid && n.Id == id)
            .Select(n => new SubSpecialtyDto
            {
                Id = n.Id,
                Name = n.Name,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate,
                MainSpecialty = new MainSpecialtyDto
                {
                    Id = n.MainSpecialty.Id,
                    Name = n.MainSpecialty.Name,
                    DocCode = n.MainSpecialty.DocCode,
                    CreateDate = n.MainSpecialty.CreateDate
                }
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.SubSpecialtyNotFound]);

        return new(countryDto);
    }

    public async Task<NoteDto> AddNoteAsync(string text)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<NoteModel>(m => m.IncrementNumber);
        var x = await context.Note.AddAsync(new NoteModel
        {
            Text = text,
            IncrementNumber = NextIncrementNumber,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.NT, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        return (await GetNoteAsync(x.Entity.Id)).Data;
    }
    public async Task UpdateNoteAsync(Guid id, string text)
    {
        var noteModel = await context.Note.Where(n => n.IsValid && n.Id == id).FirstOrDefaultAsync() ??
            throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.NoteNotFound]);

        noteModel.Text = text;

        await context.SaveChangesAsync();
    }
    public async Task RemoveNoteAsync(Guid id)
    {
        //if (await context.University.AnyAsync(u => u.CountryId == id && u.IsValid))
        //    throw new ValidException(accreditedUniversityLocalizer[ErrorMessages.CountryHasUniversity]);

        await context.Country.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<NoteDto>>> GetAllNoteAsync()
    {
        Expression<Func<NoteModel, bool>> expression = n => n.IsValid;

        //var countryQuery = context.Country.Where(expression);
        //countryQuery = CustomOrderBy(countryQuery, countrySort, isAsc);

        var notesDto = await context.Note.Where(expression)
            .Select(n => new NoteDto
            {
                Id = n.Id,
                Text = n.Text,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).ToListAsync();
        return new(notesDto);
    }
    public async Task<CommonResponseDto<NoteDto>> GetNoteAsync(Guid id)
    {
        var countryDto = (await context.Note.Where(n => n.IsValid && n.Id == id)
            .Select(n => new NoteDto
            {
                Id = n.Id,
                Text = n.Text,
                DocCode = n.DocCode,
                CreateDate = n.CreateDate
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.NoteNotFound]);

        return new(countryDto);
    }


    public async Task AddUniversityAsync(UniversityFormDto universityFormDto)
    {
        var NextIncrementNumber = await GetNextIncrementNumber<UniversityModel>(m => m.IncrementNumber);
        var x = await context.University.AddAsync(new UniversityModel
        {
            Name = universityFormDto.Name,
            Note = universityFormDto.Note,
            IncrementNumber = NextIncrementNumber,
            CountryId = universityFormDto.CountryId,
            HasMaster = universityFormDto.HasMaster,
            HasBachelor = universityFormDto.HasBachelor,
            HasDoctorate = universityFormDto.HasDoctorate,
            MainSpecialtyId = universityFormDto.SpecialtyId,
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.US, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
    }
    public async Task UpdateUniversityAsync(Guid id, UniversityFormDto universityFormDto)
    {
        var universityModel = await context.University.Where(n => n.IsValid && n.Id == id).FirstOrDefaultAsync() ??
            throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.UniversityNotFound]);

        universityModel.Name = universityFormDto.Name;
        universityModel.Note = universityFormDto.Note;
        universityModel.CountryId = universityFormDto.CountryId;
        universityModel.HasMaster = universityFormDto.HasMaster;
        universityModel.HasBachelor = universityFormDto.HasBachelor;
        universityModel.HasDoctorate = universityFormDto.HasDoctorate;
        universityModel.MainSpecialtyId = universityFormDto.SpecialtyId;

        await context.SaveChangesAsync();
    }
    public async Task RemoveUniversityAsync(Guid id)
    {
        await context.University.Where(n => n.Id == id).ExecuteUpdateAsync(n => n.SetProperty(n => n.IsValid, false));
    }
    public async Task<CommonResponseDto<List<UniversityDto>>> GetAllUniversityAsync(int pageSize, int pageNum, string search, List<Guid> mainSpecialtyIds,
        List<Guid> countryIds, bool? hasMaster, bool? hasDoctorate, bool? hasBachelor, UniversitySort universitySort, bool isAsc)
    {
        Expression<Func<UniversityModel, bool>> expression = u => u.IsValid
        && (string.IsNullOrEmpty(search) || u.Name.Contains(search) || u.Note.Contains(search) || u.Country.Name.Contains(search) || u.MainSpecialty.Name.Contains(search))
        && (mainSpecialtyIds.Count == 0 || mainSpecialtyIds.Contains(u.MainSpecialtyId))
        && (!hasMaster.HasValue || u.HasMaster == hasMaster)
        && (!hasBachelor.HasValue || u.HasBachelor == hasBachelor)
        && (!hasDoctorate.HasValue || u.HasDoctorate == hasDoctorate)
        && (countryIds.Count == 0 || countryIds.Contains(u.CountryId));

        var universitiesQuery = context.University.Where(expression);
        switch (universitySort)
        {
            case UniversitySort.CountryName:
                if (isAsc)
                    universitiesQuery = universitiesQuery.OrderBy(u => u.Country.Name);
                else
                    universitiesQuery = universitiesQuery.OrderByDescending(u => u.Country.Name);
                break;
            case UniversitySort.MainSpecialtyName:
                if (isAsc)
                    universitiesQuery = universitiesQuery.OrderBy(u => u.MainSpecialty.Name);
                else
                    universitiesQuery = universitiesQuery.OrderByDescending(u => u.MainSpecialty.Name);
                break;
            default:
                universitiesQuery = CustomOrderBy(universitiesQuery, universitySort, isAsc);
                break;
        }

        var universitiesDto = await universitiesQuery
            .Select(u => new UniversityDto
            {
                Id = u.Id,
                Name = u.Name,
                Note = u.Note,
                DocCode = u.DocCode,
                HasMaster = u.HasMaster,
                CreateDate = u.CreateDate,
                HasBachelor = u.HasBachelor,
                HasDoctorate = u.HasDoctorate,
                Country = new CountryDto
                {
                    Id = u.CountryId,
                    Name = u.Country.Name,
                    DocCode = u.Country.DocCode
                },
                MainSpecialty = new MainSpecialtyDto
                {
                    Id = u.MainSpecialty.Id,
                    Name = u.MainSpecialty.Name,
                    DocCode = u.MainSpecialty.DocCode
                }
            }).ToListAsync();
        var totalRecords = await universitiesQuery.CountAsync();
        var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
        return new(universitiesDto, pageSize, pageNum, totalRecords, hasNextPage);
    }
    public async Task<CommonResponseDto<UniversityDto>> GetUniversityAsync(Guid id)
    {
        var universityDto = (await context.University.Where(u => u.IsValid && u.Id == id)
            .Select(u => new UniversityDto
            {
                Id = u.Id,
                Name = u.Name,
                Note = u.Note,
                DocCode = u.DocCode,
                HasMaster = u.HasMaster,
                CreateDate = u.CreateDate,
                HasBachelor = u.HasBachelor,
                HasDoctorate = u.HasDoctorate,
                Country = new CountryDto
                {
                    Id = u.CountryId,
                    Name = u.Country.Name,
                    DocCode = u.Country.DocCode
                },
                MainSpecialty = new MainSpecialtyDto
                {
                    Id = u.MainSpecialty.Id,
                    Name = u.MainSpecialty.Name,
                    DocCode = u.MainSpecialty.DocCode
                }
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(accreditedUniversityLocalizer[ErrorMessages.UniversityNotFound]);

        return new(universityDto);
    }
}