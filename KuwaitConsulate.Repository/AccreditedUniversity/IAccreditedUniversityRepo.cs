﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.AccreditedUniversity;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Repository.AccreditedUniversity;

public interface IAccreditedUniversityRepo
{
    Task<CountryDto> AddCountryAsync(string name);
    Task<MainSpecialtyDto> AddMainSpecialtyAsync(string name);
    Task<NoteDto> AddNoteAsync(string text);
    Task<SubSpecialtyDto> AddSubSpecialtyAsync(string name, Guid mainSpecialtyId);
    Task AddUniversityAsync(UniversityFormDto universityFormDto);
    Task<CommonResponseDto<List<CountryDto>>> GetAllCountryAsync();
    Task<CommonResponseDto<List<MainSpecialtyDto>>> GetAllMainSpecialtyAsync();
    Task<CommonResponseDto<List<NoteDto>>> GetAllNoteAsync();
    Task<CommonResponseDto<List<SubSpecialtyDto>>> GetAllSubSpecialtyAsync(Guid? mainSpecialtyId);
    Task<CommonResponseDto<List<UniversityDto>>> GetAllUniversityAsync(int pageSize, int pageNum, string search, List<Guid> mainSpecialtyIds,
        List<Guid> countryIds, bool? hasMaster, bool? hasDoctorate, bool? hasBachelor, UniversitySort universitySort, bool isAsc);
    Task<CommonResponseDto<CountryDto>> GetCountryAsync(Guid id);
    Task<CommonResponseDto<MainSpecialtyDto>> GetMainSpecialtyAsync(Guid id);
    Task<CommonResponseDto<NoteDto>> GetNoteAsync(Guid id);
    Task<CommonResponseDto<SubSpecialtyDto>> GetSubSpecialtyAsync(Guid id);
    Task<CommonResponseDto<UniversityDto>> GetUniversityAsync(Guid id);
    Task RemoveCountryAsync(Guid id);
    Task RemoveMainSpecialtyAsync(Guid id);
    Task RemoveNoteAsync(Guid id);
    Task RemoveSubSpecialtyAsync(Guid id);
    Task RemoveUniversityAsync(Guid id);
    Task UpdateCountryAsync(Guid id, string name);
    Task UpdateMainSpecialtyAsync(Guid id, string name);
    Task UpdateNoteAsync(Guid id, string text);
    Task UpdateSubSpecialtyAsync(Guid id, string name, Guid mainSpecialtyId);
    Task UpdateUniversityAsync(Guid id, UniversityFormDto universityFormDto);
}