﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Settings;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Repository.Settings;

public interface ISettingsRepo
{
    Task UpdateSettingsAsync(List<SettingsLawsFormDto> settingsLawsDtos);
    Task<CommonResponseDto<List<SettingsDto>>> GetAllAsync();
    Task<CommonResponseDto<SettingsDto>> GetSettingsByKeyAsync(SettingsEnum key);
    Task<int> GetLastDocumentCodeAsync(DocumentCode documentCode);
    Task<CommonResponseDto<StatisticsDto>> GetStatisticsAsync();
    Task<CommonResponseDto<List<SettingsDto>>> GetContactFromSettingsAsync();
}