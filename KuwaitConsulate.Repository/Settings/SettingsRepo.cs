﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models.AccreditedUniversity;
using KuwaitConsulate.Database.Models.BasicPage;
using KuwaitConsulate.Database.Models.Employee;
using KuwaitConsulate.Database.Models.MainPage;
using KuwaitConsulate.Database.Models.Office;
using KuwaitConsulate.Database.Models.Order;
using KuwaitConsulate.Database.Models.Settings;
using KuwaitConsulate.Database.Models.User;
using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Settings;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace KuwaitConsulate.Repository.Settings;

public class SettingsRepo : BaseRepo, ISettingsRepo
{
    public SettingsRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment)
        : base(context, configuration, httpContextAccessor, hostingEnvironment)
    {

    }
    public async Task UpdateSettingsAsync(List<SettingsLawsFormDto> settingsLawsDtos)
    {
        var allSettingsLaws = await context.Settings.Include(s => s.Files).ToDictionaryAsync(s => s.Key);

        await context.Database.BeginTransactionAsync();
        foreach (var settingsLawDto in settingsLawsDtos)
        {
            if (allSettingsLaws.TryGetValue(settingsLawDto.Key, out SettingsModel model))
            {
                model.Value = settingsLawDto.Value;
                if (settingsLawDto.FileIds.Count != 0)
                    await context.File.Where(f => settingsLawDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.SettingsId, model.Id));
                if (settingsLawDto.RemoveFileIds.Count != 0)
                    await context.File.Where(f => settingsLawDto.RemoveFileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.IsValid, false));
            }
            else
            {
                var x = await context.Settings.AddAsync(new SettingsModel
                {
                    Key = settingsLawDto.Key,
                    Value = settingsLawDto.Value,
                });
                await context.SaveChangesAsync();
                if (settingsLawDto.FileIds.Count != 0)
                    await context.File.Where(f => settingsLawDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.SettingsId, x.Entity.Id));
            }
        }
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task<CommonResponseDto<List<SettingsDto>>> GetAllAsync()
    {
        var settingsDto = await context.Settings.Select(s => new SettingsDto
        {
            Id = s.Id,
            Key = s.Key,
            Value = s.Value,
            Files = s.Files.Where(f => f.IsValid).Select(f => new FileDto
            {
                Id = f.Id,
                Url = f.Url,
                FileKind = f.FileKind,
                FileType = f.FileType
            }).ToList()
        }).ToListAsync();
        return new(settingsDto);
    }
    public async Task<CommonResponseDto<List<SettingsDto>>> GetContactFromSettingsAsync()
    {
        var settingsDto = await context.Settings
            .Select(s => new SettingsDto
            {
                Id = s.Id,
                Key = s.Key,
                Value = s.Value,
                Files = s.Files.Where(f => f.IsValid).Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).ToListAsync();
        return new(settingsDto);
    }
    public async Task<CommonResponseDto<SettingsDto>> GetSettingsByKeyAsync(SettingsEnum key)
    {
        var settingsLawsDto = await context.Settings.Where(sl => sl.Key == key)
            .Select(s => new SettingsDto
            {
                Id = s.Id,
                Key = s.Key,
                Value = s.Value,
                Files = s.Files.Where(f => f.IsValid).Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    FileKind = f.FileKind,
                    FileType = f.FileType
                }).ToList()
            }).FirstOrDefaultAsync();
        return new(settingsLawsDto);
    }
    public async Task<int> GetLastDocumentCodeAsync(DocumentCode documentCode)
        => documentCode switch
        {
            DocumentCode.E => await GetNextIncrementNumber<EmployeeModel>(m => m.IncrementNumber),
            DocumentCode.U => await GetNextIncrementNumber<UserModel>(m => m.IncrementNumber),
            DocumentCode.OE => await GetNextIncrementNumber<OfficeEmployeeModel>(m => m.IncrementNumber),
            DocumentCode.N => await GetNextIncrementNumber<NewsModel>(m => m.IncrementNumber),
            DocumentCode.CF => await GetNextIncrementNumber<RegulationsModel>(m => m.IncrementNumber),
            DocumentCode.FQ => await GetNextIncrementNumber<FaqModel>(m => m.IncrementNumber),
            DocumentCode.C => await GetNextIncrementNumber<CountryModel>(m => m.IncrementNumber),
            DocumentCode.MS => await GetNextIncrementNumber<MainSpecialtyModel>(m => m.IncrementNumber),
            DocumentCode.SS => await GetNextIncrementNumber<SubSpecialtyModel>(m => m.IncrementNumber),
            DocumentCode.NT => await GetNextIncrementNumber<NoteModel>(m => m.IncrementNumber),
            DocumentCode.US => await GetNextIncrementNumber<UniversityModel>(m => m.IncrementNumber),
            DocumentCode.CR => await GetNextIncrementNumber<CircularsModel>(m => m.IncrementNumber),
            DocumentCode.SG => await GetNextIncrementNumber<StudentGuideModel>(m => m.IncrementNumber),
            DocumentCode.UL => await GetNextIncrementNumber<UsefulLinkModel>(m => m.IncrementNumber),
            DocumentCode.SN => await GetNextIncrementNumber<ShortNewsModel>(m => m.IncrementNumber),
            DocumentCode.IN => await GetNextIncrementNumber<InstructionModel>(m => m.IncrementNumber),
            DocumentCode.HI => await GetNextIncrementNumber<HealthInsuranceModel>(m => m.IncrementNumber),
            DocumentCode.AHE => await GetNextIncrementNumber<AdvantagesHigherEducationModel>(m => m.IncrementNumber),
            DocumentCode.FD => await GetNextIncrementNumber<FinancialDuesModel>(m => m.IncrementNumber),
            DocumentCode.GC => await GetNextIncrementNumber<GovernmentCommunicationModel>(m => m.IncrementNumber),
            DocumentCode.OR => await GetNextIncrementNumber<OrderModel>(m => m.IncrementNumber),
            DocumentCode.OF => await GetNextIncrementNumber<OrderFormModel>(m => m.IncrementNumber),
            DocumentCode.TS => await GetNextIncrementNumber<TechSupportModel>(m => m.IncrementNumber),
            DocumentCode.CA => await GetNextIncrementNumber<CalendarModel>(m => m.IncrementNumber),
            DocumentCode.M => await GetNextIncrementNumber<MissionModel>(m => m.IncrementNumber),
            _ => 0
        };
    public async Task<CommonResponseDto<StatisticsDto>> GetStatisticsAsync()
        => new(new StatisticsDto
        {
            UniversitiesCount = await context.University.Where(u => u.IsValid).CountAsync(),
            OfficeEmployeesCount = await context.OfficeEmployee.Where(u => u.IsValid).CountAsync(),
            MainSpecialtiesCount = await context.MainSpecialty.Where(u => u.IsValid).CountAsync(),
            SubSpecialtiesCount = await context.SubSpecialty.Where(u => u.IsValid).CountAsync(),
            NewsCount = await context.News.Where(u => u.IsValid).CountAsync(),
            PendingUsersCount = await context.User.Where(u => u.IsValid && u.AccountStatus == AccountStatus.Pending).CountAsync(),
            UsersCount = await context.User.Where(u => u.IsValid).CountAsync(),
            OrdersCount = await context.Order.Where(u => u.IsValid).CountAsync(),
            EmployeesCount = await context.Employee.Where(u => u.IsValid).CountAsync()
        });
}