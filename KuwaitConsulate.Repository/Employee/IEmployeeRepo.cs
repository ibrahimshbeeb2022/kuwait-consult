﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Employee;
using KuwaitConsulate.Dto.Role;
using KuwaitConsulate.Dto.Token;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Repository.Employee;

public interface IEmployeeRepo
{
    Task AddAsync(EmployeeFormDto employeeFormDto);
    Task<RoleDto> AddRoleAsync(RoleFormDto roleFormDto);
    Task ChangeEmployeeRoleAsync(Guid empId, List<Guid> roleIds);
    Task<bool> CheckIfEmployeeActiveAsync(Guid empId);
    Task<CommonResponseDto<List<EmployeeDto>>> GetAllAsync(int pageSize, int pageNum, EmployeeSort employeeSort, bool isAsc, string search, bool? isActive, List<Guid> roleIds
        , DateTime? from, DateTime? to);
    Task<CommonResponseDto<List<RoleDto>>> GetAllRoleAsync(string search);
    Task<CommonResponseDto<EmployeeDto>> GetAsync(Guid id);
    Task<CommonResponseDto<List<RoleDto>>> GetEmployeeRoleAsync(Guid? empId = null);
    Task<CommonResponseDto<RoleDto>> GetEmployeePermissionAsync(Guid? empId = null);
    Task<CommonResponseDto<RoleDto>> GetRoleAsync(Guid id);
    TokenDto RefreshToken();
    Task RemoveAsync(Guid employeeId);
    Task RemoveRoleAsync(Guid id);
    Task<SignInEmployeeDto> SignInIntoDashboardAsync(string email, string password);
    Task UpdateAsync(EmployeeFormDto employeeFormDto);
    Task UpdateRoleAsync(RoleFormDto roleFormDto, Guid id);
}