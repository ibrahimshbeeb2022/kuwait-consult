﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models.Employee;
using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Employee;
using KuwaitConsulate.Dto.Role;
using KuwaitConsulate.Dto.Token;
using KuwaitConsulate.Shared.Enum;
using KuwaitConsulate.Shared.Exception;
using KuwaitConsulate.Shared.Helper;
using KuwaitConsulate.Shared.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System.Data;
using System.Linq.Expressions;
using System.Security.Claims;

namespace KuwaitConsulate.Repository.Employee;

public class EmployeeRepo : BaseRepo, IEmployeeRepo
{
    private readonly IStringLocalizer<EmployeeResource> employeeLocalizer;
    public EmployeeRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor,
        IStringLocalizer<EmployeeResource> employeeLocalizer, IHostingEnvironment hostingEnvironment)
        : base(context, configuration, httpContextAccessor, hostingEnvironment)
    {
        this.employeeLocalizer = employeeLocalizer;
    }

    public async Task AddAsync(EmployeeFormDto employeeFormDto)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            var employeeModel = await context.Employee.Where(e => e.Email == employeeFormDto.Email.Trim().ToLower() && e.IsValid).FirstOrDefaultAsync();
            if (employeeModel != null)
                throw new ValidException(employeeLocalizer[ErrorMessages.EmployeeAlreadyExit]);

            if (employeeFormDto.RoleIds.Count == 0)
                throw new ValidException(employeeLocalizer[ErrorMessages.RoleRequired]);

            var hashPassword = HashPassword(employeeFormDto.Password);
            var NextIncrementNumber = await GetNextIncrementNumber<EmployeeModel>(m => m.IncrementNumber);

            var employee = new EmployeeModel
            {
                IsActive = true,
                HashPassword = hashPassword,
                Name = employeeFormDto.Name,
                UserType = UserType.Employee,
                IncrementNumber = NextIncrementNumber,
                PhoneNumber = employeeFormDto.PhoneNumber,
                EmployeeRoles = employeeFormDto.RoleIds.Select(rId => new EmployeeRoleModel
                {
                    RoleId = rId
                }).ToList(),
                Email = employeeFormDto.Email.Trim().ToLower(),
                DocCode = StringHelper.CreateDocumentCode(DocumentCode.E, NextIncrementNumber),
            };
            await context.Employee.AddAsync(employee);
            await context.SaveChangesAsync();

            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task UpdateAsync(EmployeeFormDto employeeFormDto)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            var employeeModel = await context.Employee.Where(e => e.Id == employeeFormDto.Id).Include(e => e.EmployeeRoles).FirstOrDefaultAsync() ??
                throw new NotFoundException(employeeLocalizer[ErrorMessages.EmployeeNotFound]);

            employeeModel.Name = employeeFormDto.Name;
            employeeModel.IsActive = employeeFormDto.IsActive;
            employeeModel.PhoneNumber = employeeFormDto.PhoneNumber;
            employeeModel.Email = employeeFormDto.Email.Trim().ToLower();
            context.EmployeeRole.RemoveRange(employeeModel.EmployeeRoles);
            await context.EmployeeRole.AddRangeAsync(employeeFormDto.RoleIds.Select(rId => new EmployeeRoleModel
            {
                RoleId = rId,
                EmployeeId = employeeModel.Id,
            }));
            if (!string.IsNullOrEmpty(employeeFormDto.Password))
            {
                var hashPassword = HashPassword(employeeFormDto.Password);
                employeeModel.HashPassword = hashPassword;
            }
            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task RemoveAsync(Guid employeeId)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            await context.EmployeeRole.Where(er => er.EmployeeId == employeeId).ExecuteUpdateAsync(x => x.SetProperty(x => x.IsValid, false));
            await context.Employee.Where(e => e.Id == employeeId).ExecuteUpdateAsync(x => x.SetProperty(x => x.IsValid, false));

            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task ChangeEmployeeRoleAsync(Guid empId, List<Guid> roleIds)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            await context.EmployeeRole.Where(er => er.EmployeeId == empId).ExecuteDeleteAsync();
            await context.EmployeeRole.AddRangeAsync(roleIds.Select(rId => new EmployeeRoleModel
            {
                RoleId = rId,
                EmployeeId = empId
            }));
            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task<CommonResponseDto<List<EmployeeDto>>> GetAllAsync(int pageSize, int pageNum, EmployeeSort employeeSort,
        bool isAsc, string search, bool? isActive, List<Guid> roleIds, DateTime? from, DateTime? to)
    {
        try
        {
            Expression<Func<EmployeeModel, bool>> expression = e => e.IsValid && (string.IsNullOrEmpty(search) || e.Name.Contains(search)
            || e.DocCode.Contains(search) || e.Email.Contains(search) || e.PhoneNumber.Contains(search))
            && (!isActive.HasValue || e.IsActive == isActive)
            && (e.UserType != UserType.Admin)
            && (!from.HasValue || e.CreateDate >= from)
            && (!to.HasValue || e.CreateDate <= to)
            && (!roleIds.Any() || e.EmployeeRoles.Any(er => roleIds.Contains(er.RoleId)));

            var employeesQuery = context.Employee.Where(expression);
            employeesQuery = CustomOrderBy(employeesQuery, employeeSort, isAsc);

            var employeesDto = await employeesQuery
                .Skip(pageSize * pageNum)
                .Take(pageSize)
                .Select(e => new EmployeeDto
                {
                    Id = e.Id,
                    Name = e.Name,
                    Email = e.Email,
                    DocCode = e.DocCode,
                    IsActive = e.IsActive,
                    CreateDate = e.CreateDate,
                    PhoneNumber = e.PhoneNumber,
                    Roles = e.EmployeeRoles.Select(er => new RoleDto
                    {
                        Id = er.RoleId,
                        Name = er.Role.Name,
                        AccreditedUniversityAccess = er.Role.AccreditedUniversityAccess,
                        EmployeeManagementAccess = er.Role.EmployeeManagementAccess,
                        BeneficiariesAccess = er.Role.BeneficiariesAccess,
                        EGateAccess = er.Role.EGateAccess,
                        NewsEventsAccess = er.Role.NewsEventsAccess,
                        WebsiteManagementAccess = er.Role.WebsiteManagementAccess,
                    }).ToList()
                }).ToListAsync();
            var totalRecords = await employeesQuery.CountAsync();
            var hasNextPage = await CheckIfHasNextPageAsync(expression, pageSize, pageNum);
            return new(employeesDto, pageSize, pageNum, totalRecords, hasNextPage);
        }
        catch (Exception)
        {
            throw;
        }
    }
    public async Task<CommonResponseDto<EmployeeDto>> GetAsync(Guid id)
    {
        try
        {
            var employeeDto = await context.Employee.Where(e => e.Id == id)
                .Select(e => new EmployeeDto
                {
                    Id = e.Id,
                    Name = e.Name,
                    Email = e.Email,
                    DocCode = e.DocCode,
                    IsActive = e.IsActive,
                    CreateDate = e.CreateDate,
                    PhoneNumber = e.PhoneNumber,
                    Roles = e.EmployeeRoles.Select(er => new RoleDto
                    {
                        Id = er.RoleId,
                        Name = er.Role.Name,
                        AccreditedUniversityAccess = er.Role.AccreditedUniversityAccess,
                        EmployeeManagementAccess = er.Role.EmployeeManagementAccess,
                        BeneficiariesAccess = er.Role.BeneficiariesAccess,
                        EGateAccess = er.Role.EGateAccess,
                        NewsEventsAccess = er.Role.NewsEventsAccess,
                        WebsiteManagementAccess = er.Role.WebsiteManagementAccess
                    }).ToList()
                }).FirstOrDefaultAsync();
            return new(employeeDto);
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<RoleDto> AddRoleAsync(RoleFormDto roleFormDto)
    {
        var x = await context.Role.AddAsync(new RoleModel
        {
            Name = roleFormDto.Name,
            AccreditedUniversityAccess = roleFormDto.AccreditedUniversityAccess,
            EmployeeManagementAccess = roleFormDto.EmployeeManagementAccess,
            BeneficiariesAccess = roleFormDto.BeneficiariesAccess,
            EGateAccess = roleFormDto.EGateAccess,
            NewsEventsAccess = roleFormDto.NewsEventsAccess,
            WebsiteManagementAccess = roleFormDto.WebsiteManagementAccess,
        });
        await context.SaveChangesAsync();
        return (await GetRoleAsync(x.Entity.Id)).Data;
    }
    public async Task UpdateRoleAsync(RoleFormDto roleFormDto, Guid id)
        => await context.Role.Where(r => r.Id == id).ExecuteUpdateAsync(r => r.SetProperty(r => r.Name, roleFormDto.Name)
        .SetProperty(r => r.AccreditedUniversityAccess, roleFormDto.AccreditedUniversityAccess)
        .SetProperty(r => r.EmployeeManagementAccess, roleFormDto.EmployeeManagementAccess)
        .SetProperty(r => r.BeneficiariesAccess, roleFormDto.BeneficiariesAccess)
        .SetProperty(r => r.EmployeeManagementAccess, roleFormDto.EmployeeManagementAccess)
        .SetProperty(r => r.WebsiteManagementAccess, roleFormDto.WebsiteManagementAccess)
        .SetProperty(r => r.EGateAccess, roleFormDto.EGateAccess)
        .SetProperty(r => r.NewsEventsAccess, roleFormDto.NewsEventsAccess));
    public async Task RemoveRoleAsync(Guid id)
    {
        await context.Database.BeginTransactionAsync();
        try
        {
            await context.EmployeeRole.Where(er => er.RoleId == id).ExecuteDeleteAsync();
            await context.Role.Where(r => r.Id == id).ExecuteDeleteAsync();

            await context.SaveChangesAsync();
            await context.Database.CommitTransactionAsync();
        }
        catch (Exception)
        {
            await context.Database.RollbackTransactionAsync();
            throw;
        }
    }
    public async Task<CommonResponseDto<List<RoleDto>>> GetAllRoleAsync(string search)
    {
        var rolesModel = await context.Role.OrderByDescending(c => c.CreateDate)
            .Where(r => r.IsValid && (string.IsNullOrEmpty(search) || r.Name.Contains(search)))
            .Select(r => new RoleDto
            {
                Id = r.Id,
                Name = r.Name,
                AccreditedUniversityAccess = r.AccreditedUniversityAccess,
                EmployeeManagementAccess = r.EmployeeManagementAccess,
                BeneficiariesAccess = r.BeneficiariesAccess,
                NewsEventsAccess = r.NewsEventsAccess,
                EGateAccess = r.EGateAccess,
                WebsiteManagementAccess = r.WebsiteManagementAccess
            }).ToListAsync();
        return new(rolesModel);
    }
    public async Task<CommonResponseDto<RoleDto>> GetRoleAsync(Guid id)
    {
        var roleModel = (await context.Role.Where(r => r.Id == id && r.IsValid)
            .Select(r => new RoleDto
            {
                Id = r.Id,
                Name = r.Name,
                AccreditedUniversityAccess = r.AccreditedUniversityAccess,
                EmployeeManagementAccess = r.EmployeeManagementAccess,
                BeneficiariesAccess = r.BeneficiariesAccess,
                NewsEventsAccess = r.NewsEventsAccess,
                EGateAccess = r.EGateAccess,
                WebsiteManagementAccess = r.WebsiteManagementAccess,
            }).FirstOrDefaultAsync()) ?? throw new NotFoundException(employeeLocalizer[ErrorMessages.RoleNotFound]);
        return new(roleModel);
    }
    public async Task<SignInEmployeeDto> SignInIntoDashboardAsync(string email, string password)
    {
        email = email.Trim().ToLower();
        var employeeModel = await context.Employee.Where(e => e.Email == email && e.IsValid)
            .Include(e => e.EmployeeRoles.Where(er => er.IsValid)).ThenInclude(e => e.Role)
            .FirstOrDefaultAsync() ??
            throw new NotFoundException(employeeLocalizer[ErrorMessages.EmployeeNotFound]);

        if (!employeeModel.IsActive)
            throw new ValidException(employeeLocalizer[ErrorMessages.EmployeeInActive]);

        var isMatchPassword = CheckPassword(password, employeeModel.HashPassword, out string newHash);
        if (isMatchPassword == PasswordVerificationResult.Failed)
            throw new ValidException(employeeLocalizer[ErrorMessages.WrongEmailOrPassword]);

        return new SignInEmployeeDto
        {
            Id = employeeModel.Id,
            Name = employeeModel.Name,
            Email = employeeModel.Email,
            DocCode = employeeModel.DocCode,
            IsActive = employeeModel.IsActive,
            PhoneNumber = employeeModel.PhoneNumber,
            IsAdmin = employeeModel.UserType == UserType.Admin,
            Token = GenerateJwtTokenAsync(employeeModel.Id, employeeModel.UserType),
            Roles = (await GetEmployeePermissionAsync(employeeModel.Id)).Data
        };
    }
    public async Task<bool> CheckIfEmployeeActiveAsync(Guid empId) => await CheckEntityExist<EmployeeModel>(e => e.IsValid && e.IsActive && e.Id == empId);
    public async Task<CommonResponseDto<List<RoleDto>>> GetEmployeeRoleAsync(Guid? empId = null)
    {
        if (!empId.HasValue)
            empId = CurrentUserId;

        empId = Guid.Parse(httpContextAccessor.HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).FirstOrDefault());
        var roles = await context.EmployeeRole.Where(er => er.EmployeeId == empId)
        .Select(r => new RoleDto
        {
            Id = r.Role.Id,
            Name = r.Role.Name,
            AccreditedUniversityAccess = r.Role.AccreditedUniversityAccess,
            EmployeeManagementAccess = r.Role.EmployeeManagementAccess,
            BeneficiariesAccess = r.Role.BeneficiariesAccess,
            NewsEventsAccess = r.Role.NewsEventsAccess,
            EGateAccess = r.Role.EGateAccess,
            WebsiteManagementAccess = r.Role.WebsiteManagementAccess,
        }).ToListAsync();
        return new(roles);
    }
    public TokenDto RefreshToken() => GenerateJwtTokenAsync(CurrentUserId, CurrentUserType);
    public async Task<CommonResponseDto<RoleDto>> GetEmployeePermissionAsync(Guid? empId = null)
    {
        var rolesModel = await context.EmployeeRole.Where(er => empId.HasValue ? er.EmployeeId == empId.Value : er.EmployeeId == CurrentUserId && er.IsValid)
            .Select(er => er.Role)
            .ToListAsync();

        RoleDto roleDto = new();
        foreach (var role in rolesModel)
        {
            roleDto.EmployeeManagementAccess |= role.EmployeeManagementAccess;
            roleDto.BeneficiariesAccess |= role.BeneficiariesAccess;
            roleDto.AccreditedUniversityAccess |= role.AccreditedUniversityAccess;
            roleDto.NewsEventsAccess |= role.NewsEventsAccess;
            roleDto.EGateAccess |= role.EGateAccess;
            roleDto.WebsiteManagementAccess |= role.WebsiteManagementAccess;
        }
        return new(roleDto);
    }
}