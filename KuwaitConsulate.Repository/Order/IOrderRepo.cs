﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Order;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Repository.Order;

public interface IOrderRepo
{
    Task AddAsync(OrderFormDto orderFormDto);
    Task ChangeIsSentStatusAsync(Guid orderId, bool isSent);
    Task ChangeStatusAsync(Guid universityOrderId, Guid orderId, OrderStatus orderStatus);
    Task<CommonResponseDto<List<OrderDto>>> GetAllAsync(int pageSize, int pageNum, string search, DateTime? from, DateTime? to, OrderStatus? orderStatus, OrderSort orderSort, bool isAsc, bool isDashboard);
    Task<CommonResponseDto<OrderDto>> GetDetailsAsync(Guid id);
    Task RemoveAsync(Guid id);
}