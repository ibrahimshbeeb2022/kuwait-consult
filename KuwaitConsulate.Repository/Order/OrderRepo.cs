﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Database.Models.Order;
using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.AccreditedUniversity;
using KuwaitConsulate.Dto.Mail;
using KuwaitConsulate.Dto.Order;
using KuwaitConsulate.Dto.User;
using KuwaitConsulate.Repository.Mail;
using KuwaitConsulate.Shared.Enum;
using KuwaitConsulate.Shared.Exception;
using KuwaitConsulate.Shared.Helper;
using KuwaitConsulate.Shared.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System.Linq.Expressions;

namespace KuwaitConsulate.Repository.Order;

public class OrderRepo : BaseRepo, IOrderRepo
{
    private readonly IStringLocalizer<UserResource> userLocalizer;
    private readonly IMailService mailService;
    public OrderRepo(KuwaitConsulateDbContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IHostingEnvironment hostingEnvironment, IStringLocalizer<UserResource> userLocalizer, IMailService mailService)
        : base(context, configuration, httpContextAccessor, hostingEnvironment)
    {
        this.userLocalizer = userLocalizer;
        this.mailService = mailService;
    }
    public async Task AddAsync(OrderFormDto orderFormDto)
    {
        await context.Database.BeginTransactionAsync();

        var userModel = await context.User.Where(u => u.Id == CurrentUserId).FirstOrDefaultAsync();
        if (userModel.AccountStatus != AccountStatus.Active)
            throw new ValidException(userLocalizer[ErrorMessages.CanNotDoOrder]);

        var NextIncrementNumber = await GetNextIncrementNumber<OrderModel>(m => m.IncrementNumber);
        var x = await context.Order.AddAsync(new OrderModel
        {
            UserId = CurrentUserId,
            Year = orderFormDto.Year,
            Semester = orderFormDto.Semester,
            OrderType = orderFormDto.OrderType,
            IncrementNumber = NextIncrementNumber,
            CreateDate = DateTime.UtcNow.AddMinutes(GetTimeZone()),
            OrderUniversities = orderFormDto.UniversityIds.Select(uId => new OrderUniversityModel
            {
                UniversityId = uId
            }).ToList(),
            DocCode = StringHelper.CreateDocumentCode(DocumentCode.OR, NextIncrementNumber)
        });
        await context.SaveChangesAsync();
        await context.File.Where(f => orderFormDto.FileIds.Contains(f.Id)).ExecuteUpdateAsync(f => f.SetProperty(f => f.OrderId, x.Entity.Id));
        await context.SaveChangesAsync();
        await context.Database.CommitTransactionAsync();
    }
    public async Task RemoveAsync(Guid id)
    {
        await context.OrderUniversity.Where(o => o.OrderId == id).ExecuteUpdateAsync(o => o.SetProperty(o => o.IsValid, false));
        await context.File.Where(o => o.OrderId == id).ExecuteUpdateAsync(o => o.SetProperty(o => o.IsValid, false));
        await context.Order.Where(o => o.Id == id).ExecuteUpdateAsync(o => o.SetProperty(o => o.IsValid, false));
    }
    public async Task ChangeStatusAsync(Guid universityOrderId, Guid orderId, OrderStatus orderStatus)
    {
        var orderModel = await context.Order.Where(o => o.Id == orderId).Include(o => o.User).FirstOrDefaultAsync();
        if (orderStatus == OrderStatus.Approved)
        {
            await context.Order.Where(o => o.Id == orderId).ExecuteUpdateAsync(o => o.SetProperty(o => o.OrderStatus, orderStatus));
            await context.OrderUniversity.Where(o => o.Id == universityOrderId).ExecuteUpdateAsync(o => o.SetProperty(o => o.OrderStatus, orderStatus));
            await context.OrderUniversity.Where(o => o.OrderId == orderId && o.Id != universityOrderId).ExecuteUpdateAsync(o => o.SetProperty(o => o.OrderStatus, OrderStatus.Rejected));

            try
            {
                var universityOrderModel = await context.OrderUniversity.Where(ou => ou.Id == universityOrderId).Include(ou => ou.University).FirstOrDefaultAsync();
                var stream = new StreamReader(hostingEnvironment.WebRootPath + Constants.ApproveOrderTemplatePath);
                var body = await stream.ReadToEndAsync();
                stream.Close();

                body = string.Format(body, orderModel.User.FirstNameAr + " " + orderModel.User.SecondNameAr, universityOrderModel.University.Name,
                    $"{orderModel.CreateDate:yyyy-MM-dd}");
                mailService.SendEmailAsync(new MailRequest
                {
                    Body = body,
                    Subject = "Order approved",
                    ToEmails = [orderModel.User.Email]
                });
            }
            catch (Exception)
            {
            }
        }
        else
        {
            await context.OrderUniversity.Where(o => o.Id == universityOrderId).ExecuteUpdateAsync(o => o.SetProperty(o => o.OrderStatus, orderStatus));
            var orderUniversity = await context.OrderUniversity.Where(ou => ou.IsValid && ou.OrderId == orderId).ToListAsync();
            if (orderUniversity.All(ou => ou.OrderStatus == OrderStatus.Rejected))
            {
                await context.Order.Where(o => o.Id == orderId).ExecuteUpdateAsync(o => o.SetProperty(o => o.OrderStatus, OrderStatus.Rejected));
                try
                {
                    var stream = new StreamReader(hostingEnvironment.WebRootPath + Constants.RejectOrderTemplatePath);
                    var body = await stream.ReadToEndAsync();
                    stream.Close();

                    body = string.Format(body, orderModel.User.FirstNameAr + " " + orderModel.User.SecondNameAr, $"{orderModel.CreateDate:yyyy-MM-dd}");
                    mailService.SendEmailAsync(new MailRequest
                    {
                        Body = body,
                        Subject = "Order rejected",
                        ToEmails = [orderModel.User.Email]
                    });
                }
                catch (Exception)
                {
                }
            }
        }
    }

    public async Task<CommonResponseDto<List<OrderDto>>> GetAllAsync(int pageSize, int pageNum, string search, DateTime? from, DateTime? to, OrderStatus? orderStatus, OrderSort orderSort, bool isAsc, bool isDashboard)
    {
        Expression<Func<OrderModel, bool>> expression = o => (isDashboard || o.UserId == CurrentUserId)
        && o.IsValid && (string.IsNullOrEmpty(search) || o.User.FirstNameAr.Contains(search)
        || o.User.FirstNameEn.Contains(search) || o.User.SecondNameAr.Contains(search)
        || o.User.SecondNameEn.Contains(search))
        && (!orderStatus.HasValue || o.OrderStatus == orderStatus)
        && (!from.HasValue || o.CreateDate.Date >= from.Value.Date)
        && (!to.HasValue || o.CreateDate.Date <= to.Value.Date);

        var ordersQuery = context.Order.Where(expression).AsQueryable();
        switch (orderSort)
        {
            case OrderSort.UserName:
                if (isAsc)
                    ordersQuery = ordersQuery.OrderBy(o => o.User.FirstNameAr);
                else
                    ordersQuery = ordersQuery.OrderByDescending(o => o.User.FirstNameAr);
                break;
            default:
                CustomOrderBy(ordersQuery, orderSort, isAsc);
                break;
        }
        var ordersDto = await ordersQuery.Select(o => new OrderDto
        {
            Id = o.Id,
            Year = o.Year,
            IsSent = o.ISent,
            DocCode = o.DocCode,
            Date = o.CreateDate,
            Semester = o.Semester,
            Files = o.Files.Where(f => f.IsValid).Select(f => new FileDto
            {
                Id = f.Id,
                Url = f.Url,
                Name = f.Name,
                FileKind = f.FileKind,
                FileType = f.FileType
            }).ToList(),
            OrderType = o.OrderType,
            OrderStatus = o.OrderStatus,
            User = new UserDto
            {
                Id = o.User.Id,
                Email = o.User.Email,
                DocCode = o.User.DocCode,
                CivilNo = o.User.CivilNo,
                Username = o.User.Username,
                CreateDate = o.User.CreateDate,
                PhoneNumber = o.User.PhoneNumber,
                RejectReason = o.User.RejectReason,
                AccountStatus = o.User.AccountStatus,
                AddressKuwait = o.User.AddressKuwait,
                AddressUAE = o.User.AddressUAE,
                BirthDate = o.User.BirthDate,
                FirstNameAr = o.User.FirstNameAr,
                FirstNameEn = o.User.FirstNameEn,
                FourthNameAr = o.User.FourthNameAr,
                FourthNameEn = o.User.FourthNameEn,
                EmergencyPhoneNumber = o.User.EmergencyPhoneNumber,
                ThirdNameEn = o.User.ThirdNameEn,
                ThirdNameAr = o.User.FourthNameAr,
                SocialStatus = o.User.SocialStatus,
                SecondNameEn = o.User.SecondNameEn,
                SecondNameAr = o.User.SecondNameAr,
                PhoneNumberUAE = o.User.PhoneNumberUAE,
                PhoneNumberKuwait = o.User.PhoneNumberKuwait,
                PassportNo = o.User.PassportNo,
                EmailConfirmed = o.User.EmailConfirmed,
                Gender = o.User.Gender,
                PassportExpireDate = o.User.PassportExpireDate,
                Files = o.User.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileType = f.FileType,
                    FileKind = f.FileKind
                }).ToList()
            },
            UniversityDto = o.OrderUniversities.Where(ou => ou.IsValid).Select(ou => new UniversityDto
            {
                Id = ou.Id,
                Country = new CountryDto
                {
                    Id = ou.University.CountryId,
                    Name = ou.University.Country.Name,
                    DocCode = ou.University.Country.DocCode
                },
                Name = ou.University.Name,
                HasBachelor = ou.University.HasBachelor,
                HasDoctorate = ou.University.HasDoctorate,
                Note = ou.University.Note,
                HasMaster = ou.University.HasMaster,

                MainSpecialty = new MainSpecialtyDto
                {
                    Id = ou.University.MainSpecialtyId,
                    Name = ou.University.MainSpecialty.Name,
                    DocCode = ou.University.MainSpecialty.DocCode
                },
                DocCode = ou.University.DocCode,
            }).ToList()
        }).ToListAsync();

        var totalRecords = await ordersQuery.CountAsync();
        return new(ordersDto, pageSize, pageNum, totalRecords);
    }

    public async Task<CommonResponseDto<OrderDto>> GetDetailsAsync(Guid id)
    {
        var orderDto = await context.Order.Where(o => o.IsValid && o.Id == id).Select(o => new OrderDto
        {
            Id = o.Id,
            Year = o.Year,
            IsSent = o.ISent,
            DocCode = o.DocCode,
            Date = o.CreateDate,
            Semester = o.Semester,
            Files = o.Files.Where(f => f.IsValid).Select(f => new FileDto
            {
                Id = f.Id,
                Url = f.Url,
                Name = f.Name,
                FileKind = f.FileKind,
                FileType = f.FileType
            }).ToList(),
            OrderType = o.OrderType,
            OrderStatus = o.OrderStatus,
            User = new UserDto
            {
                Id = o.User.Id,
                Email = o.User.Email,
                DocCode = o.User.DocCode,
                CivilNo = o.User.CivilNo,
                Username = o.User.Username,
                CreateDate = o.User.CreateDate,
                PhoneNumber = o.User.PhoneNumber,
                RejectReason = o.User.RejectReason,
                AccountStatus = o.User.AccountStatus,
                AddressKuwait = o.User.AddressKuwait,
                AddressUAE = o.User.AddressUAE,
                BirthDate = o.User.BirthDate,
                FirstNameAr = o.User.FirstNameAr,
                FirstNameEn = o.User.FirstNameEn,
                FourthNameAr = o.User.FourthNameAr,
                FourthNameEn = o.User.FourthNameEn,
                EmergencyPhoneNumber = o.User.EmergencyPhoneNumber,
                ThirdNameEn = o.User.ThirdNameEn,
                ThirdNameAr = o.User.FourthNameAr,
                SocialStatus = o.User.SocialStatus,
                SecondNameEn = o.User.SecondNameEn,
                SecondNameAr = o.User.SecondNameAr,
                PhoneNumberUAE = o.User.PhoneNumberUAE,
                PhoneNumberKuwait = o.User.PhoneNumberKuwait,
                PassportNo = o.User.PassportNo,
                EmailConfirmed = o.User.EmailConfirmed,
                Gender = o.User.Gender,
                PassportExpireDate = o.User.PassportExpireDate,
                Files = o.User.Files.Select(f => new FileDto
                {
                    Id = f.Id,
                    Url = f.Url,
                    Name = f.Name,
                    FileType = f.FileType,
                    FileKind = f.FileKind
                }).ToList()
            },
            UniversityDto = o.OrderUniversities.Where(ou => ou.IsValid).Select(ou => new UniversityDto
            {
                Id = ou.Id,
                OrderStatus = ou.OrderStatus,
                Country = new CountryDto
                {
                    Id = ou.University.CountryId,
                    Name = ou.University.Country.Name,
                    DocCode = ou.University.Country.DocCode
                },
                Name = ou.University.Name,
                HasBachelor = ou.University.HasBachelor,
                HasDoctorate = ou.University.HasDoctorate,
                Note = ou.University.Note,
                HasMaster = ou.University.HasMaster,
                MainSpecialty = new MainSpecialtyDto
                {
                    Id = ou.University.MainSpecialtyId,
                    Name = ou.University.MainSpecialty.Name,
                    DocCode = ou.University.MainSpecialty.DocCode
                },
                DocCode = ou.University.DocCode,
            }).ToList(),
        }).FirstOrDefaultAsync();
        return new(orderDto);
    }
    public async Task ChangeIsSentStatusAsync(Guid orderId, bool isSent)
        => await context.Order.Where(o => o.Id == orderId).ExecuteUpdateAsync(o => o.SetProperty(o => o.ISent, isSent));
}