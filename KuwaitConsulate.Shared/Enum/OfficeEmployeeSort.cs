﻿namespace KuwaitConsulate.Shared.Enum;

public enum OfficeEmployeeSort
{
    DocCode,
    Name,
    Email,
    JobTitle,
    CreateDate
}