﻿namespace KuwaitConsulate.Shared.Enum;

public enum ControllerNames
{
    DashboardEmployee,
    DashboardUser,
    DashboardAboutOffice,
    DashboardBasicPage,
    DashboardMainPage,
    DashboardAccreditedUniversity,
    DashboardGeneralSettings,
    DashboardEGate
}