﻿namespace KuwaitConsulate.Shared.Enum;

public enum StudentGuideSort
{
    DocCode,
    Icon,
    Link,
    Title,
    CreateDate
}