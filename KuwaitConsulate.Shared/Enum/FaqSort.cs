﻿namespace KuwaitConsulate.Shared.Enum;

public enum FaqSort
{
    DocCode,
    Question,
    Answer,
    CreateDate
}