﻿namespace KuwaitConsulate.Shared.Enum;

public enum UniversitySort
{
    DocCode,
    Name,
    CountryName,
    MainSpecialtyName,
    Note,
    CreateDate
}