﻿namespace KuwaitConsulate.Shared.Enum;

public enum FileType
{
    Image,
    Pdf,
    Word,
    Excel
}