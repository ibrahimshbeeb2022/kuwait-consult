﻿namespace KuwaitConsulate.Shared.Enum;

public enum SocialStatus
{
    Single,
    Married,
    Engaged,
    Widowed,
    Divorced,
    Separated
}