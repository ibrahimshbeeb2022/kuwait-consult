﻿namespace KuwaitConsulate.Shared.Enum;

public enum ShortNewsSort
{
    DocCode,
    Body,
    CreateDate
}