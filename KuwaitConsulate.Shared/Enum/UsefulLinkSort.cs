﻿namespace KuwaitConsulate.Shared.Enum;

public enum UsefulLinkSort
{
    DocCode,
    Icon,
    Link,
    Title,
    CreateDate
}