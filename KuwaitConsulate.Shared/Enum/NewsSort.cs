﻿namespace KuwaitConsulate.Shared.Enum;

public enum NewsSort
{
    DocCode,
    Title,
    Body,
    CreateDate
}