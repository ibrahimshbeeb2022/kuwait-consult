﻿namespace KuwaitConsulate.Shared.Enum;

public enum RegulationsSort
{
    DocCode,
    Name,
    CreateDate
}