﻿namespace KuwaitConsulate.Shared.Enum;

public enum UserType
{
    Admin,
    Employee,
    NormalUser
}