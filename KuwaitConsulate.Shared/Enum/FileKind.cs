﻿namespace KuwaitConsulate.Shared.Enum;

public enum FileKind
{
    ProfilePhoto,
    News,
    Regulations,
    Icon,
    OgImage,
    FooterImage,
    NavbarImage,
    Circulars,
    Instructions,
    CarouselItem,
    Passport,
    HighSchoolCertificate,
    SocialSecurity,
    GoodConductCertificate,
    BirthCertificate,
    NationalId
}