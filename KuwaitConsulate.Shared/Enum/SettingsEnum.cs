﻿namespace KuwaitConsulate.Shared.Enum;

public enum SettingsEnum
{
    Title,
    Description,
    KeyWords,
    MetaTitle,
    TechSupport,
    SiteName,
    OgImage,
    Icon,
    NavbarImage,
    FooterImage,
    Email,
    WhatsApp,
    ContactNumber,
    Instagram,
    SecondContactNumber,
    LocationUrl,
    OpenFrom,
    CloseTo
}