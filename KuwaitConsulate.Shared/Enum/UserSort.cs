﻿namespace KuwaitConsulate.Shared.Enum;

public enum UserSort
{
    DocCode,
    Username,
    CivilNo,
    Email,
    PhoneNumber,
    CreateDate
}