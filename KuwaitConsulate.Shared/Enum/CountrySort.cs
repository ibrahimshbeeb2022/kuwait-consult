﻿namespace KuwaitConsulate.Shared.Enum;

public enum CountrySort
{
    DocCode,
    Name,
    CreateDate
}