﻿namespace KuwaitConsulate.Shared.Enum;

public enum EmployeeSort
{
    DocCode,
    Name,
    Email,
    PhoneNumber,
    IsActive,
    CreateDate
}