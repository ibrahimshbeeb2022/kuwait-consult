﻿namespace KuwaitConsulate.Shared.Enum;

public enum CircularsSort
{
    DocCode,
    Name,
    CreateDate
}