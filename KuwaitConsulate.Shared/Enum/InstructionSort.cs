﻿namespace KuwaitConsulate.Shared.Enum;

public enum InstructionSort
{
    DocCode,
    Name,
    CreateDate
}