﻿namespace KuwaitConsulate.Shared.Enum;

public enum Gender
{
    Male,
    Female
}