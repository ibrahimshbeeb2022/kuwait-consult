﻿namespace KuwaitConsulate.Shared.Enum;

public enum OrderStatus
{
    Pending,
    Approved,
    Rejected,
}