﻿namespace KuwaitConsulate.Shared.Enum;

public enum Semester
{
    First,
    Second,
    Summer
}