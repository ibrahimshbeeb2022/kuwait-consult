﻿namespace KuwaitConsulate.Shared.Enum;

public enum DocumentCode
{
    E, // Base to Employee
    U, // Base to NormalUser
    OE, // Base to Office Employee
    N, // Base to News
    CF, // Base to Regulations
    FQ, // Base to FAQ
    C, // Base to Country
    MS, // Base to Main Specialty
    SS, // Base to Sub Specialty
    NT, // Base to Note
    US, // Base to University
    CR, // Base to Circulars
    IN, // Base to Instruction
    SG, // Base to StudentGuide
    UL, // Base to UsefulLink
    SN, // Base to ShortNews
    HI, // Base to HealthInsurance
    FD, // Base to financialDues
    AHE, // Base to AdvantagesHigherEducation
    GC, // Base to GovernmentCommunication
    OR, // Base to Order
    OF, // Base to Order Form
    TS, // Base to Tech Support
    CA, // Base to Calendar
    M, // Base to Mission
}