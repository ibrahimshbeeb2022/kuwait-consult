﻿namespace KuwaitConsulate.Shared.Enum;

public enum OrderSort
{
    DocCode,
    UserName,
    OrderStatus,
    CreateDate
}