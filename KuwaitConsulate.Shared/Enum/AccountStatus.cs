﻿namespace KuwaitConsulate.Shared.Enum;

public enum AccountStatus
{
    Pending,
    Active,
    Inactive,
    Rejected,
}