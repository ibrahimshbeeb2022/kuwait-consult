﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Shared.Helper;

public static class StringHelper
{
    public static string CreateDocumentCode(DocumentCode documentCode, int nextIncrementNumber) => documentCode.ToString() + $"00{nextIncrementNumber}";
    public static string FileUrlSimplify(this string url) => string.IsNullOrEmpty(url) ? null : url.Split("wwwroot").ElementAt(1);
    public static string GenerateRandomString(int length)
    {
        Random random = new();
        const string allowedChars = "abcdefghijklmnopqrstuvwxyz0123456789-";

        char[] suffixChars = new char[length];

        for (int i = 0; i < length; i++)
            suffixChars[i] = allowedChars[random.Next(0, allowedChars.Length)];

        return new(suffixChars);
    }
    public static string GenerateRandomNumber(int length)
    {
        Random random = new();
        const string allowedChars = "0123456789";

        char[] suffixChars = new char[length];

        for (int i = 0; i < length; i++)
            suffixChars[i] = allowedChars[random.Next(0, allowedChars.Length)];

        return new(suffixChars);
    }
}