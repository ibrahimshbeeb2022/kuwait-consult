﻿namespace KuwaitConsulate.Shared.Helper;

public static class Constants
{
    public static readonly string ResetPasswordTemplatePath = "/Templates/ResetPassword.html";
    public static readonly string ConfirmAccountTemplatePath = "/Templates/ConfirmAccount.html";
    public static readonly string RejectAccountTemplatePath = "/Templates/RejectAccount.html";
    public static readonly string ApproveOrderTemplatePath = "/Templates/ApproveOrder.html";
    public static readonly string RejectOrderTemplatePath = "/Templates/RejectOrder.html";
    public static readonly string ActiveAccountTemplatePath = "/Templates/ActiveAccount.html";
    public static readonly double MaximumFileSizeInMB = 30.0;
    public static readonly double Vat = 0.05;
    public static Dictionary<string, string> ImageExtensions
        => new(StringComparer.OrdinalIgnoreCase) { { ".jpeg", ".jpeg" }, { ".jpg", ".jpg" }, { ".png", ".png" }, { ".icon", ".icon" }, { ".svg", ".svg" }, { ".webp", ".webp" }, { ".heic", ".heic" } };
    public static Dictionary<string, string> PdfExtensions => new(StringComparer.OrdinalIgnoreCase) { { ".pdf", ".pdf" } };
    public static Dictionary<string, string> WordExtensions => new(StringComparer.OrdinalIgnoreCase) { { ".docx", ".docx" } };
    public static Dictionary<string, string> ExcelExtensions
        => new(StringComparer.OrdinalIgnoreCase) { { ".xls", ".xls" }, { ".xlsx", ".xlsx" }, { ".xlsm", ".xlsm" }, { ".xltx", ".xltm" }, { ".csv", ".csv" } };
}