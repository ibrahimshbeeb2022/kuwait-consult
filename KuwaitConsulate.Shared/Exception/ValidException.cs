﻿namespace KuwaitConsulate.Shared.Exception;

public class ValidException : System.Exception
{
    public ValidException(string message) : base(message) { }
}