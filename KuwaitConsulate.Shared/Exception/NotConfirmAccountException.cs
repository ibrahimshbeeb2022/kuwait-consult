﻿namespace KuwaitConsulate.Shared.Exception;

public class NotConfirmAccountException : System.Exception
{
    public NotConfirmAccountException(string message) : base(message) { }
}