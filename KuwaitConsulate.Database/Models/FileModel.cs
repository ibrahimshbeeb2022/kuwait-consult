﻿using KuwaitConsulate.Database.Models.BasicPage;
using KuwaitConsulate.Database.Models.MainPage;
using KuwaitConsulate.Database.Models.Office;
using KuwaitConsulate.Database.Models.Order;
using KuwaitConsulate.Database.Models.Settings;
using KuwaitConsulate.Database.Models.User;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Database.Models;
public class FileModel : BaseModel
{
    public string Url { get; set; }
    public string Name { get; set; }
    public FileKind FileKind { get; set; }
    public FileType FileType { get; set; }

    public Guid? OfficeEmployeeId { get; set; }
    public OfficeEmployeeModel OfficeEmployee { get; set; }

    public Guid? NewsId { get; set; }
    public NewsModel News { get; set; }

    public Guid? RegulationsId { get; set; }
    public RegulationsModel Regulations { get; set; }

    public Guid? SettingsId { get; set; }
    public SettingsModel Settings { get; set; }

    public Guid? CircularsId { get; set; }
    public CircularsModel Circulars { get; set; }

    public Guid? InstructionId { get; set; }
    public InstructionModel Instruction { get; set; }

    public Guid? CarouselItemId { get; set; }
    public CarouselItemModel CarouselItem { get; set; }

    public Guid? UserId { get; set; }
    public UserModel User { get; set; }

    public Guid? HealthInsuranceId { get; set; }
    public HealthInsuranceModel HealthInsurance { get; set; }

    public Guid? FinancialDuesId { get; set; }
    public FinancialDuesModel FinancialDues { get; set; }

    public Guid? AdvantagesHigherEducationId { get; set; }
    public AdvantagesHigherEducationModel AdvantagesHigherEducation { get; set; }

    public Guid? GovernmentCommunicationId { get; set; }
    public GovernmentCommunicationModel GovernmentCommunication { get; set; }

    public Guid? OrderId { get; set; }
    public OrderModel Order { get; set; }

    public Guid? OrderFormId { get; set; }
    public OrderFormModel OrderForm { get; set; }

    public Guid? TechSupportId { get; set; }
    public TechSupportModel TechSupport { get; set; }

    public Guid? CalendarId { get; set; }
    public CalendarModel Calendar { get; set; }

    public Guid? MissionId { get; set; }
    public MissionModel Mission { get; set; }
}