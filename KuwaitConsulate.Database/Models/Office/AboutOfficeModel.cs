﻿namespace KuwaitConsulate.Database.Models.Office;

public class AboutOfficeModel : BaseModel
{
    public string Text { get; set; }
}