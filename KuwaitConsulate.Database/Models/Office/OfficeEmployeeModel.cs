﻿namespace KuwaitConsulate.Database.Models.Office;

public class OfficeEmployeeModel : BaseModel
{
    public string Name { get; set; }
    public string JobTitle { get; set; }
    public string Email { get; set; }

    public ICollection<FileModel> Files { get; set; }
}