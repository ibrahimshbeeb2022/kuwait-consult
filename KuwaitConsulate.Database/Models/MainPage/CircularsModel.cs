﻿namespace KuwaitConsulate.Database.Models.MainPage;

public class CircularsModel : BaseModel
{
    public string Name { get; set; }

    public ICollection<FileModel> Files { get; set; }
}