﻿namespace KuwaitConsulate.Database.Models.MainPage;

public class CarouselItemModel : BaseModel
{
    public string Title { get; set; }

    public ICollection<FileModel> Files { get; set; }
}