﻿namespace KuwaitConsulate.Database.Models.MainPage;

public class InstructionModel : BaseModel
{
    public string Name { get; set; }

    public ICollection<FileModel> Files { get; set; }
}