﻿namespace KuwaitConsulate.Database.Models.MainPage;

public class StudentGuideModel : BaseModel
{
    public string Link { get; set; }
    public string Title { get; set; }
    public string Icon { get; set; }
}