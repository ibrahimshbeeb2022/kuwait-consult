﻿namespace KuwaitConsulate.Database.Models.MainPage;

public class ShortNewsModel : BaseModel
{
    public string Body { get; set; }
}