﻿namespace KuwaitConsulate.Database.Models.MainPage;

public class AdvantagesHigherEducationModel : BaseModel
{
    public string Title { get; set; }
    public string Body { get; set; }

    public ICollection<FileModel> Files { get; set; }
}