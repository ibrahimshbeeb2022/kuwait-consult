﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace KuwaitConsulate.Database.Models;

[Index(nameof(DocCode))]
[Index(nameof(IncrementNumber))]
public class BaseModel
{
    [Key]
    public Guid Id { get; set; }
    public string DocCode { get; set; }
    public int IncrementNumber { get; set; }
    public bool IsValid { get; set; } = true;
    public DateTime CreateDate { get; set; } = DateTime.UtcNow;
}