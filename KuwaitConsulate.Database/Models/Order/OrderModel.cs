﻿using KuwaitConsulate.Database.Models.User;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Database.Models.Order;

public class OrderModel : BaseModel
{
    public OrderType OrderType { get; set; }
    public OrderStatus OrderStatus { get; set; }
    public Semester Semester { get; set; }
    public string Year { get; set; }
    public bool ISent { get; set; }

    public Guid UserId { get; set; }
    public UserModel User { get; set; }

    public ICollection<FileModel> Files { get; set; }
    public ICollection<OrderUniversityModel> OrderUniversities { get; set; }
}