﻿using KuwaitConsulate.Database.Models.AccreditedUniversity;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Database.Models.Order;

public class OrderUniversityModel : BaseModel
{
    public OrderStatus OrderStatus { get; set; }

    public Guid OrderId { get; set; }
    public OrderModel Order { get; set; }

    public Guid UniversityId { get; set; }
    public UniversityModel University { get; set; }
}