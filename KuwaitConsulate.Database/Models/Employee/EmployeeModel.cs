﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Database.Models.Employee;

public class EmployeeModel : BaseModel
{
    public string Name { get; set; }
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public string HashPassword { get; set; }
    public bool IsActive { get; set; }
    public UserType UserType { get; set; }

    public ICollection<EmployeeRoleModel> EmployeeRoles { get; set; }
}