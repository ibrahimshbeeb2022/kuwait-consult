﻿namespace KuwaitConsulate.Database.Models.Employee;

public class RoleModel : BaseModel
{
    public string Name { get; set; }
    public bool BeneficiariesAccess { get; set; }
    public bool AccreditedUniversityAccess { get; set; }
    public bool EmployeeManagementAccess { get; set; }
    public bool NewsEventsAccess { get; set; }
    public bool WebsiteManagementAccess { get; set; }
    public bool EGateAccess { get; set; }

    public ICollection<EmployeeRoleModel> RoleEmployees { get; set; }
}