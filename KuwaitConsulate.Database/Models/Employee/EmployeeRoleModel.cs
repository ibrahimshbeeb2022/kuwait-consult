﻿namespace KuwaitConsulate.Database.Models.Employee;

public class EmployeeRoleModel : BaseModel
{
    public Guid RoleId { get; set; }
    public RoleModel Role { get; set; }

    public Guid EmployeeId { get; set; }
    public EmployeeModel Employee { get; set; }
}