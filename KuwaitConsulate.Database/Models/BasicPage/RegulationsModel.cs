﻿namespace KuwaitConsulate.Database.Models.BasicPage;

public class RegulationsModel : BaseModel
{
    public string Name { get; set; }

    public ICollection<FileModel> Files { get; set; }
}