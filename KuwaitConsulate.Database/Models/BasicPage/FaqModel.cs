﻿namespace KuwaitConsulate.Database.Models.BasicPage;

public class FaqModel : BaseModel
{
    public string Question { get; set; }
    public string Answer { get; set; }
}