﻿namespace KuwaitConsulate.Database.Models.BasicPage;

public class MissionModel : BaseModel
{
    public string Name { get; set; }

    public ICollection<FileModel> Files { get; set; }
}