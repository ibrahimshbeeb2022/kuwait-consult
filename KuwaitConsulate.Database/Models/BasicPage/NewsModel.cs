﻿namespace KuwaitConsulate.Database.Models.BasicPage;

public class NewsModel : BaseModel
{
    public string Title { get; set; }
    public string Body { get; set; }

    public ICollection<FileModel> Files { get; set; }
}