﻿using KuwaitConsulate.Database.Models.Order;

namespace KuwaitConsulate.Database.Models.AccreditedUniversity;

public class UniversityModel : BaseModel
{
    public string Name { get; set; }
    public string Note { get; set; }
    public bool HasMaster { get; set; }
    public bool HasBachelor { get; set; }
    public bool HasDoctorate { get; set; }

    public Guid CountryId { get; set; }
    public CountryModel Country { get; set; }

    public Guid MainSpecialtyId { get; set; }
    public MainSpecialtyModel MainSpecialty { get; set; }

    public ICollection<OrderUniversityModel> UniversityOrders { get; set; }
}