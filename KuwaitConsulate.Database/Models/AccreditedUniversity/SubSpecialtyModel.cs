﻿namespace KuwaitConsulate.Database.Models.AccreditedUniversity;

public class SubSpecialtyModel : BaseModel
{
    public string Name { get; set; }

    public Guid MainSpecialtyId { get; set; }
    public MainSpecialtyModel MainSpecialty { get; set; }
}