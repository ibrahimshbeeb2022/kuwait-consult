﻿namespace KuwaitConsulate.Database.Models.AccreditedUniversity;

public class CountryModel : BaseModel
{
    public string Name { get; set; }

    public ICollection<UniversityModel> Universities { get; set; }
}