﻿namespace KuwaitConsulate.Database.Models.AccreditedUniversity;

public class NoteModel : BaseModel
{
    public string Text { get; set; }
}