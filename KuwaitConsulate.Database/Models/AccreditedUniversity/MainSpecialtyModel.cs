﻿namespace KuwaitConsulate.Database.Models.AccreditedUniversity;

public class MainSpecialtyModel : BaseModel
{
    public string Name { get; set; }

    public ICollection<SubSpecialtyModel> SubSpecialties { get; set; }
    public ICollection<UniversityModel> Universities { get; set; }
}