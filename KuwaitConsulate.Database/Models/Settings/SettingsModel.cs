﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Database.Models.Settings;

public class SettingsModel : BaseModel
{
    public string Value { get; set; }
    public SettingsEnum Key { get; set; }

    public ICollection<FileModel> Files { get; set; }
}