﻿using KuwaitConsulate.Shared.Enum;
using Microsoft.EntityFrameworkCore;

namespace KuwaitConsulate.Database.Models.User;

[Index(nameof(Email))]
[Index(nameof(CivilNo))]
[Index(nameof(Username))]
public class UserModel : BaseModel
{
    public string Username { get; set; }
    public string CivilNo { get; set; }
    public string Email { get; set; }
    public bool EmailConfirmed { get; set; }
    public string PhoneNumber { get; set; }
    public string HashPassword { get; set; }
    public string RejectReason { get; set; }
    public string ConfirmOTP { get; set; }
    public AccountStatus AccountStatus { get; set; }
    public string ResetPasswordToken { get; set; }
    public string FirstNameAr { get; set; }
    public string SecondNameAr { get; set; }
    public string ThirdNameAr { get; set; }
    public string FourthNameAr { get; set; }
    public string FirstNameEn { get; set; }
    public string SecondNameEn { get; set; }
    public string ThirdNameEn { get; set; }
    public string FourthNameEn { get; set; }
    public string PassportNo { get; set; }
    public DateTime? PassportExpireDate { get; set; }
    public string PhoneNumberUAE { get; set; }
    public string PhoneNumberKuwait { get; set; }
    public string AddressUAE { get; set; }
    public string AddressKuwait { get; set; }
    public string EmergencyPhoneNumber { get; set; }
    public Gender? Gender { get; set; }
    public DateTime? BirthDate { get; set; }
    public SocialStatus? SocialStatus { get; set; }

    public ICollection<FileModel> Files { get; set; }
}