﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KuwaitConsulate.Database.Migrations
{
    /// <inheritdoc />
    public partial class AddIsSent : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ISent",
                table: "Order",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ISent",
                table: "Order");
        }
    }
}
