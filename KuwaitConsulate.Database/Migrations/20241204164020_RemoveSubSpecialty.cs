﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KuwaitConsulate.Database.Migrations
{
    /// <inheritdoc />
    public partial class RemoveSubSpecialty : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_University_SubSpecialty_SubSpecialtyId",
                table: "University");

            migrationBuilder.RenameColumn(
                name: "SubSpecialtyId",
                table: "University",
                newName: "MainSpecialtyId");

            migrationBuilder.RenameIndex(
                name: "IX_University_SubSpecialtyId",
                table: "University",
                newName: "IX_University_MainSpecialtyId");

            migrationBuilder.AddForeignKey(
                name: "FK_University_MainSpecialty_MainSpecialtyId",
                table: "University",
                column: "MainSpecialtyId",
                principalTable: "MainSpecialty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_University_MainSpecialty_MainSpecialtyId",
                table: "University");

            migrationBuilder.RenameColumn(
                name: "MainSpecialtyId",
                table: "University",
                newName: "SubSpecialtyId");

            migrationBuilder.RenameIndex(
                name: "IX_University_MainSpecialtyId",
                table: "University",
                newName: "IX_University_SubSpecialtyId");

            migrationBuilder.AddForeignKey(
                name: "FK_University_SubSpecialty_SubSpecialtyId",
                table: "University",
                column: "SubSpecialtyId",
                principalTable: "SubSpecialty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
