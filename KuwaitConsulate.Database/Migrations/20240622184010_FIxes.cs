﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace KuwaitConsulate.Database.Migrations
{
    /// <inheritdoc />
    public partial class FIxes : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Semester",
                table: "Order",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Year",
                table: "Order",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Semester",
                table: "Order");

            migrationBuilder.DropColumn(
                name: "Year",
                table: "Order");
        }
    }
}
