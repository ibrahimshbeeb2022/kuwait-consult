﻿using KuwaitConsulate.Database.Models;
using KuwaitConsulate.Database.Models.AccreditedUniversity;
using KuwaitConsulate.Database.Models.BasicPage;
using KuwaitConsulate.Database.Models.Employee;
using KuwaitConsulate.Database.Models.MainPage;
using KuwaitConsulate.Database.Models.Office;
using KuwaitConsulate.Database.Models.Order;
using KuwaitConsulate.Database.Models.Settings;
using KuwaitConsulate.Database.Models.User;
using Microsoft.EntityFrameworkCore;

namespace KuwaitConsulate.Database.Context;

public class KuwaitConsulateDbContext : DbContext
{
    public KuwaitConsulateDbContext(DbContextOptions<KuwaitConsulateDbContext> options) : base(options) { }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.EnableSensitiveDataLogging();
        base.OnConfiguring(optionsBuilder);
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }
    public DbSet<UserModel> User { get; set; }
    public DbSet<RoleModel> Role { get; set; }
    public DbSet<EmployeeModel> Employee { get; set; }
    public DbSet<EmployeeRoleModel> EmployeeRole { get; set; }
    public DbSet<AboutOfficeModel> AboutOffice { get; set; }
    public DbSet<OfficeEmployeeModel> OfficeEmployee { get; set; }
    public DbSet<FileModel> File { get; set; }
    public DbSet<FaqModel> Faq { get; set; }
    public DbSet<RegulationsModel> Regulations { get; set; }
    public DbSet<NewsModel> News { get; set; }
    public DbSet<CountryModel> Country { get; set; }
    public DbSet<UniversityModel> University { get; set; }
    public DbSet<NoteModel> Note { get; set; }
    public DbSet<MainSpecialtyModel> MainSpecialty { get; set; }
    public DbSet<SubSpecialtyModel> SubSpecialty { get; set; }
    public DbSet<SettingsModel> Settings { get; set; }
    public DbSet<InstructionModel> Instruction { get; set; }
    public DbSet<CircularsModel> Circulars { get; set; }
    public DbSet<UsefulLinkModel> UsefulLink { get; set; }
    public DbSet<StudentGuideModel> StudentGuide { get; set; }
    public DbSet<ShortNewsModel> ShortNews { get; set; }
    public DbSet<CarouselItemModel> CarouselItem { get; set; }
    public DbSet<HealthInsuranceModel> HealthInsurance { get; set; }
    public DbSet<FinancialDuesModel> FinancialDues { get; set; }
    public DbSet<AdvantagesHigherEducationModel> AdvantagesHigherEducation { get; set; }
    public DbSet<GovernmentCommunicationModel> GovernmentCommunication { get; set; }
    public DbSet<OrderModel> Order { get; set; }
    public DbSet<OrderUniversityModel> OrderUniversity { get; set; }
    public DbSet<OrderFormModel> OrderForm { get; set; }
    public DbSet<CalendarModel> Calendar { get; set; }
    public DbSet<TechSupportModel> TechSupport { get; set; }
    public DbSet<MissionModel> Mission { get; set; }
}