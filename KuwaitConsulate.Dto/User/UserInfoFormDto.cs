﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto.User;

public class UserInfoFormDto
{
    public required string FirstNameAr { get; set; }
    public required string SecondNameAr { get; set; }
    public required string Email { get; set; }
    public string ThirdNameAr { get; set; }
    public required string FourthNameAr { get; set; }
    public required string FirstNameEn { get; set; }
    public required string SecondNameEn { get; set; }
    public string ThirdNameEn { get; set; }
    public required string FourthNameEn { get; set; }
    public required string PassportNo { get; set; }
    public required DateTime PassportExpireDate { get; set; }
    public string PhoneNumberUAE { get; set; }
    public required string PhoneNumberKuwait { get; set; }
    public string AddressUAE { get; set; }
    public required string AddressKuwait { get; set; }
    public required string EmergencyPhoneNumber { get; set; }
    public required Gender Gender { get; set; }
    public required DateTime BirthDate { get; set; }
    public required SocialStatus SocialStatus { get; set; }
    public List<Guid> FileIds { get; set; } = [];
}