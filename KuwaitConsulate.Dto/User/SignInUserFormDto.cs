﻿namespace KuwaitConsulate.Dto.User;

public class SignInUserFormDto
{
    public string CivilNo { get; set; }
    public string Password { get; set; }
}