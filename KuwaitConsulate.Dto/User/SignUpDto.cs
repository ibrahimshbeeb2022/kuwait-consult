﻿namespace KuwaitConsulate.Dto.User;

public class SignUpDto
{
    public string Email { get; set; }
    public string CivilNo { get; set; }
    public string Password { get; set; }
    public string Username { get; set; }
    public string PhoneNumber { get; set; }
}