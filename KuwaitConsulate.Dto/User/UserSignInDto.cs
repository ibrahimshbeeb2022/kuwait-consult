﻿using KuwaitConsulate.Dto.Token;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto.User;

public class UserSignInDto
{
    public Guid Id { get; set; }
    public string Username { get; set; }
    public string CivilNo { get; set; }
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public string DocCode { get; set; }
    public string RejectReason { get; set; }
    public AccountStatus AccountStatus { get; set; }
    public TokenDto Token { get; set; }
}