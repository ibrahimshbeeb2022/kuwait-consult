﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto.User;

public class UserStatusDto
{
    public AccountStatus Status { get; set; }
    public string RejectReason { get; set; }
}