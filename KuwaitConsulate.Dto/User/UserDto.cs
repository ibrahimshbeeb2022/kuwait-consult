﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto.User;

public class UserDto
{
    public Guid Id { get; set; }
    public string DocCode { get; set; }
    public string Username { get; set; }
    public string CivilNo { get; set; }
    public string Email { get; set; }
    public bool EmailConfirmed { get; set; }
    public string PhoneNumber { get; set; }
    public string RejectReason { get; set; }
    public DateTime CreateDate { get; set; }
    public AccountStatus AccountStatus { get; set; }
    public string FirstNameAr { get; set; }
    public string SecondNameAr { get; set; }
    public string ThirdNameAr { get; set; }
    public string FourthNameAr { get; set; }
    public string FirstNameEn { get; set; }
    public string SecondNameEn { get; set; }
    public string ThirdNameEn { get; set; }
    public string FourthNameEn { get; set; }
    public string PassportNo { get; set; }
    public DateTime? PassportExpireDate { get; set; }
    public string PhoneNumberUAE { get; set; }
    public string PhoneNumberKuwait { get; set; }
    public string AddressUAE { get; set; }
    public string AddressKuwait { get; set; }
    public string EmergencyPhoneNumber { get; set; }
    public Gender? Gender { get; set; }
    public DateTime? BirthDate { get; set; }
    public SocialStatus? SocialStatus { get; set; }
    public List<FileDto> Files { get; set; } = [];
}