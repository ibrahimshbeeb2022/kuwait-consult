﻿namespace KuwaitConsulate.Dto;

public class CommonResponseDto<T>
{
    public CommonResponseDto()
    {
    }
    public CommonResponseDto(T data, int? pageSize = null, int? pageNum = null, int? totalRecords = null, bool? hasNextPage = null)
    {
        Data = data;
        PageNum = pageNum;
        PageSize = pageSize;
        HasNextPage = hasNextPage;
        TotalRecords = totalRecords;
    }
    public T Data { get; set; }
    public int? PageNum { get; set; }
    public int? PageSize { get; set; }
    public int? TotalRecords { get; set; }
    public bool? HasNextPage { get; set; }
    public string ErrorMessage { get; set; }
}