﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto;

public class FileDto
{
    public Guid Id { get; set; }
    public string Url { get; set; }
    public string Name { get; set; }
    public FileKind FileKind { get; set; }
    public FileType FileType { get; set; }
}