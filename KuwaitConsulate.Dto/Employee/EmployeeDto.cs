﻿using KuwaitConsulate.Dto.Role;

namespace KuwaitConsulate.Dto.Employee;

public class EmployeeDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public string Email { get; set; }
    public bool IsActive { get; set; }
    public string PhoneNumber { get; set; }
    public DateTime CreateDate { get; set; }
    public List<RoleDto> Roles { get; set; } = [];
}