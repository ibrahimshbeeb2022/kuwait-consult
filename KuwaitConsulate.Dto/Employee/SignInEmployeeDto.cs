﻿using KuwaitConsulate.Dto.Role;
using KuwaitConsulate.Dto.Token;

namespace KuwaitConsulate.Dto.Employee;

public class SignInEmployeeDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string DocCode { get; set; }
    public string PhoneNumber { get; set; }
    public bool IsActive { get; set; }
    public bool IsAdmin { get; set; }
    public TokenDto Token { get; set; }
    public RoleDto Roles { get; set; }
}