﻿namespace KuwaitConsulate.Dto.Employee;

public class SignInEmployeeFormDto
{
    public string Email { get; set; }
    public string Password { get; set; }
}