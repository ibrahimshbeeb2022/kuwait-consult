﻿namespace KuwaitConsulate.Dto.BasicPage;

public class RegulationsFormDto
{
    public string Name { get; set; }
    public List<Guid> FileIds { get; set; } = [];
}