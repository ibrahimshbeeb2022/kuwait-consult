﻿namespace KuwaitConsulate.Dto.BasicPage;

public class AboutOfficeDto
{
    public string Text { get; set; }
}