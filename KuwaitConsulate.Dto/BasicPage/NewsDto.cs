﻿namespace KuwaitConsulate.Dto.BasicPage;

public class NewsDto
{
    public Guid Id { get; set; }
    public string Body { get; set; }
    public string Title { get; set; }
    public string DocCode { get; set; }
    public DateTime CreateDate { get; set; }
    public List<FileDto> Files { get; set; } = [];
}