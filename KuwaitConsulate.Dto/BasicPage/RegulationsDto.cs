﻿namespace KuwaitConsulate.Dto.BasicPage;

public class RegulationsDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public DateTime CreateDate { get; set; }
    public List<FileDto> Files { get; set; } = [];
}