﻿namespace KuwaitConsulate.Dto.BasicPage;

public class NewsFormDto
{
    public string Title { get; set; }
    public string Body { get; set; }
    public List<Guid> FileIds { get; set; } = [];
}