﻿namespace KuwaitConsulate.Dto.BasicPage;

public class FaqDto
{
    public Guid Id { get; set; }
    public string Answer { get; set; }
    public string Question { get; set; }
    public string DocCode { get; set; }
    public DateTime CreateDate { get; set; }
}