﻿namespace KuwaitConsulate.Dto.BasicPage;

public class FaqFormDto
{
    public string Question { get; set; }
    public string Answer { get; set; }
}