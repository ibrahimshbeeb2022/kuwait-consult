﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto.Settings;

public class SettingsLawsFormDto
{
    public Guid Id { get; set; }
    public string Value { get; set; }
    public SettingsEnum Key { get; set; }

    public List<Guid> FileIds { get; set; } = [];
    public List<Guid> RemoveFileIds { get; set; } = [];
}