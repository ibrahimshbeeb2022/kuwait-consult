﻿namespace KuwaitConsulate.Dto.Settings;

public class StatisticsDto
{
    public int UniversitiesCount { get; set; }
    public int UsersCount { get; set; }
    public int PendingUsersCount { get; set; }
    public int NewsCount { get; set; }
    public int OfficeEmployeesCount { get; set; }
    public int MainSpecialtiesCount { get; set; }
    public int SubSpecialtiesCount { get; set; }
    public int OrdersCount { get; set; }
    public int EmployeesCount { get; set; }
}