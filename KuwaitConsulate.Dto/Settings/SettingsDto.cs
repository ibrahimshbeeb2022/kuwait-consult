﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto.Settings;

public class SettingsDto
{
    public Guid Id { get; set; }
    public string Value { get; set; }
    public SettingsEnum Key { get; set; }
    public List<FileDto> Files { get; set; } = [];
}