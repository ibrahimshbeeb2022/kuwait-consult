﻿namespace KuwaitConsulate.Dto.MainPage;

public class ShortNewsDto
{
    public Guid Id { get; set; }
    public string Body { get; set; }
    public string DocCode { get; set; }
    public DateTime CreateDate { get; set; }
}