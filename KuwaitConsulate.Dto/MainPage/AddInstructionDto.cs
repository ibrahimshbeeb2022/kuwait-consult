﻿namespace KuwaitConsulate.Dto.MainPage;

public class AddInstructionDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public List<Guid> FileIds { get; set; } = [];
}