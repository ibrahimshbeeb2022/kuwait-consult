﻿namespace KuwaitConsulate.Dto.MainPage;

public class ShortNewsFormDto
{
    public string Body { get; set; }
}