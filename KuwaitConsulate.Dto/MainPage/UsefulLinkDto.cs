﻿namespace KuwaitConsulate.Dto.MainPage;

public class UsefulLinkDto
{
    public Guid Id { get; set; }
    public string Icon { get; set; }
    public string Link { get; set; }
    public string Title { get; set; }
    public string DocCode { get; set; }
    public DateTime CreateDate { get; set; }
}