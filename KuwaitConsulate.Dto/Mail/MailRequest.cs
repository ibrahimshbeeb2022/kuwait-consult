﻿using Microsoft.AspNetCore.Http;

namespace KuwaitConsulate.Dto.Mail;

public class MailRequest
{
    public string Body { get; set; }
    public string Subject { get; set; }
    public List<string> ToEmails { get; set; } = [];
    public List<IFormFile> Attachments { get; set; } = [];
}