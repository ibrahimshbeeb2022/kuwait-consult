﻿namespace KuwaitConsulate.Dto.AccreditedUniversity;

public class CountryDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public DateTime CreateDate { get; set; }
}