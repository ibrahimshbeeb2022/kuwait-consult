﻿namespace KuwaitConsulate.Dto.AccreditedUniversity;

public class NoteDto
{
    public Guid Id { get; set; }
    public string Text { get; set; }
    public string DocCode { get; set; }
    public DateTime CreateDate { get; set; }
}