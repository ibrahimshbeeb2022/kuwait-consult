﻿namespace KuwaitConsulate.Dto.AccreditedUniversity;

public class UniversityFormDto
{
    public string Name { get; set; }
    public string Note { get; set; }
    public bool HasMaster { get; set; }
    public bool HasBachelor { get; set; }
    public bool HasDoctorate { get; set; }
    public Guid CountryId { get; set; }
    public Guid SpecialtyId { get; set; }
}