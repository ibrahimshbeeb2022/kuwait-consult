﻿namespace KuwaitConsulate.Dto.AccreditedUniversity;

public class SubSpecialtyDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public DateTime CreateDate { get; set; }
    public MainSpecialtyDto MainSpecialty { get; set; }
}