﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto.AccreditedUniversity;

public class UniversityDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string DocCode { get; set; }
    public string Note { get; set; }
    public OrderStatus OrderStatus { get; set; }
    public bool HasMaster { get; set; }
    public bool HasBachelor { get; set; }
    public bool HasDoctorate { get; set; }
    public CountryDto Country { get; set; }
    public MainSpecialtyDto MainSpecialty { get; set; }
    public DateTime CreateDate { get; set; }
}