﻿namespace KuwaitConsulate.Dto.Office;

public class OfficeEmployeeDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string DocCode { get; set; }
    public string JobTitle { get; set; }
    public DateTime CreateDate { get; set; }
    public List<FileDto> Files { get; set; } = [];
}