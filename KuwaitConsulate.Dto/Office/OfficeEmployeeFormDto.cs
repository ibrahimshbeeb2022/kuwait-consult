﻿namespace KuwaitConsulate.Dto.Office;

public class OfficeEmployeeFormDto
{
    public string Name { get; set; }
    public string JobTitle { get; set; }
    public string Email { get; set; }
    public List<Guid> FileIds { get; set; } = [];
}