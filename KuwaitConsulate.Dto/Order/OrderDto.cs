﻿using KuwaitConsulate.Dto.AccreditedUniversity;
using KuwaitConsulate.Dto.User;
using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto.Order;

public class OrderDto
{
    public Guid Id { get; set; }
    public string Year { get; set; }
    public bool IsSent { get; set; }
    public UserDto User { get; set; }
    public DateTime Date { get; set; }
    public string DocCode { get; set; }
    public Semester Semester { get; set; }
    public OrderType OrderType { get; set; }
    public OrderStatus OrderStatus { get; set; }
    public List<FileDto> Files { get; set; } = [];
    public List<UniversityDto> UniversityDto { get; set; } = [];
}