﻿using KuwaitConsulate.Shared.Enum;

namespace KuwaitConsulate.Dto.Order;

public class OrderFormDto
{
    public OrderType OrderType { get; set; }
    public Semester Semester { get; set; }
    public string Year { get; set; }
    public List<Guid> FileIds { get; set; } = [];
    public List<Guid> UniversityIds { get; set; } = [];
}