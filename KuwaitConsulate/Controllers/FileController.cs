﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Repository.File;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class FileController : ControllerBase
{
    private readonly IFileRepo fileRepo;
    public FileController(IFileRepo fileRepo)
    {
        this.fileRepo = fileRepo;
    }
    [HttpPost, Produces(typeof(FileDto))]
    public async Task<ActionResult> SaveFile(IFormFile file, FileKind fileKind) => Ok(await fileRepo.SaveFile(file, fileKind));

    [HttpDelete]
    public async Task<ActionResult> DeleteFile(Guid fileId)
    {
        await fileRepo.DeleteFile(fileId);
        return Ok();
    }
    [HttpPut]
    public async Task<ActionResult> UpdateCarouselItem(Guid fileId, string url)
    {
        await fileRepo.UpdateCarouselItemAsync(fileId, url);
        return Ok();
    }
}