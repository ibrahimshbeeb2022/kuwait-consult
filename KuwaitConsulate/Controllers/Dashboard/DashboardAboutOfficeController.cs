﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.BasicPage;
using KuwaitConsulate.Dto.Office;
using KuwaitConsulate.Repository.Office;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Dashboard;

[Route("api/[controller]/[action]")]
[ApiController]
public class DashboardAboutOfficeController : ControllerBase
{
    private readonly IOfficeRepo officeRepo;
    public DashboardAboutOfficeController(IOfficeRepo officeRepo)
    {
        this.officeRepo = officeRepo;
    }
    [HttpPost]
    public async Task<IActionResult> AddOfficeEmployee(OfficeEmployeeFormDto officeEmployeeFormDto)
    {
        await officeRepo.AddOfficeEmployeeAsync(officeEmployeeFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> Update(OfficeEmployeeFormDto officeEmployeeFormDto, Guid officeEmployeeId)
    {
        await officeRepo.UpdateOfficeEmployeeAsync(officeEmployeeFormDto, officeEmployeeId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> Remove(Guid officeEmployeeId)
    {
        await officeRepo.RemoveOfficeEmployeeAsync(officeEmployeeId);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateAboutOffice(AboutOfficeDto aboutOfficeDto)
    {
        await officeRepo.UpdateAboutOfficeAsync(aboutOfficeDto.Text);
        return Ok();
    }
    [HttpGet]
    public async Task<IActionResult> GetAboutOffice() => Ok(await officeRepo.GetAboutOfficeAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<List<OfficeEmployeeDto>>))]
    public async Task<IActionResult> GetAllOfficeEmployee(bool isAsc, string search, DateTime? from, DateTime? to, OfficeEmployeeSort officeEmployeeSort = OfficeEmployeeSort.CreateDate,
        int pageSize = 20, int pageNum = 0) => Ok(await officeRepo.GetAllOfficeEmployeeAsync(pageSize, pageNum, search, officeEmployeeSort, isAsc, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<OfficeEmployeeDto>))]
    public async Task<IActionResult> GetOfficeEmployee(Guid officeEmployeeId) => Ok(await officeRepo.GetOfficeEmployeeAsync(officeEmployeeId));
}