﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Order;
using KuwaitConsulate.Repository.Order;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Dashboard;

[Route("api/[controller]/[action]")]
[ApiController]
public class DashboardEGateController : ControllerBase
{
    private readonly IOrderRepo orderRepo;
    public DashboardEGateController(IOrderRepo orderRepo)
    {
        this.orderRepo = orderRepo;
    }
    [HttpPut]
    public async Task<IActionResult> ChangeStatus(Guid universityOrderId, Guid orderId, OrderStatus orderStatus)
    {
        await orderRepo.ChangeStatusAsync(universityOrderId, orderId, orderStatus);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> Remove(Guid id)
    {
        await orderRepo.RemoveAsync(id);
        return Ok();
    }

    [HttpGet, Produces(typeof(CommonResponseDto<List<OrderDto>>))]
    public async Task<IActionResult> GetAll(bool isAsc, string search, DateTime? from, DateTime? to, OrderStatus? orderStatus,
        OrderSort orderSort = OrderSort.CreateDate, int pageSize = 50, int pageNum = 0)
        => Ok(await orderRepo.GetAllAsync(pageSize, pageNum, search, from, to, orderStatus, orderSort, isAsc, true));

    [HttpGet, Produces(typeof(CommonResponseDto<OrderDto>))]
    public async Task<IActionResult> GetDetails(Guid id)
        => Ok(await orderRepo.GetDetailsAsync(id));
    [HttpPut]
    public async Task<IActionResult> UpdateIsSentStatus(Guid orderId, bool isSent)
    {
        await orderRepo.ChangeIsSentStatusAsync(orderId, isSent);
        return Ok();
    }
}