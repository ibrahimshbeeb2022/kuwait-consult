﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Settings;
using KuwaitConsulate.Repository.Settings;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Dashboard;

[Route("api/[controller]/[action]")]
[ApiController]
public class DashboardGeneralSettingsController : ControllerBase
{
    private readonly ISettingsRepo settingsRepo;
    public DashboardGeneralSettingsController(ISettingsRepo settingsRepo)
    {
        this.settingsRepo = settingsRepo;
    }
    [HttpPut]
    public async Task<IActionResult> UpdateSettings(List<SettingsLawsFormDto> settingsLawsFormDtos)
    {
        await settingsRepo.UpdateSettingsAsync(settingsLawsFormDtos);
        return Ok();
    }

    [HttpGet, Produces(typeof(CommonResponseDto<List<SettingsDto>>))]
    public async Task<IActionResult> GetAll() => Ok(await settingsRepo.GetAllAsync());
    [HttpGet, Produces(typeof(CommonResponseDto<SettingsDto>))]
    public async Task<IActionResult> GetSettingsByKey(SettingsEnum key) => Ok(await settingsRepo.GetSettingsByKeyAsync(key));
    [HttpGet]
    public async Task<IActionResult> GetLastDocumentNumber(DocumentCode documentCode)
        => Ok(await settingsRepo.GetLastDocumentCodeAsync(documentCode));

    [HttpGet, AllowAnonymous]
    public async Task<IActionResult> GetStatistics()
        => Ok(await settingsRepo.GetStatisticsAsync());
}