﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.User;
using KuwaitConsulate.Repository.User;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Dashboard;

[Route("api/[controller]/[action]")]
[ApiController]
public class DashboardUserController : ControllerBase
{
    private readonly IUserRepo userRepo;
    public DashboardUserController(IUserRepo userRepo)
    {
        this.userRepo = userRepo;
    }
    [HttpPut]
    public async Task<IActionResult> ActiveUser(Guid userId)
    {
        await userRepo.ActiveUserAsync(userId);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> DeactivateUser(Guid userId)
    {
        await userRepo.DeactivateUserAsync(userId);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> RejectUser(Guid userId, string reason)
    {
        await userRepo.RejectUserAsync(userId, reason);
        return Ok();
    }

    [HttpGet, Produces(typeof(CommonResponseDto<List<UserDto>>))]
    public async Task<IActionResult> GetAll([FromQuery] List<AccountStatus> accountStatuses, bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, UserSort userSort = UserSort.CreateDate)
        => Ok(await userRepo.GetAllAsync(accountStatuses, search, pageSize, pageNum, userSort, isAsc, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<UserDto>))]
    public async Task<IActionResult> Get(Guid userId)
        => Ok(await userRepo.GetAsync(userId));
}