﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Employee;
using KuwaitConsulate.Dto.Role;
using KuwaitConsulate.Repository.Employee;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Dashboard;

[Route("api/[controller]/[action]")]
[ApiController]
public class DashboardEmployeeController : ControllerBase
{
    private readonly IEmployeeRepo employeeRepo;
    public DashboardEmployeeController(IEmployeeRepo employeeRepo)
    {
        this.employeeRepo = employeeRepo;
    }
    [HttpPost, Produces(typeof(SignInEmployeeDto))]
    [AllowAnonymous]
    public async Task<IActionResult> SignIn(SignInEmployeeFormDto signInDto)
        => Ok(await employeeRepo.SignInIntoDashboardAsync(signInDto.Email, signInDto.Password));

    [HttpPost]
    public async Task<IActionResult> Add(EmployeeFormDto employeeFormDto)
    {
        await employeeRepo.AddAsync(employeeFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> Update(EmployeeFormDto employeeFormDto)
    {
        await employeeRepo.UpdateAsync(employeeFormDto);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> Remove(Guid employeeId)
    {
        await employeeRepo.RemoveAsync(employeeId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<EmployeeDto>>))]
    public async Task<IActionResult> GetAll(bool isAsc, string search, bool? isActive, [FromQuery] List<Guid> roleIds, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, EmployeeSort employeeSort = EmployeeSort.CreateDate)
        => Ok(await employeeRepo.GetAllAsync(pageSize, pageNum, employeeSort, isAsc, search, isActive, roleIds, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<EmployeeDto>))]
    public async Task<IActionResult> Get(Guid employeeId)
        => Ok(await employeeRepo.GetAsync(employeeId));

    [HttpPost]
    public async Task<IActionResult> AddRole(RoleFormDto roleFormDto)
    {
        return Ok(await employeeRepo.AddRoleAsync(roleFormDto));
    }
    [HttpPut]
    public async Task<IActionResult> UpdateRole(RoleFormDto roleFormDto, Guid roleId)
    {
        await employeeRepo.UpdateRoleAsync(roleFormDto, roleId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveRole(Guid roleId)
    {
        await employeeRepo.RemoveRoleAsync(roleId);
        return Ok();
    }

    [HttpGet, Produces(typeof(CommonResponseDto<List<RoleDto>>))]
    public async Task<IActionResult> GetAllRole(string search) => Ok(await employeeRepo.GetAllRoleAsync(search));

    [HttpGet, Produces(typeof(CommonResponseDto<RoleDto>))]
    public async Task<IActionResult> GetRole(Guid roleId) => Ok(await employeeRepo.GetRoleAsync(roleId));

    [HttpPut]
    public async Task<IActionResult> ChangeEmployeeRole(Guid employeeId, List<Guid> roleIds)
    {
        await employeeRepo.ChangeEmployeeRoleAsync(employeeId, roleIds);
        return Ok();
    }
    [HttpPut]
    public IActionResult RefreshToken() => Ok(employeeRepo.RefreshToken());

    [HttpGet, AllowAnonymous]
    public async Task<IActionResult> GetEmployeePermission() => Ok(await employeeRepo.GetEmployeePermissionAsync());
}