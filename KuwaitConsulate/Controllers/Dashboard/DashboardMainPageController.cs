﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.MainPage;
using KuwaitConsulate.Repository.MainPage;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Dashboard;

[Route("api/[controller]/[action]")]
[ApiController]
public class DashboardMainPageController : ControllerBase
{
    private readonly IMainPageRepo mainPageRepo;
    public DashboardMainPageController(IMainPageRepo mainPageRepo)
    {
        this.mainPageRepo = mainPageRepo;
    }

    [HttpPost]
    public async Task<IActionResult> AddShortNews(ShortNewsFormDto shortNewsFormDto)
    {
        await mainPageRepo.AddShortNewsAsync(shortNewsFormDto.Body);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateShortNews(ShortNewsFormDto shortNewsFormDto, Guid shortNewsId)
    {
        await mainPageRepo.UpdateShortNewsAsync(shortNewsId, shortNewsFormDto.Body);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveShortNews(Guid newsId)
    {
        await mainPageRepo.RemoveShortNewsAsync(newsId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<ShortNewsDto>>))]
    public async Task<IActionResult> GetAllShortNews(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, ShortNewsSort shortNewsSort = ShortNewsSort.CreateDate)
        => Ok(await mainPageRepo.GetAllShortNewsAsync(search, pageSize, pageNum, shortNewsSort, isAsc, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<ShortNewsDto>))]
    public async Task<IActionResult> GetShortNews(Guid shortNewsId) => Ok(await mainPageRepo.GetShortNewsAsync(shortNewsId));



    [HttpPost]
    public async Task<IActionResult> AddUsefulLink(string title, string link, string icon)
    {
        await mainPageRepo.AddUsefulLinkAsync(title, link, icon);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateUsefulLink(string title, string link, string icon, Guid usefulLinkId)
    {
        await mainPageRepo.UpdateUsefulLinkAsync(usefulLinkId, title, link, icon);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveUsefulLink(Guid usefulLinkId)
    {
        await mainPageRepo.RemoveUsefulLinkAsync(usefulLinkId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<UsefulLinkDto>>))]
    public async Task<IActionResult> GetAllUsefulLink(bool isAsc, string search,
        int pageSize = 20, int pageNum = 0, UsefulLinkSort usefulLinkSort = UsefulLinkSort.CreateDate)
        => Ok(await mainPageRepo.GetAllUsefulLinkAsync(search, pageSize, pageNum, usefulLinkSort, isAsc));

    [HttpGet, Produces(typeof(CommonResponseDto<UsefulLinkDto>))]
    public async Task<IActionResult> GetUsefulLink(Guid usefulLinkId) => Ok(await mainPageRepo.GetUsefulLinkAsync(usefulLinkId));


    [HttpPost]
    public async Task<IActionResult> AddStudentGuide(string title, string link, string icon)
    {
        await mainPageRepo.AddStudentGuideAsync(title, link, icon);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateStudentGuide(string title, string link, string icon, Guid studentGuideId)
    {
        await mainPageRepo.UpdateStudentGuideAsync(studentGuideId, title, link, icon);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveStudentGuide(Guid studentGuideId)
    {
        await mainPageRepo.RemoveUsefulLinkAsync(studentGuideId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<StudentGuideDto>>))]
    public async Task<IActionResult> GetAllStudentGuide(bool isAsc, string search,
        int pageSize = 20, int pageNum = 0, StudentGuideSort studentGuideSort = StudentGuideSort.CreateDate)
        => Ok(await mainPageRepo.GetAllStudentGuideAsync(search, pageSize, pageNum, studentGuideSort, isAsc));

    [HttpGet, Produces(typeof(CommonResponseDto<StudentGuideDto>))]
    public async Task<IActionResult> GetStudentGuide(Guid studentGuideId) => Ok(await mainPageRepo.GetUsefulLinkAsync(studentGuideId));



    [HttpPost]
    public async Task<IActionResult> AddCirculars(string name, [FromQuery] List<Guid> fileIds)
    {
        await mainPageRepo.AddCircularsAsync(name, fileIds);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateCirculars(string name, Guid circularsId, [FromQuery] List<Guid> fileIds)
    {
        await mainPageRepo.UpdateCircularsAsync(circularsId, name, fileIds);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveCirculars(Guid circularsId)
    {
        await mainPageRepo.RemoveCircularsAsync(circularsId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<CircularsDto>>))]
    public async Task<IActionResult> GetAllCirculars(bool isAsc, string search,
        int pageSize = 20, int pageNum = 0, CircularsSort circularsSort = CircularsSort.CreateDate)
        => Ok(await mainPageRepo.GetAllCircularsAsync(pageSize, pageNum, search, circularsSort, isAsc));

    [HttpGet, Produces(typeof(CommonResponseDto<CircularsDto>))]
    public async Task<IActionResult> GetCirculars(Guid circularsId) => Ok(await mainPageRepo.GetUsefulLinkAsync(circularsId));




    [HttpPost]
    public async Task<IActionResult> AddInstruction(AddInstructionDto addInstructionDto)
    {
        await mainPageRepo.AddInstructionAsync(addInstructionDto.Name, addInstructionDto.FileIds);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateInstruction(string name, Guid instructionId, [FromQuery] List<Guid> fileIds)
    {
        await mainPageRepo.UpdateInstructionAsync(instructionId, name, fileIds);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveInstruction(Guid instructionId)
    {
        await mainPageRepo.RemoveInstructionAsync(instructionId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<InstructionDto>>))]
    public async Task<IActionResult> GetAllInstruction(bool isAsc, string search,
        int pageSize = 20, int pageNum = 0, InstructionSort instructionSort = InstructionSort.CreateDate)
        => Ok(await mainPageRepo.GetAllInstructionAsync(pageSize, pageNum, search, instructionSort, isAsc));

    [HttpGet, Produces(typeof(CommonResponseDto<InstructionDto>))]
    public async Task<IActionResult> GetInstruction(Guid circularsId) => Ok(await mainPageRepo.GetUsefulLinkAsync(circularsId));

    [HttpGet, Produces(typeof(CommonResponseDto<List<FileDto>>))]
    public async Task<IActionResult> GetAllCarouselItem() => Ok(await mainPageRepo.GetAllCarouselItemAsync());
}