﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.AccreditedUniversity;
using KuwaitConsulate.Repository.AccreditedUniversity;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Dashboard;

[Route("api/[controller]/[action]")]
[ApiController]
public class DashboardAccreditedUniversityController : ControllerBase
{
    private readonly IAccreditedUniversityRepo accreditedUniversityRepo;
    public DashboardAccreditedUniversityController(IAccreditedUniversityRepo accreditedUniversityRepo)
    {
        this.accreditedUniversityRepo = accreditedUniversityRepo;
    }
    [HttpPost]
    public async Task<IActionResult> AddCountry(string name)
    {
        return Ok(await accreditedUniversityRepo.AddCountryAsync(name));
    }
    [HttpPut]
    public async Task<IActionResult> UpdateCountry(string name, Guid countryId)
    {
        await accreditedUniversityRepo.UpdateCountryAsync(countryId, name);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveCountry(Guid countryId)
    {
        await accreditedUniversityRepo.RemoveCountryAsync(countryId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<CountryDto>>))]
    public async Task<IActionResult> GetAllCountry() => Ok(await accreditedUniversityRepo.GetAllCountryAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<CountryDto>))]
    public async Task<IActionResult> GetCountry(Guid countryId) => Ok(await accreditedUniversityRepo.GetCountryAsync(countryId));



    [HttpPost]
    public async Task<IActionResult> AddMainSpecialty(string name)
    {
        return Ok(await accreditedUniversityRepo.AddMainSpecialtyAsync(name));
    }
    [HttpPut]
    public async Task<IActionResult> UpdateMainSpecialty(string name, Guid mainSpecialtyId)
    {
        await accreditedUniversityRepo.UpdateMainSpecialtyAsync(mainSpecialtyId, name);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveMainSpecialty(Guid mainSpecialtyId)
    {
        await accreditedUniversityRepo.RemoveMainSpecialtyAsync(mainSpecialtyId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<MainSpecialtyDto>>))]
    public async Task<IActionResult> GetAllMainSpecialty() => Ok(await accreditedUniversityRepo.GetAllMainSpecialtyAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<MainSpecialtyDto>))]
    public async Task<IActionResult> GetMainSpecialty(Guid mainSpecialtyId) => Ok(await accreditedUniversityRepo.GetMainSpecialtyAsync(mainSpecialtyId));



    [HttpPost]
    public async Task<IActionResult> AddSubSpecialty(string name, Guid mainSpecialtyId)
    {
        return Ok(await accreditedUniversityRepo.AddSubSpecialtyAsync(name, mainSpecialtyId));
    }
    [HttpPut]
    public async Task<IActionResult> UpdateSubSpecialty(string name, Guid mainSpecialtyId, Guid subSpecialtyId)
    {
        await accreditedUniversityRepo.UpdateSubSpecialtyAsync(subSpecialtyId, name, mainSpecialtyId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveSubSpecialty(Guid subSpecialtyId)
    {
        await accreditedUniversityRepo.RemoveSubSpecialtyAsync(subSpecialtyId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<SubSpecialtyDto>>))]
    public async Task<IActionResult> GetAllSubSpecialty(Guid? mainSpecialtyId) => Ok(await accreditedUniversityRepo.GetAllSubSpecialtyAsync(mainSpecialtyId));

    [HttpGet, Produces(typeof(CommonResponseDto<SubSpecialtyDto>))]
    public async Task<IActionResult> GetSubSpecialty(Guid subSpecialtyId) => Ok(await accreditedUniversityRepo.GetSubSpecialtyAsync(subSpecialtyId));




    [HttpPost]
    public async Task<IActionResult> AddNote(string name)
    {
        return Ok(await accreditedUniversityRepo.AddNoteAsync(name));
    }
    [HttpPut]
    public async Task<IActionResult> UpdateNote(string name, Guid noteId)
    {
        await accreditedUniversityRepo.UpdateNoteAsync(noteId, name);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveNote(Guid noteId)
    {
        await accreditedUniversityRepo.RemoveNoteAsync(noteId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<NoteDto>>))]
    public async Task<IActionResult> GetAllNote() => Ok(await accreditedUniversityRepo.GetAllNoteAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<NoteDto>))]
    public async Task<IActionResult> GetNote(Guid noteId) => Ok(await accreditedUniversityRepo.GetNoteAsync(noteId));





    [HttpPost]
    public async Task<IActionResult> AddUniversity(UniversityFormDto universityFormDto)
    {
        await accreditedUniversityRepo.AddUniversityAsync(universityFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateUniversity(UniversityFormDto universityFormDto, Guid universityId)
    {
        await accreditedUniversityRepo.UpdateUniversityAsync(universityId, universityFormDto);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveUniversity(Guid universityId)
    {
        await accreditedUniversityRepo.RemoveUniversityAsync(universityId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<UniversityDto>>))]
    public async Task<IActionResult> GetAllUniversity(string search, bool isAsc, [FromQuery] List<Guid> mainSpecialtyIds, [FromQuery] List<Guid> countryIds, bool? hasMaster, bool? hasDoctorate, bool? hasBachelor, UniversitySort universitySort = UniversitySort.CreateDate, int pageSize = 20, int pageNum = 0)
        => Ok(await accreditedUniversityRepo.GetAllUniversityAsync(pageSize, pageNum, search, mainSpecialtyIds, countryIds, hasMaster, hasDoctorate, hasBachelor, universitySort, isAsc));

    [HttpGet, Produces(typeof(CommonResponseDto<UniversityDto>))]
    public async Task<IActionResult> GetUniversity(Guid universityId) => Ok(await accreditedUniversityRepo.GetUniversityAsync(universityId));
}