﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.BasicPage;
using KuwaitConsulate.Dto.Employee;
using KuwaitConsulate.Repository.BasicPage;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Dashboard;

[Route("api/[controller]/[action]")]
[ApiController]
public class DashboardBasicPageController : ControllerBase
{
    private readonly IBasicPageRepo basicPageRepo;
    public DashboardBasicPageController(IBasicPageRepo basicPageRepo)
    {
        this.basicPageRepo = basicPageRepo;
    }
    [HttpPost]
    public async Task<IActionResult> AddNews(NewsFormDto newsFormDto)
    {
        await basicPageRepo.AddNewsAsync(newsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateNews(NewsFormDto newsFormDto, Guid newsId)
    {
        await basicPageRepo.UpdateNewsAsync(newsFormDto, newsId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveNews(Guid newsId)
    {
        await basicPageRepo.RemoveNewsAsync(newsId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllNews(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, NewsSort newsSort = NewsSort.CreateDate)
        => Ok(await basicPageRepo.GetAllNewsAsync(pageSize, pageNum, newsSort, isAsc, search, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetNews(Guid newsId) => Ok(await basicPageRepo.GetNewsAsync(newsId));



    [HttpPost]
    public async Task<IActionResult> AddRegulations(RegulationsFormDto regulationsFormDto)
    {
        await basicPageRepo.AddRegulationsAsync(regulationsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateRegulations(RegulationsFormDto regulationsFormDto, Guid regulationsId)
    {
        await basicPageRepo.UpdateRegulationsAsync(regulationsFormDto, regulationsId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveRegulations(Guid regulationsId)
    {
        await basicPageRepo.RemoveRegulationsAsync(regulationsId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<RegulationsDto>>))]
    public async Task<IActionResult> GetAllRegulations(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, RegulationsSort regulationsSort = RegulationsSort.CreateDate)
        => Ok(await basicPageRepo.GetAllRegulationsAsync(pageSize, pageNum, regulationsSort, isAsc, search, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<RegulationsDto>))]
    public async Task<IActionResult> GetRegulations(Guid regulationsId)
        => Ok(await basicPageRepo.GetRegulationsAsync(regulationsId));


    [HttpPost]
    public async Task<IActionResult> AddOrderForm(RegulationsFormDto regulationsFormDto)
    {
        await basicPageRepo.AddOrderFormAsync(regulationsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateOrderForm(RegulationsFormDto regulationsFormDto, Guid orderFormId)
    {
        await basicPageRepo.UpdateOrderFormAsync(regulationsFormDto, orderFormId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveOrderForm(Guid orderFormId)
    {
        await basicPageRepo.RemoveOrderFormAsync(orderFormId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<RegulationsDto>>))]
    public async Task<IActionResult> GetAllOrderForm(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, RegulationsSort regulationsSort = RegulationsSort.CreateDate)
        => Ok(await basicPageRepo.GetAllOrderFormAsync(pageSize, pageNum, regulationsSort, isAsc, search, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<RegulationsDto>))]
    public async Task<IActionResult> GetOrderForm(Guid orderFormId)
        => Ok(await basicPageRepo.GetOrderFormAsync(orderFormId));


    [HttpPost]
    public async Task<IActionResult> AddMission(RegulationsFormDto regulationsFormDto)
    {
        await basicPageRepo.AddMissionAsync(regulationsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateMission(RegulationsFormDto regulationsFormDto, Guid missionId)
    {
        await basicPageRepo.UpdateMissionAsync(regulationsFormDto, missionId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveMission(Guid missionId)
    {
        await basicPageRepo.RemoveMissionAsync(missionId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<RegulationsDto>>))]
    public async Task<IActionResult> GetAllMission(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, RegulationsSort regulationsSort = RegulationsSort.CreateDate)
        => Ok(await basicPageRepo.GetAllMissionAsync(pageSize, pageNum, regulationsSort, isAsc, search, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<RegulationsDto>))]
    public async Task<IActionResult> GetMission(Guid missionId)
        => Ok(await basicPageRepo.GetMissionAsync(missionId));


    [HttpPost]
    public async Task<IActionResult> AddFaq(FaqFormDto faqFormDto)
    {
        await basicPageRepo.AddFaqAsync(faqFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateFaq(FaqFormDto faqFormDto, Guid faqId)
    {
        await basicPageRepo.UpdateFaqAsync(faqFormDto, faqId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveFaq(Guid faqId)
    {
        await basicPageRepo.RemoveFaqAsync(faqId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<FaqDto>>))]
    public async Task<IActionResult> GetAllFaq(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, FaqSort faqSort = FaqSort.CreateDate)
        => Ok(await basicPageRepo.GetAllFaqAsync(pageSize, pageNum, faqSort, isAsc, search, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<EmployeeDto>))]
    public async Task<IActionResult> GetFaq(Guid faqId)
        => Ok(await basicPageRepo.GetFaqAsync(faqId));


    [HttpPost]
    public async Task<IActionResult> AddHealthInsurance(NewsFormDto newsFormDto)
    {
        await basicPageRepo.AddHealthInsuranceAsync(newsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateHealthInsurance(NewsFormDto newsFormDto, Guid healthInsuranceId)
    {
        await basicPageRepo.UpdateHealthInsuranceAsync(newsFormDto, healthInsuranceId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveHealthInsurance(Guid healthInsuranceId)
    {
        await basicPageRepo.RemoveHealthInsuranceAsync(healthInsuranceId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllHealthInsurance(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, NewsSort newsSort = NewsSort.CreateDate)
        => Ok(await basicPageRepo.GetAllHealthInsuranceAsync(pageSize, pageNum, newsSort, isAsc, search, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetHealthInsurance() => Ok(await basicPageRepo.GetHealthInsuranceAsync());


    [HttpPost]
    public async Task<IActionResult> AddFinancialDues(NewsFormDto newsFormDto)
    {
        await basicPageRepo.AddFinancialDuesAsync(newsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateFinancialDues(NewsFormDto newsFormDto, Guid financialDuesId)
    {
        await basicPageRepo.UpdateFinancialDuesAsync(newsFormDto, financialDuesId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveFinancialDues(Guid financialDuesId)
    {
        await basicPageRepo.RemoveFinancialDuesAsync(financialDuesId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllFinancialDues(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, NewsSort newsSort = NewsSort.CreateDate)
        => Ok(await basicPageRepo.GetAllFinancialDuesAsync(pageSize, pageNum, newsSort, isAsc, search, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetFinancialDues() => Ok(await basicPageRepo.GetFinancialDuesAsync());



    [HttpPost]
    public async Task<IActionResult> AddTechSupport(NewsFormDto newsFormDto)
    {
        await basicPageRepo.AddTechSupportAsync(newsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateTechSupport(NewsFormDto newsFormDto, Guid techSupportId)
    {
        await basicPageRepo.UpdateTechSupportAsync(newsFormDto, techSupportId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveTechSupport(Guid techSupportId)
    {
        await basicPageRepo.RemoveTechSupportAsync(techSupportId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetTechSupport() => Ok(await basicPageRepo.GetTechSupportAsync());



    [HttpPost]
    public async Task<IActionResult> AddCalendar(NewsFormDto newsFormDto)
    {
        await basicPageRepo.AddCalendarAsync(newsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateCalendar(NewsFormDto newsFormDto, Guid calendarId)
    {
        await basicPageRepo.UpdateCalendarAsync(newsFormDto, calendarId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveCalendar(Guid calendarId)
    {
        await basicPageRepo.RemoveCalendarAsync(calendarId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetCalendar() => Ok(await basicPageRepo.GetCalendarAsync());


    [HttpPost]
    public async Task<IActionResult> AddAdvantagesHigherEducation(NewsFormDto newsFormDto)
    {
        await basicPageRepo.AddAdvantagesHigherEducationAsync(newsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateAdvantagesHigherEducation(NewsFormDto newsFormDto, Guid advantagesHigherEducationId)
    {
        await basicPageRepo.UpdateAdvantagesHigherEducationAsync(newsFormDto, advantagesHigherEducationId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveAdvantagesHigherEducation(Guid advantagesHigherEducationId)
    {
        await basicPageRepo.RemoveAdvantagesHigherEducationAsync(advantagesHigherEducationId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllAdvantagesHigherEducation(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, NewsSort newsSort = NewsSort.CreateDate)
        => Ok(await basicPageRepo.GetAllAdvantagesHigherEducationAsync(pageSize, pageNum, newsSort, isAsc, search, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetAdvantagesHigherEducation() => Ok(await basicPageRepo.GetAdvantagesHigherEducationAsync());


    [HttpPost]
    public async Task<IActionResult> AddGovernmentCommunication(NewsFormDto newsFormDto)
    {
        await basicPageRepo.AddGovernmentCommunicationAsync(newsFormDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateGovernmentCommunication(NewsFormDto newsFormDto, Guid governmentCommunicationId)
    {
        await basicPageRepo.UpdateGovernmentCommunicationAsync(newsFormDto, governmentCommunicationId);
        return Ok();
    }
    [HttpDelete]
    public async Task<IActionResult> RemoveGovernmentCommunication(Guid governmentCommunicationId)
    {
        await basicPageRepo.RemoveGovernmentCommunicationAsync(governmentCommunicationId);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllGovernmentCommunication(bool isAsc, string search, DateTime? from, DateTime? to,
        int pageSize = 20, int pageNum = 0, NewsSort newsSort = NewsSort.CreateDate)
        => Ok(await basicPageRepo.GetAllGovernmentCommunicationAsync(pageSize, pageNum, newsSort, isAsc, search, from, to));

    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetGovernmentCommunication() => Ok(await basicPageRepo.GetGovernmentCommunicationAsync());
}