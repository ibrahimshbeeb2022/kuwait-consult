﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.Order;
using KuwaitConsulate.Repository.Order;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Website;

[Route("api/[controller]/[action]")]
[ApiController]
public class WebsiteEGateController : ControllerBase
{
    private readonly IOrderRepo orderRepo;
    public WebsiteEGateController(IOrderRepo orderRepo)
    {
        this.orderRepo = orderRepo;
    }
    [HttpPost]
    public async Task<IActionResult> Add(OrderFormDto orderFormDto)
    {
        await orderRepo.AddAsync(orderFormDto);
        return Ok();
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<OrderDto>>))]
    public async Task<IActionResult> GetAll(bool isAsc, string search, DateTime? from, DateTime? to, OrderStatus? orderStatus,
        OrderSort orderSort = OrderSort.CreateDate, int pageSize = 50, int pageNum = 0)
        => Ok(await orderRepo.GetAllAsync(pageSize, pageNum, search, from, to, orderStatus, orderSort, isAsc, false));

    [HttpGet, Produces(typeof(CommonResponseDto<OrderDto>))]
    public async Task<IActionResult> GetDetails(Guid id)
        => Ok(await orderRepo.GetDetailsAsync(id));
}