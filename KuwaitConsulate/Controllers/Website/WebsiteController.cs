﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.AccreditedUniversity;
using KuwaitConsulate.Dto.BasicPage;
using KuwaitConsulate.Dto.MainPage;
using KuwaitConsulate.Dto.Office;
using KuwaitConsulate.Dto.Settings;
using KuwaitConsulate.Repository.AccreditedUniversity;
using KuwaitConsulate.Repository.BasicPage;
using KuwaitConsulate.Repository.MainPage;
using KuwaitConsulate.Repository.Office;
using KuwaitConsulate.Repository.Settings;
using KuwaitConsulate.Shared.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Website;

[Route("api/[controller]/[action]")]
[ApiController, AllowAnonymous]
public class WebsiteController : ControllerBase
{
    private readonly IOfficeRepo officeRepo;
    private readonly IMainPageRepo mainPageRepo;
    private readonly ISettingsRepo settingsRepo;
    private readonly IBasicPageRepo basicPageRepo;
    private readonly IAccreditedUniversityRepo accreditedUniversityRepo;
    public WebsiteController(IMainPageRepo mainPageRepo, IBasicPageRepo basicPageRepo, IAccreditedUniversityRepo accreditedUniversityRepo, IOfficeRepo officeRepo, ISettingsRepo settingsRepo)
    {
        this.mainPageRepo = mainPageRepo;
        this.basicPageRepo = basicPageRepo;
        this.accreditedUniversityRepo = accreditedUniversityRepo;
        this.officeRepo = officeRepo;
        this.settingsRepo = settingsRepo;
    }
    [HttpGet, Produces(typeof(CommonResponseDto<List<UsefulLinkDto>>))]
    public async Task<IActionResult> GetAllUseFullLinks(string search, int pageSize = 20, int pageNum = 0)
        => Ok(await mainPageRepo.GetAllUsefulLinkAsync(search, pageSize, pageNum, UsefulLinkSort.CreateDate, true));

    [HttpGet, Produces(typeof(CommonResponseDto<UsefulLinkDto>))]
    public async Task<IActionResult> GetUseFullLinks(Guid id)
        => Ok(await mainPageRepo.GetUsefulLinkAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<StudentGuideDto>>))]
    public async Task<IActionResult> GetAllStudentGuide(string search, int pageSize = 20, int pageNum = 0)
        => Ok(await mainPageRepo.GetAllStudentGuideAsync(search, pageSize, pageNum, StudentGuideSort.CreateDate, true));

    [HttpGet, Produces(typeof(CommonResponseDto<StudentGuideDto>))]
    public async Task<IActionResult> GetStudentGuide(Guid id)
        => Ok(await mainPageRepo.GetStudentGuideAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<ShortNewsDto>>))]
    public async Task<IActionResult> GetAllShortNews(string search, int pageSize = 20, int pageNum = 0)
        => Ok(await mainPageRepo.GetAllShortNewsAsync(search, pageSize, pageNum, ShortNewsSort.CreateDate, true, null, null));

    [HttpGet, Produces(typeof(CommonResponseDto<ShortNewsDto>))]
    public async Task<IActionResult> GetShortNews(Guid id)
        => Ok(await mainPageRepo.GetShortNewsAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<InstructionDto>>))]
    public async Task<IActionResult> GetAllInstruction(string search, int pageSize = 20, int pageNum = 0)
        => Ok(await mainPageRepo.GetAllInstructionAsync(pageSize, pageNum, search, InstructionSort.CreateDate, true));

    [HttpGet, Produces(typeof(CommonResponseDto<InstructionDto>))]
    public async Task<IActionResult> GetInstruction(Guid id)
        => Ok(await mainPageRepo.GetInstructionAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<FileDto>>))]
    public async Task<IActionResult> GetAllCarouselItem()
        => Ok(await mainPageRepo.GetAllCarouselItemAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<List<FaqDto>>))]
    public async Task<IActionResult> GetAllFaq(string search, int pageSize = 20, int pageNum = 0)
        => Ok(await basicPageRepo.GetAllFaqAsync(pageSize, pageNum, FaqSort.CreateDate, true, search, null, null));

    [HttpGet, Produces(typeof(CommonResponseDto<FaqDto>))]
    public async Task<IActionResult> GetFaq(Guid id)
        => Ok(await basicPageRepo.GetFaqAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllNews(string search, int pageSize = 20, int pageNum = 0)
        => Ok(await basicPageRepo.GetAllNewsAsync(pageSize, pageNum, NewsSort.CreateDate, true, search, null, null));

    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetNews(Guid id)
        => Ok(await basicPageRepo.GetNewsAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<RegulationsDto>>))]
    public async Task<IActionResult> GetAllRegulations(string search, int pageSize = 1000, int pageNum = 0)
        => Ok(await basicPageRepo.GetAllRegulationsAsync(pageSize, pageNum, RegulationsSort.CreateDate, true, search, null, null));

    [HttpGet, Produces(typeof(CommonResponseDto<RegulationsDto>))]
    public async Task<IActionResult> GetRegulations(Guid id)
        => Ok(await basicPageRepo.GetRegulationsAsync(id));


    [HttpGet, Produces(typeof(CommonResponseDto<List<CircularsDto>>))]
    public async Task<IActionResult> GetAllCirculars()
        => Ok(await mainPageRepo.GetAllCircularsAsync(1000, 0, null, CircularsSort.CreateDate, true));

    [HttpGet, Produces(typeof(CommonResponseDto<CircularsDto>))]
    public async Task<IActionResult> GetCirculars(Guid circularsId) => Ok(await mainPageRepo.GetUsefulLinkAsync(circularsId));

    [HttpGet, Produces(typeof(CommonResponseDto<List<RegulationsDto>>))]
    public async Task<IActionResult> GetAllOrderForm(string search, int pageSize = 1000, int pageNum = 0)
        => Ok(await basicPageRepo.GetAllOrderFormAsync(pageSize, pageNum, RegulationsSort.CreateDate, true, search, null, null));

    [HttpGet, Produces(typeof(CommonResponseDto<RegulationsDto>))]
    public async Task<IActionResult> GetOrderForm(Guid id)
        => Ok(await basicPageRepo.GetOrderFormAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<RegulationsDto>>))]
    public async Task<IActionResult> GetAllMission(string search, int pageSize = 1000, int pageNum = 0)
        => Ok(await basicPageRepo.GetAllMissionAsync(pageSize, pageNum, RegulationsSort.CreateDate, true, search, null, null));

    [HttpGet, Produces(typeof(CommonResponseDto<RegulationsDto>))]
    public async Task<IActionResult> GetMission(Guid id)
        => Ok(await basicPageRepo.GetMissionAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<NoteDto>>))]
    public async Task<IActionResult> GetAllNote()
        => Ok(await accreditedUniversityRepo.GetAllNoteAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<List<CountryDto>>))]
    public async Task<IActionResult> GetAllCountry()
        => Ok(await accreditedUniversityRepo.GetAllCountryAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<List<MainSpecialtyDto>>))]
    public async Task<IActionResult> GetAllMainSpecialty()
        => Ok(await accreditedUniversityRepo.GetAllMainSpecialtyAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<List<SubSpecialtyDto>>))]
    public async Task<IActionResult> GetAllSubSpecialty(Guid? mainSpecialtyId)
        => Ok(await accreditedUniversityRepo.GetAllSubSpecialtyAsync(mainSpecialtyId));

    [HttpGet, Produces(typeof(CommonResponseDto<string>))]
    public async Task<IActionResult> GetAboutOffice()
        => Ok(await officeRepo.GetAboutOfficeAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<List<OfficeEmployeeDto>>))]
    public async Task<IActionResult> GetAllOfficeEmployee(string search, int pageSize = 20, int pageNum = 0)
        => Ok(await officeRepo.GetAllOfficeEmployeeAsync(pageSize, pageNum, search, OfficeEmployeeSort.CreateDate, true, null, null));

    [HttpGet, Produces(typeof(CommonResponseDto<OfficeEmployeeDto>))]
    public async Task<IActionResult> GetOfficeEmployee(Guid id)
        => Ok(await officeRepo.GetOfficeEmployeeAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<UniversityDto>>))]
    public async Task<IActionResult> GetAllUniversity(string search, [FromQuery] List<Guid> mainSpecialtyIds,
        [FromQuery] List<Guid> countryIds, bool? hasMaster, bool? hasBachelor, bool? hasDoctorate, int pageSize = 1000, int pageNum = 0)
        => Ok(await accreditedUniversityRepo.GetAllUniversityAsync(pageSize, pageNum, search, mainSpecialtyIds, countryIds,
            hasMaster, hasBachelor, hasDoctorate, UniversitySort.CreateDate, true));

    [HttpGet, Produces(typeof(CommonResponseDto<List<UniversityDto>>))]
    public async Task<IActionResult> GetUniversity(Guid id)
        => Ok(await accreditedUniversityRepo.GetUniversityAsync(id));

    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllFinancialDues()
        => Ok(await basicPageRepo.GetAllFinancialDuesAsync(100, 0, NewsSort.CreateDate, true, null, null, null));
    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetFinancialDues() => Ok(await basicPageRepo.GetFinancialDuesAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetTechSupport() => Ok(await basicPageRepo.GetTechSupportAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetCalendar() => Ok(await basicPageRepo.GetCalendarAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllHealthInsurance()
        => Ok(await basicPageRepo.GetAllHealthInsuranceAsync(100, 0, NewsSort.CreateDate, true, null, null, null));
    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetHealthInsurance() => Ok(await basicPageRepo.GetHealthInsuranceAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllAdvantagesHigherEducation()
        => Ok(await basicPageRepo.GetAllAdvantagesHigherEducationAsync(100, 0, NewsSort.CreateDate, true, null, null, null));

    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetAdvantagesHigherEducation() => Ok(await basicPageRepo.GetAdvantagesHigherEducationAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<List<NewsDto>>))]
    public async Task<IActionResult> GetAllGovernmentCommunication()
        => Ok(await basicPageRepo.GetAllGovernmentCommunicationAsync(100, 0, NewsSort.CreateDate, true, null, null, null));
    [HttpGet, Produces(typeof(CommonResponseDto<NewsDto>))]
    public async Task<IActionResult> GetGovernmentCommunication() => Ok(await basicPageRepo.GetGovernmentCommunicationAsync());
    [HttpGet, Produces(typeof(CommonResponseDto<SettingsDto>))]
    public async Task<IActionResult> GetContactFromSettings() => Ok(await settingsRepo.GetContactFromSettingsAsync());
}