﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Dto.User;
using KuwaitConsulate.Repository.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KuwaitConsulate.Controllers.Website;

[Route("api/[controller]/[action]")]
[ApiController]
public class WebsiteUserController : ControllerBase
{
    private readonly IUserRepo userRepo;
    public WebsiteUserController(IUserRepo userRepo)
    {
        this.userRepo = userRepo;
    }
    [HttpPost, AllowAnonymous]
    public async Task<IActionResult> SignUp(SignUpDto signUpDto)
    {
        await userRepo.SignUpAsync(signUpDto);
        return Ok();
    }
    [HttpPost, Produces(typeof(CommonResponseDto<UserSignInDto>)), AllowAnonymous]
    public async Task<IActionResult> SignIn(SignInUserFormDto signInDto)
        => Ok(await userRepo.SignInAsync(signInDto.CivilNo, signInDto.Password));

    [HttpPut, AllowAnonymous]
    public async Task<IActionResult> ReSubmitUser(SignUpDto signUpDto, Guid userId)
    {
        await userRepo.ReSubmitUserAsync(signUpDto, userId);
        return Ok();
    }
    [HttpPost, AllowAnonymous]
    public async Task<IActionResult> RequestConfirmEmail(string email)
    {
        await userRepo.RequestConfirmEmailAsync(email);
        return Ok();
    }
    [HttpPost, AllowAnonymous]
    public async Task<IActionResult> RequestResetPassword(string email)
    {
        await userRepo.RequestResetPasswordAsync(email);
        return Ok();
    }

    [HttpPut, AllowAnonymous]
    public async Task<IActionResult> ResetPassword(string email, string token, string newPassword)
    {
        await userRepo.ResetPasswordAsync(email, token, newPassword);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> UpdateInfo(UserInfoFormDto userInfoDto)
    {
        await userRepo.UpdateInfoAsync(userInfoDto);
        return Ok();
    }
    [HttpPut]
    public async Task<IActionResult> ChangePassword(ChangePasswordDto changePasswordDto)
    {
        await userRepo.ChangePasswordAsync(changePasswordDto);
        return Ok();
    }
    [HttpPut, AllowAnonymous]
    public async Task<IActionResult> ConfirmAccount(string token, string email)
    {
        await userRepo.ConfirmAccountAsync(token, email);
        return Ok();
    }
    [HttpPut, AllowAnonymous]
    public async Task<IActionResult> ReSendConfirmToken(string email)
    {
        await userRepo.ReSendConfirmTokenAsync(email);
        return Ok();
    }
    [HttpPut, AllowAnonymous]
    public IActionResult RefreshToken() => Ok(userRepo.RefreshToken());

    [HttpPut, Produces(typeof(CommonResponseDto<UserInfoDto>))]
    public async Task<IActionResult> GetUserInfo() => Ok(await userRepo.GetUserInfoAsync());

    [HttpGet, Produces(typeof(CommonResponseDto<UserStatusDto>))]
    public async Task<IActionResult> GetUserStatus() => Ok(await userRepo.GetUserStatusAsync());
}