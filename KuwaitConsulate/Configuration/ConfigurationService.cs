﻿using KuwaitConsulate.Database.Context;
using KuwaitConsulate.Repository.AccreditedUniversity;
using KuwaitConsulate.Repository.BasicPage;
using KuwaitConsulate.Repository.Employee;
using KuwaitConsulate.Repository.File;
using KuwaitConsulate.Repository.Mail;
using KuwaitConsulate.Repository.MainPage;
using KuwaitConsulate.Repository.Office;
using KuwaitConsulate.Repository.Order;
using KuwaitConsulate.Repository.Settings;
using KuwaitConsulate.Repository.User;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Globalization;
using System.Text;

namespace KuwaitConsulate.Configuration;

public static class ConfigurationService
{
    public static void ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
    => services.AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    }).AddJwtBearer(options =>
    {
        options.SaveToken = true;
        options.IncludeErrorDetails = true;
        options.RequireHttpsMetadata = false;
        //options.Events = new JwtBearerEvents
        //{
        //    OnChallenge = context =>
        //    {
        //        context.Response.StatusCode = 401;
        //        return Task.CompletedTask;
        //    }
        //};
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            RequireExpirationTime = true,
            ClockSkew = TimeSpan.Zero,
            ValidAudience = configuration["JwtConfig:validAudience"],
            ValidIssuer = configuration["JwtConfig:validIssuer"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(s: configuration["JwtConfig:secret"])),
            TokenDecryptionKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(s: configuration["JwtConfig:secret"])),
        };
    });

    public static void ConfigureSwagger(this IServiceCollection services)
        => services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "Kuwait Consulate Web API",
                Version = "v1",
                Description = "Kuwait Consulate API Services.",
                Contact = new OpenApiContact
                {
                    Name = "Kuwait Consulate"
                },
            });
            c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer",
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
                Description = "JWT Authorization header using the Bearer scheme."
            });
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    Array.Empty<string>()
                }
            });
        });

    public static void ConfigureLocalization(this IServiceCollection services)
        => services.AddLocalization()
        .Configure<RequestLocalizationOptions>(options =>
        {
            var supportedCultures = new[]
            {
                new CultureInfo("ar"),
                new CultureInfo("en")
            };
            options.DefaultRequestCulture = new RequestCulture(culture: "ar", uiCulture: "ar");
            options.SupportedCultures = supportedCultures;
            options.SupportedUICultures = supportedCultures;
        });
    public static void ConfigureDatabase(this IServiceCollection services, string connectionString)
        => services.AddDbContext<KuwaitConsulateDbContext>(op => op.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)));

    public static void ConfigureRepositoryInjection(this IServiceCollection services)
        => services.AddScoped<IEmployeeRepo, EmployeeRepo>()
        .AddScoped<IMailService, MailService>()
        .AddScoped<IOfficeRepo, OfficeRepo>()
        .AddScoped<ISettingsRepo, SettingsRepo>()
        .AddScoped<IFileRepo, FileRepo>()
        .AddScoped<IOrderRepo, OrderRepo>()
        .AddScoped<IMainPageRepo, MainPageRepo>()
        .AddScoped<IAccreditedUniversityRepo, AccreditedUniversityRepo>()
        .AddScoped<IBasicPageRepo, BasicPageRepo>()
        .AddScoped<IUserRepo, UserRepo>();
}