﻿using KuwaitConsulate.Dto.Role;
using KuwaitConsulate.Repository.Employee;
using KuwaitConsulate.Repository.User;
using KuwaitConsulate.Shared.Enum;
using KuwaitConsulate.Shared.Exception;
using KuwaitConsulate.Shared.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using System.Data;
using System.Security.Claims;

namespace KuwaitConsulate.Middleware;

public class AuthorizationMiddleware : IMiddleware
{
    private readonly IStringLocalizer<SharedResource> sharedLocalizer;
    public AuthorizationMiddleware(IStringLocalizer<SharedResource> sharedLocalizer)
    {
        this.sharedLocalizer = sharedLocalizer;
    }
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        var endpoint = context.GetEndpoint() ??
            throw new NotFoundException("endpoint not found");

        if (endpoint.DisplayName == "405 HTTP Method Not Supported")
            throw new MethodNotAllowedException("HTTP Method Not Supported");

        bool? isAllowAnonymous = endpoint?.Metadata.Any(x => x.GetType() == typeof(AllowAnonymousAttribute));
        if (isAllowAnonymous == true)
        {
            await next(context);
            return;
        }

        var token = context.Request.Headers.Authorization.ToString();
        if (string.IsNullOrEmpty(token) || !context.User.Identity.IsAuthenticated)
            throw new UnauthorizedAccessException("Unauthorized");

        string controllerName = context.Request.RouteValues["controller"].ToString() ?? throw new NotFoundException("endpoint not found");

        string actionType = context.Request.RouteValues["action"].ToString() ?? throw new NotFoundException("endpoint not found");

        var userId = Guid.Parse(context.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).Select(c => c.Value).FirstOrDefault());
        var userType = context.User.Claims.Where(c => c.Type == "userType").Select(c => c.Value).FirstOrDefault();
        var userRepo = context.RequestServices.GetRequiredService<IUserRepo>();
        var employeeRepo = context.RequestServices.GetRequiredService<IEmployeeRepo>();

        //if (userType == nameof(UserType.NormalUser) && await userRepo.CheckIfUserActiveAsync(userId))
        //    throw new AccessViolationException(sharedLocalizer[ErrorMessages.UserInActive]);

        if (userType == nameof(UserType.Employee) && !await employeeRepo.CheckIfEmployeeActiveAsync(userId))
            throw new AccessViolationException(sharedLocalizer[ErrorMessages.UserInActive]);

        if (controllerName.Contains("File"))
        {
            await next(context);
            return;
        }
        if (controllerName.Contains("Dashboard"))
        {
            if (userType == nameof(UserType.Admin))
            {
                await next(context);
                return;
            }
            else if (userType == nameof(UserType.Employee))
            {
                RoleDto role = new();
                var userRoles = await employeeRepo.GetEmployeeRoleAsync(userId);
                foreach (var userRole in userRoles.Data)
                {
                    role.AccreditedUniversityAccess |= userRole.AccreditedUniversityAccess;
                    role.EmployeeManagementAccess |= userRole.EmployeeManagementAccess;
                    role.BeneficiariesAccess |= userRole.BeneficiariesAccess;
                    role.NewsEventsAccess |= userRole.NewsEventsAccess;
                    role.EGateAccess |= userRole.EGateAccess;
                    role.EmployeeManagementAccess |= userRole.EmployeeManagementAccess;
                    role.WebsiteManagementAccess |= userRole.WebsiteManagementAccess;
                }
                switch (controllerName)
                {
                    case nameof(ControllerNames.DashboardEmployee):
                        if (!role.EmployeeManagementAccess)
                            throw new AccessViolationException();
                        break;
                    case nameof(ControllerNames.DashboardEGate):
                        if (!role.EGateAccess)
                            throw new AccessViolationException();
                        break;
                    case nameof(ControllerNames.DashboardUser):
                        if (!role.BeneficiariesAccess)
                            throw new AccessViolationException();
                        break;
                    case nameof(ControllerNames.DashboardAccreditedUniversity):
                        if (!role.AccreditedUniversityAccess)
                            throw new AccessViolationException();
                        break;
                    case nameof(ControllerNames.DashboardBasicPage):
                    case nameof(ControllerNames.DashboardMainPage):
                        if (!role.NewsEventsAccess)
                            throw new AccessViolationException();
                        break;

                    default:
                        throw new AccessViolationException();
                }
                await next(context);
                return;
            }
        }
        else
        {
            if (userType == nameof(UserType.Employee) || userType == nameof(UserType.Admin))
                throw new AccessViolationException(sharedLocalizer[ErrorMessages.NoAccess]);
            else
            {
                await next(context);
                return;
            }
        }
        throw new AccessViolationException(sharedLocalizer[ErrorMessages.NoAccess]);
    }
}