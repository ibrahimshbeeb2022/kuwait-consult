﻿using KuwaitConsulate.Dto;
using KuwaitConsulate.Shared.Exception;
using System.Net;

namespace KuwaitConsulate.Middleware;

public class ErrorHandlerMiddleware : IMiddleware
{
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next(context);
        }
        catch (Exception error)
        {
            var response = context.Response;
            response.ContentType = "application/json";
            CommonResponseDto<object> customResponse = new()
            {
                ErrorMessage = error.Message,
            };
            switch (error)
            {
                case ValidException:
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    break;
                case NotFoundException:
                    response.StatusCode = (int)HttpStatusCode.NotFound;
                    break;
                case MethodNotAllowedException:
                    response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                    break;
                case AccessViolationException:
                    response.StatusCode = (int)HttpStatusCode.Forbidden;
                    break;
                case UnauthorizedAccessException:
                    response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    break;
                case NotConfirmAccountException:
                    response.StatusCode = (int)HttpStatusCode.UnprocessableEntity;
                    break;
                default:
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }
            await response.WriteAsJsonAsync(customResponse);
        }
    }
}